
[iworms_measure_dt_src](./iworms_measure_dt_src) : Codes to measure clock differences between each pair of San Jacinto stations, and to compute clock errors 

[iworms_preprocess_src](./iworms_preprocess_src) : Processing tools and RESIF benchmarks

[iworms_synthetic_src](./iworms_synthetic_src) : Codes to simulate synthetic NCCFs, to measure clock differences for each pair of these synthetic stations, and to compute clock errors 

---

$ git clone git@gricad-gitlab.univ-grenoble-alpes.fr:lecoinal/iworms.git

---

wiki projet iworms : https://ciment.ujf-grenoble.fr/wiki/index.php/Projects/pr-iworms et https://gricad-gitlab.univ-grenoble-alpes.fr/lecoinal/iworms/wikis/home

---

:warning:
**Les données relatives au projet IWORMS ont été archivées sous** `/summer/f_image/lecointre/`

**Ces données ont été déplacées vers une zone d'archive froide à ISTerre** `ist-oar:/data/ondes/ARCHIVE/lecoinal/DATA/IWORMS/`

---

Accès au dernier paper.pdf généré (latex) : 

[paper_new.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/lecoinal/iworms/builds/artifacts/master/browse/docs/pdf?job=make_pdf_paper)

<!---

Compiler le papier en local dans un docker même image que gitlab GRICAD:
```
# recuperer les sources
> cd /tmp
> git clone https://gricad-gitlab.univ-grenoble-alpes.fr/lecoinal/iworms.git

# en wifi
> docker run  -v /tmp/iworms/paper:/root/iworms --privileged --net="host" -ti tianon/latex@sha256:e8f4f35be81fda7ee838ac8c21e5cf32251f7e218a6157144b9d548bd6b5ac93 /bin/bash
# en filaire
> docker run  -v /tmp/iworms/paper:/root/iworms -ti tianon/latex@sha256:e8f4f35be81fda7ee838ac8c21e5cf32251f7e218a6157144b9d548bd6b5ac93 /bin/bash

# et ensuite dans l'image docker
> apt-get update -qq && apt-get install -y -qq python-pygments
> cd /root/iworms
> pdflatex -shell-escape paper.tex
```

-->

[![pipeline status](https://gricad-gitlab.univ-grenoble-alpes.fr/lecoinal/iworms/badges/master/pipeline.svg)](https://gricad-gitlab.univ-grenoble-alpes.fr/lecoinal/iworms/commits/master)
