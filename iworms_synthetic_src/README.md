# iworms-synthetic
## A python code for creating synthetic Green Function timeseries at each pair of station, applying timeshift for pairs related to first station, measuring this timeshift, and invert it.


This [Python notebook](./srcPython) is related to the section **Numerical experiment with synthetic data** from the article:

**From clock-error detection to structure-oriented monitoring for massive seismic data.** *Lecointre, A., Roux, P, Bouttier, P.-A., Picard, C., Louvet, V.*, submitted to GJI.

---

The source code is available as a python script [synthetic.py](./srcPython/synthetic.py)

Dependencies:

 * scipy
 * h5py

An example of execution is also available as a ipynb notebook [synthetic.ipynb](./srcPython/synthetic.ipynb)

The synthetic input data used in the article: [dist_SW_synthetiques_3081pairs.dat](./inputdata/dist_SW_synthetiques_3081pairs.dat) is an ASCII file with interstation distances (in meters) between all stations from a complete array with n=79 stations (n*(n-1)/2=3081 pairs).

The notebook execution example was restricted to only [3 stations (3 pairs)](./inputdata/dist_SW_synthetiques_3pairs.dat), and thus 3 figures in the notebook output.

We also provide a [submission script](./submit.sh) adapted for ISTerre ist-oar cluster ("module" package manager for user software environment) and for GriCAD clusters ("Guix" package manager).

---

**Usage:**

```
$ python3 srcPython/synthetic.py -h
usage: synthetic.py [-h] [-v V] [--snr SNR] [--dec DEC] [--fi FI]

measure dt and compute Dt on synthetic data

optional arguments:
  -h, --help  show this help message and exit
  -v V        set logging level: 0 none, 1 info, default=info
  --snr SNR   SNR (default=100.0)
  --dec DEC   set option to apply a dcreasing factor of GF amplitude for
              distance > 20km (default=0 : disable)
  --fi FI     input file containing list of pairs and distance
              (default=./inputdata/dist_SW_synthetiques_3081pairs.dat)
```

The code simulates the Noise Cross Correlation Functions between all pairs of stations, and for several time steps (splitted into 3 equal periods names T1, T2, T3). Each NCCF is built from a reference signal, defined as a synthetic Gaussian pulse centered at a lag time corresponding to the current interstation distance. A decrease in amplitude is optionally applied for large interstation distances. A signal-to-noise ratio (SNR) parameter allows to add white noise to the reference NCCF.

The code simulates a clock error that occurs on first station only and on T2 period only, by applying a temporal shift to the correlations of the (n-1) pairs associated to this station, for the time steps from the T2 period.

The code measures the clock differences for each pair of stations (Eq. 11 from related article), and then takes the second station as master station to invert clock differences and compute clock errors (Eqs. 7 and 13 from related article).

*Example with only 3 pairs, SNR = 100 and no decreasing of Green Function with station pairs interdistance (decGF=0)*

```
$ python3 srcPython/synthetic.py --fi ./inputdata/dist_SW_synthetiques_3pairs.dat
measure dt for all pairs
ip, mean(dt(T1)), var(dt(T1)), mean(dt(T2)), var(dt(T2))
0 -2.6769314e-06 2.0676662e-05 0.9972964 2.5932462e-05
1 -5.755263e-07 2.5710166e-05 1.000271 2.4251865e-05
2 5.6426506e-07 2.4100156e-05 -0.00046851436 2.5996787e-05
compute Dt for all stations
Dt of first station if second station is the reference, mean(T1), var(T1), mean(T2), var(T2)
-2.1645178e-06 1.568023e-05 0.99844414 1.525185e-05
```
---

Note: The [previous Matlab deposit](./srcMatlab) is deprecated and no longer maintained.

---

**Links**

The [deposit of the iworms project](https://gricad-gitlab.univ-grenoble-alpes.fr/lecoinal/iworms).

The wikis of the iworms project : [old wiki (CIMENT)](https://ciment.ujf-grenoble.fr/wiki/index.php/Projects/pr-iworms) and [new wiki (GriCAD forge)](https://gricad-gitlab.univ-grenoble-alpes.fr/lecoinal/iworms/-/wikis/home).
