#!/bin/bash
##OAR --project iste-equ-ondes
#OAR --project iworms
#OAR -n iworms_synthetic
#OAR -l /nodes=1/core=1,walltime=00:05:00
##OAR -p network_address='ist-calcul10.ujf-grenoble.fr'
#OAR -t devel

# example: apply snr=0.1 and decGF=0:
# oarsub -S "./submit.sh 0.1 0"

set -e

date

START=$(date +%s.%N)

cat $OAR_NODE_FILE

# change here to switch between ISTerre cluster or one of the CIMENT/GriCAD - grid clusters
#source /soft/env.bash    # ISTerre cluster
source /applis/site/guix-start.sh   # GriCAD cigri clusters

########################

# In case of GriCAD grid clusters :

# suppose the GUIX profile named iworms-synthetic was previously created with installing
# > guix install -p /var/guix/profiles/per-user/lecoinal/iworms-synthetic python 
# > guix install -p /var/guix/profiles/per-user/lecoinal/iworms-synthetic python-h5py 
# > guix install -p /var/guix/profiles/per-user/lecoinal/iworms-synthetic python-scipy 

# The profile should contain:
# > guix package -p /var/guix/profiles/per-user/lecoinal/iworms-synthetic -I
# python	3.9.6	out	/gnu/store/p5fgysbcnnp8b1d91mrvjvababmczga0-python-3.9.6
# python-h5py	2.10.0	out	/gnu/store/4cczj0vda2rfb4y1nf9796dpd4vvcrq0-python-h5py-2.10.0
# python-scipy	1.7.3 	out	/gnu/store/ma1mbbx4wfpljf9s9vxrvmb9pmcrc0z9-python-scipy-1.7.3


############## do not change anything under this line

case "$CLUSTER_NAME" in
  ISTERRE)
    module load python/python3.7 
    ;;
  *)  # froggy|luke|dahu|ceciccluster|bigfoot)
    refresh_guix iworms-synthetic
    ;;
esac

# If not cigri, output dir is arbitrary Process_ID 
#PID=$$ 
#CIGRI_CAMPAIGN_ID=${CIGRI_CAMPAIGN_ID:=$PID}  # substitute if not initialized 
CIGRI_CAMPAIGN_ID=${CIGRI_CAMPAIGN_ID:=00000}  # substitute if not initialized

SNR=${1}
decGF=${2}

TMPDIR=$SHARED_SCRATCH_DIR/$USER/$CIGRI_CAMPAIGN_ID/oar.iworms_synthetic.$OAR_JOB_ID.$SNR.$decGF
#TMPDIR=$SHARED_SCRATCH_DIR/$USER/$CIGRI_CAMPAIGN_ID/oar.Tover12.iworms_synthetic.$OAR_JOB_ID.$SNR.$decGF
mkdir -p $TMPDIR
cp srcPython/synthetic.py $TMPDIR/.
mkdir $TMPDIR/inputdata
cp inputdata/dist_SW_synthetiques_3081pairs.dat $TMPDIR/inputdata/.
cd $TMPDIR

#/usr/bin/time -v python3 -u srcPython/synthetic.py --dec ${decGF} --snr ${SNR} --fi ./inputdata/dist_SW_synthetiques_3pairs.dat
#/usr/bin/time -v python3 -u srcPython/synthetic.py --dec ${decGF} --snr ${SNR}
#/usr/bin/time -v python3 -u synthetic.py --dec ${decGF} --snr ${SNR} --cc 1
/usr/bin/time -v python3 -u synthetic.py --dec ${decGF} --snr ${SNR} --cc 0

date
END=$(date +%s.%N)
DIFF=$(echo "$END - $START" | bc)
echo "ElapsedTime: $DIFF sec"

