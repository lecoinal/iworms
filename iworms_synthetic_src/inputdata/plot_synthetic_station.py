#! /usr/bin/env python
# -*- coding: utf-8 -*-

# FR.ARBF.00.HHZ 43.491700 5.332500
# FR.ARTF.00.HHZ 43.588200 5.806700
# FR.BANN.00.HHZ 44.368920 4.156330
# FR.BLAF.00.HHZ 43.951000 6.045000
# FR.BSTF.00.HHZ 43.800900 5.643300


from mpl_toolkits.basemap import Basemap,interp
import matplotlib.pyplot as plt
import numpy as np
from PIL import Image
from optparse import OptionParser
import argparse
from datetime import date
from datetime import datetime

# Definition de la région utilisée pour tracer la carte
m= Basemap(width=1000000,height=1000000,projection='lcc',lat_1=40.,lat_2=46.,lat_0=46.,lon_0=2.5,resolution='c') # France SW

# Trace les parallèles et les méridiens
'''set a background colour'''
m.drawparallels(np.r_[-40:90:5], labels=[1,1,0,0], color='grey')
m.drawmeridians(np.r_[-10:351:5], labels=[0,0,1,1], color='grey')

# Fond de Carte numérisé 
m.shadedrelief()

net = 'synthetic stations'

#----------------------------- 1998-2002 ----------------------------------------------------

lats=[] #longitudes
lngs=[] #latitudes
station=[] # name of station

# Lecture du fichier décrivant les stations : Nom_Sta Lat lon
fi=open(r'./stalatlon.txt','r')

for line in fi:
   lines=line.split(" ")
   stat,lon,lat=lines[0:3]
   lats.append(float(lon))
   lngs.append(float(lat))
   station.append(stat)

fi.close()
print(len(station))

dists = np.loadtxt('./pair_dist.txt', usecols=(2,))

print(len(dists))
print(dists)


# compute the native map projection coordinates for cities

x,y = m(lngs,lats)

for xpt, ypt in zip(x, y):
  label_txt="^"
  plt.plot(
    xpt,
    ypt,
    label_txt,
    markersize = 4,
    markerfacecolor='black',
    markeredgecolor='black',
    )

plt.suptitle('Network : n = ' + str(len(station)) + ' ' + net + ' ',fontsize=12)

#-----------------------------------------------------------------------------------------------------------------

plt.savefig('synthetic_station.png', dpi=300, bbox_inches=0)

print(dists)
plt.clf()
#plt.hist(1e-3 * dists, bins=np.logspace(np.log10(0.01),np.log10(450.0), 50), histtype='step')
plt.hist(1e-3 * dists, bins=np.linspace(0.01, 450.0, 50), histtype='step')
#plt.gca().set_xscale('log')
plt.grid()
plt.xlabel('interstation distances (km)',fontsize=15)
plt.xticks(fontsize=15)
plt.yticks(fontsize=15)
plt.savefig('synthetic_dists.png', dpi=300, bbox_inches=0)

