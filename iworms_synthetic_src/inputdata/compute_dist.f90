PROGRAM computedist

! $ head -3 stalatlon.txt
! FR.ANTF.00.BHE 43.564400 7.123400
! FR.ANTF.00.BHN 43.564400 7.123400
! FR.ANTF.00.BHZ 43.564400 7.123400

  IMPLICIT NONE

  INTEGER :: iwcl,ji,jj
  CHARACTER(len=128) :: sta,sta1,sta2
  CHARACTER(len=128), DIMENSION(:), ALLOCATABLE :: allsta
  REAL(KIND=8) :: lat,lon,rlat1,rlon1,rlat2,rlon2
  REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: rlat,rlon

  REAL(KIND=8), PARAMETER :: rT = 6378000.d0  ! Rayon terrestre
  REAL(KIND=8) :: pi
  REAL(KIND=8) :: dist
  
!  INTEGER :: narg,iarg
!  CHARACTER(len=256) :: cldum,clo

  pi=4.d0 * DATAN(1.d0)

!  narg = iargc()

!  iarg = 1
!  DO WHILE ( iarg <= narg )
!    CALL getarg ( iarg, cldum ) ; iarg = iarg + 1
!    SELECT CASE ( cldum )
!      CASE ('-latref' ) ; CALL getarg ( iarg, clo ) ; READ(clo,*)latref ; iarg=iarg+1
!      CASE ('-lonref' ) ; CALL getarg ( iarg, clo ) ; READ(clo,*)lonref ; iarg=iarg+1
!      CASE ('-distmax' ) ; CALL getarg ( iarg, clo ) ; READ(clo,*)distmax ; iarg=iarg+1
!      CASE DEFAULT   ; PRINT *,TRIM(cldum),' : option not available.' ; STOP
!    END SELECT
!  ENDDO

  open(11, file="stalatlon.txt", form="formatted", action="read")
  iwcl=0
  DO
    read(11,*,end=1000)sta,lat,lon 
    iwcl = iwcl + 1
  ENDDO
1000  close(11)

  ALLOCATE(rlat(iwcl),rlon(iwcl),allsta(iwcl))

  open(11, file="stalatlon.txt", form="formatted", action="read")
  iwcl=0
  DO
    read(11,*,end=2000)sta,lat,lon 
    iwcl = iwcl + 1
    allsta(iwcl) = TRIM(sta)
    rlat(iwcl) = lat * pi / 180.d0
    rlon(iwcl) = lon * pi / 180.d0
  ENDDO
2000  close(11)

  open(12, file="pair_dist.txt", form="formatted", action="write")
  DO ji=1,iwcl
    sta1=TRIM(allsta(ji))
    rlat1=rlat(ji)
    rlon1=rlon(ji)
    DO jj=ji+1,iwcl
      sta2=TRIM(allsta(jj))
      rlat2=rlat(jj)
      rlon2=rlon(jj)
      if ( rlat1==rlat2 .and. rlon1==rlon2 ) then
        dist=0.d0
      else
        dist = rT * DACOS( DSIN(rlat1) * DSIN(rlat2) + DCOS(rlat1) * DCOS(rlat2) * DCOS(rlon2-rlon1) )
      endif
      write(12,"(A20, A20, F20.3)")TRIM(sta1),TRIM(sta2),dist
    ENDDO
  ENDDO
  close(12)

END PROGRAM computedist



