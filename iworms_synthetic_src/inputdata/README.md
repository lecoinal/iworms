```

# source /applis/ciment/v2/env.bash

# module load gnu-devel/4.6.2

$ gfortran compute_dist.f90

$ ./a.out 

$ wc -l stalatlon.txt pair_dist.txt 
    79 stalatlon.txt
  3081 pair_dist.txt

$ python3 plot_synthetic_station.py 
79
3081
[ 39743.844 135741.095  76809.048 ... 141007.127 144518.294   5325.945]
[ 39743.844 135741.095  76809.048 ... 141007.127 144518.294   5325.945]
# this creates 2 plots synthetic_dists.png and synthetic_station.png

# merge two plots
$ convert -resize 950x713 synthetic_dists.png resized_synthetic_dists.png
$ convert resized_synthetic_dists.png -trim trim_resized_synthetic_dists.png
$ convert synthetic_station.png -trim trim_synthetic_station.png
$ composite -compose Over -gravity NorthWest -geometry +0+60 trim_resized_synthetic_dists.png trim_synthetic_station.png synthetic_station_dists.png
 


$ awk '{print $3 " % " $1 " "$2}' pair_dist.txt > dist_SW_synthetiques_3081pairs.dat


```
