#!/usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
#    This file is part of iworms-synthetic : a python code for creating       #
#    synthetic Green Function timeseries at each pair of station, applying    #
#    timeshift for pairs related to first station, measuring this timeshift,  #
#    and invert it.                                                           #
#                                                                             #
#    Copyright (C) 2019 Albanne Lecointre, Philippe Roux, Christophe Picard,  #
#                       Pierre-Antoine Bouttier, Violaine Louvet              #
#    albanne.lecointre@univ-grenoble-alpes.fr                                 #
#                                                                             #
#    This program is free software: you can redistribute it and/or modify     #
#    it under the terms of the GNU General Public License as published by     #
#    the Free Software Foundation, either version 3 of the License, or        #
#    (at your option) any later version.                                      #
#                                                                             #
#    This program is distributed in the hope that it will be useful,          #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of           #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
#    GNU General Public License for more details.                             #
#                                                                             #
#    You should have received a copy of the GNU General Public License        #
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.   #
###############################################################################

import h5py
import numpy as np
import sys
import math
from scipy.signal import butter, lfilter, gausspulse
import matplotlib.pyplot as plt
import matplotlib as mpl
import argparse


mpl.rcParams["figure.figsize"] = [7.4, 4.8]
mpl.rcParams["lines.linewidth"] = 1.0
mpl.rcParams["figure.dpi"] = 600.
mpl.rcParams["font.size"] = 15.



from matplotlib.transforms import (
    Bbox, TransformedBbox, blended_transform_factory)
from mpl_toolkits.axes_grid1.inset_locator import (
    BboxPatch, BboxConnector, BboxConnectorPatch)


def connect_bbox(bbox1, bbox2,
                 loc1a, loc2a, loc1b, loc2b,
                 prop_lines, prop_patches=None):
    if prop_patches is None:
        prop_patches = {
            **prop_lines,
            "alpha": prop_lines.get("alpha", 1) * 0.2,
        }

    c1 = BboxConnector(bbox1, bbox2, loc1=loc1a, loc2=loc2a, **prop_lines)
    c1.set_clip_on(False)
    c2 = BboxConnector(bbox1, bbox2, loc1=loc1b, loc2=loc2b, **prop_lines)
    c2.set_clip_on(False)

    bbox_patch1 = BboxPatch(bbox1, **prop_patches)
    bbox_patch2 = BboxPatch(bbox2, **prop_patches)

    p = BboxConnectorPatch(bbox1, bbox2,
                           # loc1a=3, loc2a=2, loc1b=4, loc2b=1,
                           loc1a=loc1a, loc2a=loc2a, loc1b=loc1b, loc2b=loc2b,
                           **prop_patches)
    p.set_clip_on(False)

    return c1, c2, bbox_patch1, bbox_patch2, p


def zoom_effect01(ax1, ax2, xmin, xmax, **kwargs):
    """
    Connect *ax1* and *ax2*. The *xmin*-to-*xmax* range in both axes will
    be marked.

    Parameters
    ----------
    ax1
        The main axes.
    ax2
        The zoomed axes.
    xmin, xmax
        The limits of the colored area in both plot axes.
    **kwargs
        Arguments passed to the patch constructor.
    """

    trans1 = blended_transform_factory(ax1.transData, ax1.transAxes)
    trans2 = blended_transform_factory(ax2.transData, ax2.transAxes)

    bbox = Bbox.from_extents(xmin, 0, xmax, 1)

    mybbox1 = TransformedBbox(bbox, trans1)
    mybbox2 = TransformedBbox(bbox, trans2)

    prop_patches = {**kwargs, "ec": "none", "alpha": 0.2}

    prop_lines = {**kwargs, "linestyle": "--"}

    c1, c2, bbox_patch1, bbox_patch2, p = connect_bbox(
        mybbox1, mybbox2,
        loc1a=3, loc2a=2, loc1b=4, loc2b=1,
        prop_lines=prop_lines, prop_patches=prop_patches)

    ax1.add_patch(bbox_patch1)
    ax2.add_patch(bbox_patch2)
    ax2.add_patch(c1)
    ax2.add_patch(c2)
    ax2.add_patch(p)

    return c1, c2, bbox_patch1, bbox_patch2, p








def butter_bandpass(lowcut, highcut, fs, order=5):
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    b, a = butter(order, [low, high], btype='band')
    return b, a


def butter_bandpass_filter(data, lowcut, highcut, fs, order=5):
    b, a = butter_bandpass(lowcut, highcut, fs, order=order)
    y = lfilter(b, a, data)
    return y


def read_list_pairs(path):
    """
    read the list of synthetic pairs:
     - distance in meters
     - indexes for zooming on surface wave (if SW=1)
    (this option SW=1 does not work anymore since I have
    corrected distance files: there is no more 2nd and 3rd columns)
    """
    f = open(path, 'r')
    lines = f.readlines()
    f.close()
    dist = []
    # swb = []
    # swe = []
    for l in lines:
        splitline = l.split(' ')
        dist.append(splitline[0])
        # swb.append(splitline[1])
        # swe.append(splitline[2])
    return dist
    # return dist, swb, swe


def compute_Dt(deltat, nstep, nbsta, nbref):
    """ Calcul du Dt:
     Dt(S=j,ref=k) = (1/nbsta) * ( sum( deltat[j,:]) ) - sum ( deltat[k,:] ) )
    """
    invN = 1.0/float(nbsta)
    dt_2D = np.zeros((nbsta, nbsta), dtype=np.float32)
    sumdt = np.empty((nbsta,), dtype=np.float32)
    for istep in np.arange(nstep):
        # reorganize flattened allpairs vector dt into an antisymmetric matrix
        # diag(dt_2D) should be 0-init
        cpt = 0
        for ji in np.arange(nbsta-1):
            for jj in np.arange(ji+1, nbsta):
                dt_2D[ji, jj] = deltat[istep, cpt]
                dt_2D[jj, ji] = -deltat[istep, cpt]
                cpt += 1
        for ji in np.arange(nbsta):
            # On calcule la somme des (N-1) elements non nuls de chaque ligne
            # (dt(ji,1)+dt(ji,2)+...+dt(ji,N))
            sumdt[ji] = np.sum(dt_2D[ji, :])
        # On choisit la station k comme référence, on suppose donc Dt(k)=0
        # On itère pour toutes les k références
        for iref in np.arange(nbref):
            for ji in np.arange(nbsta):
                # On calcule les differences de sommes 2 a 2
                Dt[iref, istep, ji] = invN*(sumdt[ji]-sumdt[iref])
    return Dt

def compute_Dtw(deltat, coh, nstep, nbsta, nbref):
    """ Calcul du Dtw:
     Dtw(S=j,ref=k) = sum( deltat[j,:]*coh[j,:] ) / sum( coh[j,:] without coh[j,j] )
                    - sum( deltat[k,:]*coh[k,:] ) / sum( coh[k,:] without coh[k,k] )
    """
    dt_2D = np.zeros((nbsta, nbsta), dtype=np.float32)
    co_2D = np.zeros((nbsta, nbsta), dtype=np.float32) # on met zero sur la diag des coh !
    sumdtcoh = np.empty((nbsta,), dtype=np.float32)
    sumcoh = np.empty((nbsta,), dtype=np.float32)
    for istep in np.arange(nstep):
        # reorganize flattened allpairs vector dt into an antisymmetric matrix
        # diag(dt_2D) should be 0-init
        cpt = 0
        for ji in np.arange(nbsta-1):
            for jj in np.arange(ji+1, nbsta):
                dt_2D[ji, jj] = deltat[istep, cpt]
                dt_2D[jj, ji] = -deltat[istep, cpt]
                co_2D[ji, jj] = coh[istep, cpt]
                co_2D[jj, ji] = coh[istep, cpt]
                cpt += 1
        for ji in np.arange(nbsta):
            # On calcule la somme des (N-1) elements non nuls de chaque ligne
            # (dt(ji,1)+dt(ji,2)+...+dt(ji,N))
            sumdtcoh[ji] = np.dot(dt_2D[ji, :],co_2D[ji,:])
            sumcoh[ji] = np.sum(co_2D[ji,:])
        # On choisit la station k comme référence, on suppose donc Dt(k)=0
        # On itère pour toutes les k références
        for iref in np.arange(nbref):
            for ji in np.arange(nbsta):
                # On calcule les differences de sommes 2 a 2
                Dt[iref, istep, ji] = (sumdtcoh[ji]/sumcoh[ji])-(sumdtcoh[iref]/sumcoh[iref])
    return Dt

def compute_coherence(refGF_norm, stepGF, dt, sf):
    """ mesure de la coherence entre refGF_norm et stepGF,
        associee au decalage dt
    """
    idec = int(round(-sf*dt))
    stepGF_rolled = np.roll(stepGF, idec)
    stepGF_rolled_norm = stepGF_rolled / max(abs(stepGF_rolled))
    coherence = np.corrcoef(refGF_norm, stepGF_rolled_norm)[1, 0]
    return coherence


def write_f32gz(fid, var, varname):
    """ Create and write a dataset named varname, containing
    variable var, into HDF5 file identifier fid,
    with F32 encoding, and apply gz compression filter
    """
    dset = fid.create_dataset(varname,
                              data=var,
                              dtype="float32",
                              compression="gzip")
    return dset


def get_gauspuls():
    """ Get gauspuls from provided input file
    """
    f = h5py.File('./inputdata/gauspuls.h5', 'r')
    sbc = f['signal_base_centre'][()]
    f.close()
    return sbc

def compute_gauspuls(time0,X):
    """ default fr bw in gausspulse is 0.5
    """
    sbc = gausspulse(time0,X)
    return sbc


if __name__ == '__main__':

    parser = argparse.ArgumentParser(
        description="measure dt and compute Dt on synthetic data")
    parser.add_argument("-v",
                        type=int,
                        default=1,
                        help="set logging level: 0 none, 1 info (default=1)")
    parser.add_argument("--cc",
                        type=int,
                        default=0,
                        help="method for measuring timeshift differences: \
                              0 phase differences, 1 maxabs of \
                              cross-correlations (default=0)")
    parser.add_argument("--snr",
                        default=100.0,
                        type=float,
                        help="SNR (default=100.0)")
    parser.add_argument("--dec",
                        default=0,
                        type=int,
                        help="set option to apply a decreasing factor of GF \
                              amplitude for distance > 20km \
                              (default=0 : disable)")
    parser.add_argument(
        "--fi",
        default='./inputdata/dist_SW_synthetiques_3081pairs.dat',
        type=str,
        help="input file containing list of pairs and distance \
        (default=./inputdata/dist_SW_synthetiques_3081pairs.dat)"
    )

    args = parser.parse_args()
    ln_verbose = args.v
    SNR = args.snr
    decGF = args.dec
    # 0 : no GF amplitude decrease with distance
    # 1 : after 20km, decrease GF amplitude with factor 1/sqrt(dist/20km)
    fileinput = args.fi

    sf = 10  # sampling frequency

    lnplot = 0

    # time vector in sec (only positive part: GF would be symmetric)
    time0 = np.arange(-300.0, 300.0+1.0/float(sf), 1.0/float(sf))
    vel = 3.0  # km/s

    SW = 2
    # [0/1/2]
    # 0 : entire correlation time -300sec:300sec
    # 1 : zoom on surface wave but window width varies with the distance
    # 2 : zoom with constant window width = 40sec

    CC = args.cc  # [0/1] 1 : cc method / O : rapport des phase

    nstep = 480 # 720

    decalage = 1

    REF = 240
    # 240 : GFtimeref sur T1 (periode sans décalage)
    # 720 : GFtimeref sur T1+T2+T3

    LIN = 0
    # 0 : decalage creneau 1sec sur T2
    # 1 : decalage lineaire 1sec sur T2 puis créneau sur T3

    niter = 1
    # number of successive evaluations of dt
    # only 1 is supported in Python code (see Matlab code for niter > 1)

    # Get some metrics : pair distances, start and end indexes
    # for surface wave zoom (in case of SW=1)
    # dist, swb, swe = read_list_pairs(fileinput)
    dist = read_list_pairs(fileinput)
    nbpair = len(dist)
    nbsta = int(math.ceil(math.sqrt(2.0*nbpair)))

    # Define a central frequency for phase
    fb1 = 0.1
    fb2 = 0.2   # frequence choisie   % 5-10sec = 0.1-0.2Hz
    fc = 0.5*(fb1+fb2)
    omega = 2.0 * math.pi * fc
    inv_omega = 1.0 / omega
    bw = 0.5

    # Gauspuls
    #sbc = get_gauspuls()
    sbc = compute_gauspuls(time0,fc)

    # caracteristique du filter une seule fois pour toute
    lowcut = 0.1
    highcut = 0.2
    bb, aa = butter_bandpass(lowcut, highcut, sf, order=3)

    # Pour calcul Dt, on prefere etre contigu en memoire selon les paires
    deltat = np.zeros((nstep, nbpair), dtype=np.float32)
    coh = np.zeros((nstep, nbpair), dtype=np.float32)

    if ln_verbose:
        print("measure dt for all pairs")
        print("ip, mean(dt(T1)), var(dt(T1)), mean(dt(T2)), var(dt(T2))")

    for ip in np.arange(nbpair):

        if lnplot:
            plt.clf()

        # keep GF in memory for each timesteps
        GF = np.zeros((nstep, len(time0)), dtype=np.float32)

        # indices pour le zoom éventuel sur l'onde de surface
        distance = float(dist[ip])*1e-3  # km
        if SW == 1:
            sys.exit("Option SW=1 does not work")
            # bsw = swb-1  # 1-based index -> 0-based index
            # esw = swe
        elif SW == 2:
            # fenetre de largeur fixe 40sec,
            # centrée sur centre gauspuls,
            # du côte des temps positifs,
            # sauf si trop proche de zero, on decale vers la droite
            t1 = max(distance/vel-20.0, 0)  # (3km/s)
            t2 = t1+40.0
            bsw = 3000+round(sf*t1)
            esw = 3001+round(sf*t2)
        else:
            bsw = 0
            esw = len(time0)

        rGF = np.zeros((esw-bsw,), dtype=np.float32)
        phase = np.zeros((esw-bsw,), dtype=np.complex64)

        # gauspuls : cote positif + cote negatif
        decalage0 = int(round(sf*(distance/vel)))
        signal_base = np.roll(sbc, decalage0) + np.roll(sbc, -decalage0)

        # Apply decreasing factor GF amplitude for distances > 20km
        if (int(decGF) == 1) and (distance > 20.0):
            decfactor = 1.0/np.sqrt(distance/20.0)
            signal_base *= decfactor

        if lnplot: # and ip < 2:
            ax1 = plt.subplot(2, 1, 1)
            ax1.plot(time0, signal_base, 'b', label=r'$CC_{ref}^{p='+str(ip)+'}$')
            ax1.plot([time0[bsw], time0[bsw]], [-1, 1], '--r')
            ax1.plot([time0[esw], time0[esw]], [-1, 1], '--r')
            ax1.grid()
            ax1.set_xlim((-300, 300))
            ax1.set_ylim((-1, 1))
            ax1.set_xticks([-300, 0, 300])
            if distance > 60:
                if SNR == 5:
                    ax1.set_title('b) dist = {:6.2f} km, SNR = {:3.1f}'.format(distance,SNR), \
                          fontsize=15)
                elif SNR == 0.8:
                    ax1.set_title('a) dist = {:6.2f} km, SNR = {:3.1f}'.format(distance,SNR), \
                          fontsize=15)
                else:
                    ax1.set_title('dist = {:6.2f} km, SNR = {:3.1f}'.format(distance,SNR), \
                          fontsize=15)
            else:
                if SNR == 5:
                    ax1.set_title('d) dist = {:6.2f} km, SNR = {:3.1f}'.format(distance,SNR), \
                          fontsize=15)
                elif SNR == 0.8:
                    ax1.set_title('c) dist = {:6.2f} km, SNR = {:3.1f}'.format(distance,SNR), \
                          fontsize=15)
                else:
                    ax1.set_title('dist = {:6.2f} km, SNR = {:3.1f}'.format(distance,SNR), \
                          fontsize=15)
            # plt.xlabel(r'$t_{corr} (sec)$')
            # plt.show()
            # plt.savefig('GP_'+'{:06.2f}'.format(distance)+'_'+str(SNR)+'.png',dpi=300)

        # timestep loop : ajout bruit + applique decalage en temps
        istepdec = 0
        for istep in np.arange(nstep):

            # creates rand matrix and add it to the GF foreach time steps
            r = np.random.uniform(-1, 1, len(time0))
            # r1 = 2*(rand(numel(time0),1)-0.5) uniform distribution
            # r2 = sqrt(1/3).*randn(numel(time0),1) normal law with var=1/3
            # print(np.mean(r),np.var(r))
            # r = butter_bandpass_filter(r, 0.1, 0.2, sf, order=3)
            r = lfilter(bb, aa, r)
            # print(np.mean(r),np.var(r))
            r /= max(abs(r))  # normalize
            r /= float(SNR)  # fix SNR
            GF[istep, :] = signal_base + r

            # if lnplot and (istep % 240) == 0:
            #if lnplot and (istep == 240) and ip < 2:
            if lnplot and (istep == 0): # and ip < 2:
                ax1.plot(time0, r, 'grey', label='noise')
                ax1.legend(fontsize=15, loc='right')

                # plt.clf()
                ax2 = plt.subplot(2, 1, 2)
                ax2.plot(time0, signal_base, 'b', label=r'$CC_{ref}^{p='+str(ip)+'}$')
                ax2.plot(time0, GF[istep], 'k', label=r'$CC_{i \in T1}^{p='+str(ip)+'}$')
                ax2.grid()
                ax2.set_xlim((time0[bsw], time0[esw]))
                ax2.set_ylim((-1.5, 1.5))
                #plt.title(r'$noise_{p='+str(ip)+'}^{i='+str(istep)+'} \
                #          (SNR='+str(SNR)+')$', fontsize=8)
                #plt.xticks([-300, 300])
                # plt.xlabel(r'$t_{corr} (sec)$')
                # plt.show()
                # plt.savefig('noise_'+'{:06.2f}'.format(distance)+'_'+str(SNR)+'_'+str(istep)+'.png',dpi=300)

                # plt.clf()
                #plt.subplot(4, 2, 5+ip)
                #plt.plot(time0, GF[istep], 'b')
                #plt.plot([time0[bsw], time0[bsw]], [-1, 1], '--r')
                #plt.plot([time0[esw], time0[esw]], [-1, 1], '--r')
                #plt.grid()
                #plt.xlim((-300, 300))
                #plt.xticks([-300, 300])
                #plt.title(r'$GP_{p='+str(ip)+'}^{i='+str(istep)+'}$',
                #          fontsize=8)
                # plt.xlabel(r'$t_{corr} (sec)$')
                # plt.show()
                # plt.savefig('GP_'+'{:06.2f}'.format(distance)+'_'+str(SNR)+'_'+str(istep)+'.png',dpi=300)

            if decalage == 1 and istep >= 240:
                # on impose des décalages sur T2 (et T3 dans le cas lineaire)

                # ici il faut choisir l'un des 2 cas
                # ligne de commentaire a changer en dur dans le code

                # on decale S1 ET S2
                # if ip>0 and ip<=(2*nbsta-4):
                # on ne décale que les paires 2 à 2N-3
                # (les paires des 2 premières stations),
                # la 1ere paire (S1-S2) reste donc en phase

                # OU on decale S1
                if ip < nbsta-1:
                    # on ne décale que les paires 1 à N-1
                    # (les paires de la premiere station)

                    # decalage lineaire sur T2 puis créneau sur T3
                    if LIN == 1:

                        if istep < 480:
                            istepdec += 1
                        decsec = (1.0 / 240.0) * istepdec
                        tmp = GF[istep, :]
                        GF[istep, :] = np.roll(tmp, int(round(decsec*sf)))

                    else:  # creneau sur T2

                        if istep < 480:
                            #decsec = 1.0
                            decsec = 0.15 / fc
                            #decsec = 2.0 / (3.0 * fc)    # 2T0/3
                            #decsec = 1.0 / (12.0 * fc)   # T0/12
                            tmp = GF[istep, :]
                            GF[istep, :] = np.roll(tmp, int(round(decsec*sf)))

        # end loop on timesteps pour ajout bruit + decalage

        # Now we cut all nstep GF for this pair between swb and swe

        # try to measure dt for each step

        if CC == 0: 
            phase = np.exp(1j * omega * time0[bsw:esw])
            # compute the reference
            rGF = np.mean(GF[0:int(REF), bsw:esw], axis=0)
            # and normalized ref (for coherence)
            rGF_n = rGF/max(abs(rGF))
            # DFT signal ref
            rDFT = np.dot(rGF, phase)
            for istep in np.arange(nstep):
                # DFT signal perturbé (step)
                sDFT = np.dot(GF[istep, bsw:esw], phase)
                # mesure du dt
                deltat[istep, ip] = np.angle(sDFT/rDFT) * inv_omega
                coh[istep, ip] = compute_coherence(rGF_n,
                                                   GF[istep, bsw:esw],
                                                   deltat[istep, ip],
                                                   sf)
        else:
            rGF = np.mean(GF[0:int(REF), bsw:esw], axis=0)
            rGF_n = rGF/max(abs(rGF))
            for istep in np.arange(nstep):
                tmp = np.correlate(rGF,GF[istep, bsw:esw],"full")
                deltat[istep,ip] = -float(np.argmax(tmp)-tmp.size/2)/sf
                coh[istep, ip] = compute_coherence(rGF_n,
                                                   GF[istep, bsw:esw],
                                                   deltat[istep, ip],
                                                   sf)

        if ln_verbose:
            print(ip,
                  np.mean(deltat[0:240, ip]),
                  np.var(deltat[0:240, ip]),
                  np.mean(deltat[240:480, ip]),
                  np.var(deltat[240:480, ip]))

        if lnplot: #and ip < 2:
            # plt.clf()
            #plt.subplot(4, 2, 7+ip)
            #plt.plot(time0[bsw:esw], GF[0, bsw:esw], '--b', label='istep=0')
            #plt.plot(time0[bsw:esw], rGF, 'r', label='ref')
            ax2.plot(time0[bsw:esw], GF[240, bsw:esw], 'r', label=r'$CC_{i \in T2}^{p='+str(ip)+'}$')
            #if ip < 1:
            ax2.legend(fontsize=15, loc='right')
            #plt.grid()
            ax2.set_xlabel(r'$t (s)$')
            ax2.text(time0[bsw]+5, -1.3, r'$\delta^{p='+str(ip)+'}_{i \in T2}\
                      ='+'{:4.2f}'.format(deltat[240, ip])+', \
                      coh^{p='+str(ip)+'}_{i \in T2}\
                      ='+'{:4.2f}'.format(coh[240, ip])+'$', fontsize=15, color='r')
            zoom_effect01(ax1, ax2, time0[bsw], time0[esw])
            # plt.show()
            # plt.savefig('GFsr_'+'{:06.2f}'.format(distance)+'_'+str(SNR)+'_240.png',dpi=300)

        if lnplot:
            plt.savefig('pair'+str(ip)+'_decGF'+str(decGF)+'_SNR'+str(SNR)+'.png', dpi=600)
            #plt.show()

    # end of global loop over all the pairs

    # write dt into h5 output file
    #outfile = "/nfs_scratch/lecoinal/zzzzz/SNR{0}_SW{1}_CC{2}_decGF{3}_REF{4}_LIN{5}_nstep{6}_nbpair{7}" \
    outfile = "./SNR{0}_SW{1}_CC{2}_decGF{3}_REF{4}_LIN{5}_nstep{6}_nbpair{7}" \
              ".h5".format(SNR, SW, CC, decGF, REF, LIN, nstep, nbpair)
    f = h5py.File(outfile, 'w')
    dset = write_f32gz(f, deltat, "dt")
    dset = write_f32gz(f, coh, "coh")

    # Calcul du Dt
    # Dt(S=j,ref=k) = (1/N) * ( sum( dt[j,:]) ) - sum ( dt[k,:] ) )

    nbref = nbsta  # on essaiera toutes les sta comme ref, successivement
    Dt = np.zeros((nbref, nstep, nbsta), dtype=np.float32)
    Dt = compute_Dt(deltat, nstep, nbsta, nbref)

    dset = write_f32gz(f, Dt, "Dt")
    
    # On moyenne les solutions obtenues avec toutes les références
    # N.B. : cela revient au même que de directement moyenner
    # tous les dts arrivant (ou partant selon convention choisie) à un noeud
    Dt_mean_ref = np.mean(Dt, axis=0)

    dset = write_f32gz(f, Dt_mean_ref, "Dt_mean_ref")

    if ln_verbose:
        print("compute Dt for all stations")
        ista = 0
        iref = 1
        print("Dt of first station if second station is the reference, "
              "mean(T1), var(T1), mean(T2), var(T2)")
        print(np.mean(Dt[iref, 0:240, ista]),
              np.var(Dt[iref, 0:240, ista]),
              np.mean(Dt[iref, 240:480, ista]),
              np.var(Dt[iref, 240:480, ista]))
        print("Dt of first station if all stations (including first) "
              "are successively the references, mean(T1), var(T1), "
              "mean(T2), var(T2)")
        print(np.mean(Dt_mean_ref[0:240, ista]),
              np.var(Dt_mean_ref[0:240, ista]),
              np.mean(Dt_mean_ref[240:480, ista]),
              np.var(Dt_mean_ref[240:480, ista]))

    # Calcul du Dtw
    # Dtw(S=j,ref=k) = sum( dt[j,:]*coh[j,:] ) / sum( coh[j,:] without coh[j,j] ) 
    #                - sum( dt[k,:]*coh[k,:] ) / sum( coh[k,:] without coh[k,k] )

    Dt = np.zeros((nbref, nstep, nbsta), dtype=np.float32)
    Dt = compute_Dtw(deltat, coh, nstep, nbsta, nbref)

    dset = write_f32gz(f, Dt, "Dtw")

    f.close()

