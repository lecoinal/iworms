% create a synthetic Green Function

% on utilise ces metriques : luke4:/scratch_md1200/lecointre/IWORMS/METRICS/SW/
% HHZ
% FR,Z3,G,ZH
% lat 43 - 46
% lon 3 - 8
% 1 year 2016183 -> 2017181
% -> 79 traces -> 79*78/2=3081
% lecointre@luke4:/scratch_md1200/lecointre/IWORMS/METRICS/SW$ wc -l pair_dist.txt stalatlon.txt
%  3081 pair_dist.txt
%    79 stalatlon.txt

% awk '{print $3 " % " $1 " " $2}' /media/luke4_scratch_md1200/lecointre/IWORMS/METRICS/SW/pair_dist.txt > dist.dat

% NON PLUTOT CES METRIQUES : adaptées à nos analyses 1year des slides chamonix
% FR,Z3,G
% lat 43 - 46
% lon 3 - 8
% 1 year 2016183 -> 2017181
% lecointre@luke4:/scratch_md1200/lecointre/11048/oar.iworms.3895435.2017012$ wc -l pair_dist.txt 
% 2838 pair_dist.txt
% il s'agit en fait exactement de luke4:/scratch_md1200/lecointre/IWORMS/METRICS/SW/pair_dist_sw.txt
% lecointre@luke4:/scratch_md1200/lecointre/IWORMS/METRICS/SW$ wc -l pair_dist_sw.txt 
% 2838 pair_dist_sw.txt

% awk '{print $3 " " $4 " " $5 " " $6 " " $7 " " $8 " " $9 " % " $1 " " $2}' /media/luke4_scratch_md1200/lecointre/IWORMS/METRICS/SW/pair_dist_sw.txt > dist.dat

% il manque 3081-2838=243 paires pour lesquelles on n'a pas de SW positions
% ce sont ces paires:
% awk '{print $3 " % " $1 " " $2}' /media/luke4_scratch_md1200/lecointre/IWORMS/METRICS/SW/pair_dist.txt > dist_3081.dat
% awk '{print $3 " % " $1 " " $2}' /media/luke4_scratch_md1200/lecointre/IWORMS/METRICS/SW/pair_dist_sw.txt > dist_2838.dat
% grep -v -f dist_2838.dat dist_3081.dat |wc -l

% awk '{print $3 " " $4 " " $5 " % " $1 " " $2}' /media/luke4_scratch_md1200/lecointre/IWORMS/METRICS/SW/pair_dist_sw.txt > dist_SW_2838.dat

%------------------------------

% Laurent Stehly 2018/02/08
% la fonction de Green analytique a une distance données (dist) pour un milieu ou les ondes se propagent à une vitesse vel, et s'attenue avec une atténation Q. En pratique le signal est large bande. Tu voudras le filtrer dans une bande de fréquence qui t'intéresse (5-10s par ex).

%------------------------------

clear

%addpath .

Fs=10;

%figure(1);clf;hold on;grid on;

timepos=0:0.1:300; % time vector in sec (only positive part: GF would be symmetric)
time0=-300:0.1:300; 
vel=3; % km/s
Q=50; % attenuation factor

SNR=<<<SNR>>>; % 100; % 1; % 100;

SW=2;  % [0/1/2] 
       % 0 : -300sec:300sec
       % 1 : zoom on surface wave but window width varies with the distance
       % 2 : zoom on surface wave with window width = 4xgauspuls_width = 4x20sec = 80sec

CC=0;  % [0/1] 1 : cc method / O : rapport des phase

nstep=720; % <<<nstep>>>; % 24hour or 365 days ?

decalage=1;

REF=<<<REF>>>;

LIN=<<<LIN>>>;

decGF=0;  % 0 : no GF amplitude decrease with distance
          % 1 : after 20km, decrease GF amplitude with factor 1/sqrt(dist/20km)

ln_plot = 0; %1;

niter = <<<NITER>>>; % 1;
%niter = 5;

distarray=load('<<<filesta>>>');
nbpair=length(distarray);

nbsta=ceil(sqrt(2*nbpair))

outfile=['SNR_',num2str(SNR),'_SW',num2str(SW),'_CC',int2str(CC),'_decGF',int2str(decGF),'_nstep',int2str(nstep),'_nbpair',int2str(nbpair),'.h5'];

% dist=distarray(1,1)*1e-3; % km

%gf_pos=get_gf_fft_numericaly(timepos,dist,vel,Q);

%gf(1:3001)=gf_pos(3001:-1:1);
%gf(3001:6001)=gf_pos(1:3001);

%subplot(211);hold on;grid on;
%plot(time,gf,'-b');

%----------

X=0.15; %frequence choisie   % 5-10sec = 0.1-0.2Hz


bw=0.5;
signal_base_centre = gauspuls(time0,X,bw)';

alldt = zeros(nstep,nbpair);

%for ipair = 525:525;
%for ipair = 1:1;
%for ipair = 1651:1651;
%for ipair = [1 525 1651];
for ipair = 1:nbpair;
  clear GF signal_base time GF_100Hz GF_100_corrected GF_corrected GF_ref GF_ref100 SWlimit phase_1 r %time100Hz 
  ipair

  dist=distarray(ipair,1)*1e-3; % km
  if SW==1;
    SWlim=distarray(ipair,[2 3]);
  elseif SW==2;
    % fenetre de largeur fixe 40sec centrée sur centre gauspuls sauf si trop proche de zero, on decale vers la droite
    t1=max(dist/vel-20,0); % (3km/s)
    t2=t1+40;
    it1=3001+round(10*t1); 
    it2=3001+round(10*t2);
    SWlim=[it1 it2]; 
  else;
    SWlim=[1 length(time0)];
  end
  SWlimit=SWlim(1):SWlim(2);

  %cote positif+cote negatif
  decalage0=round(10*(dist/vel));
  signal_base = circshift(signal_base_centre,decalage0)+circshift(signal_base_centre,-decalage0);

  if ( decGF == 1 ) && ( dist > 20 ) ;
    decfactor = 1/sqrt(dist/20);
    signal_base = decfactor * signal_base;
  end

%subplot(212);hold on;grid on;
%plot(time,signal_base,'-k');

%print('-dpng','fig0.png');

%---------

  % creates rand matrix and add the the GF as many times as we want time steps
  istepdec=0;
  for istep=1:nstep

    r=2*(rand(numel(time0),1)-0.5);
    %%r1=2*(rand(numel(time0),1)-0.5);  %distribution uniform
    %%r2 = sqrt(1/3).*randn(numel(time0),1); % distribution normal de var 1/3

% option 1
%    r = filter1('bp',r,'fc',[0.1 0.2],'fs',Fs);  % filtre bandpass 5-10sec

% option 2
    [BB AA]=butter(3,[0.1 0.2]/Fs*2);  % Wn[2] donc 'bandpass' par défault
    r=filter(BB,AA,r);

    r=r/max(abs(r));  % normalize
    r=r/SNR;  % fix SNR = 100

    GF(:,istep)=signal_base+r;

    if ( ( decalage == 1 ) && ( istep>240 ) );
     % optionnel : on impose des décalages:
     % decale sta 1 de + 1 sec

%      if ( ipair > 1 && ipair < (2*nbsta-4) ) ; % on ne décale que les paires 2 à 2N-3 : les paires des 2 premières stations, la première paire (S1-S2) reste donc en phase
      if ipair < nbsta ; % on ne décale que les paires 1 à N-1 : les paires de la première station

        if ( LIN == 1 );   % decalage lineare entre T/3 et 2T/3

          if ( istep < 481 );
            istepdec = istepdec + 1;
          end

          decsec = (1.0 / 240.0) * istepdec; %  0.1*min([istep 24]);
          tmp = GF(:,istep);
          round(decsec*Fs);
          GF(:,istep) = circshift(tmp,round(decsec*Fs));

        else   % creneau

          if ( istep < 481 );
            decsec = 1;
            tmp = GF(:,istep);
            round(decsec*Fs);
            GF(:,istep) = circshift(tmp,round(decsec*Fs));
          end       
        end
      end
    end

  end

  % Now we cut all the arrays between SWlim
  GF=GF(SWlimit,:);
  time=time0(SWlimit);
  signal_base=signal_base(SWlimit);

  if ln_plot == 1;
    figure(1);clf;hold on;grid on;
    subplot(4,4,[2 3 4 6 7 8 10 11 12]);imagesc(time,1:24,GF');colorbar('location','east');xlim([time(1) time(end)])
    title(['SNR:',num2str(SNR),' SW:',num2str(SW),' dist:',num2str(dist)]);
    subplot(4,4,[14 15 16]);plot(time,signal_base);
  end

  % try to measure dt for erach step

  if CC==0;
    phase_1=exp(2*pi*i*X*time).'; % pour la mesure dt avec rapport des phase
  else;
    maxlag=50;  % +/-5sec
  end
  
  for iter = 1:niter;

  % compute the reference
  tmp = GF(:,1:REF);
  GF_ref = mean(tmp');
  if ln_plot == 1;
    subplot(4,4,[14 15 16]);hold on;plot(time,GF_ref,'--r');grid on;xlim([time(1) time(end)])
  end

  if CC==0;
    DFT_base=mean(GF_ref'.*phase_1,1); % DFT signal ref
    clear GF_100Hz
    for istep=1:nstep;
      DFT_fluct=mean(GF(:,istep).*phase_1,1); % DFT signal perturbé
      deltat(istep)=angle(DFT_fluct./DFT_base)/2/pi/X; %calcul du dt
%%%      GF_corrected(:,istep) = circshift(GF(:,istep),round(10*(-1)*deltat(istep)));
      % reinterpoler a 100Hz pour affiner mesure de coherence
      %[GF_100Hz(:,istep),time100Hz] = resample(GF(:,istep),time,100,'linear');
      GF_100Hz(:,istep) = resample(GF(:,istep),10,1);
    end
  
    % GFref corrige surechantillonne a 100Hz
    GF_ref100 = mean(GF_100Hz');
    % mesure de coherences
    for istep=1:nstep;
      GF_100_corrected = circshift(GF_100Hz(:,istep),round(100*(-1)*deltat(istep)));
      coh(istep)=xcorr(GF_ref100',GF_100_corrected,0,'coeff');
      % mise a jour GF(istep,:) pour seconde iteration
      GF(:,istep) = resample(GF_100_corrected,1,10);
    end
  else;
      % methode correlation
    for istep=1:nstep;
      [cc,lags]=xcorr(GF_ref',GF(:,istep),maxlag,'coeff');
      [I,J]=max(abs(cc));
      coh(istep)=I;deltat(istep)=lags(J)/Fs;
      % mise a jour GF(istep,:) pour seconde iteration
      tmp = circshift(GF(:,istep),Fs*(-1)*deltat(istep))
      GF(:,istep) = tmp;
    end
  end

  if ln_plot == 1;
  subplot(4,4,[1 5 9]);
  grid on;hold on;
  pl1 = plot(coh,1:24,'--o','Color','r');
  ax1 = gca; % current axes
  ax1.XColor = 'r';
  ax1.YColor = 'r';
  ax1.YDir = 'reverse';ylim([0 25]);
%ax1.xlabel('dt');
  ax1_pos = ax1.Position; % position of first axes
  ax2 = axes('Position',ax1_pos,'XAxisLocation','top','YAxisLocation','right','Color','none');
  grid on;hold on;
  pl2 = plot(deltat,1:24,'--o','Parent',ax2,'Color','k');
  ax2 = gca;
  ax2.YDir = 'reverse';ylim([0 25]);
%ax2.xlabel('coherence');
%%  Leg = legend([pl1; pl2], {'coh', 'dt'},'location','northwest');
  text(0,27.5,'coherence','color','r','horizontalalignment','center');
  text(0,-2.5,'dt','color','k','horizontalalignment','center');

  print('-dpng',['SNR_',num2str(SNR),'_SW_',num2str(SW),'_dist_',num2str(dist),'.png']);
  end

  allcoh(:,iter,ipair)=coh(:);
  alldti(:,iter,ipair)=deltat(:);
  
  alldt(:,ipair)=alldt(:,ipair)+alldti(:,iter,ipair);

  end % end loop on iteration

end



hdf5write(outfile, '/dt_bypair', alldt)
hdf5write(outfile, '/dt_bypair_iter', alldti, 'WriteMode', 'append')
hdf5write(outfile, '/coh_iter', allcoh, 'WriteMode', 'append')
