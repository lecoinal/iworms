function gf=get_gf_fft_numericaly(time,dist,vel,Q)
  % Compute the analytical GF between two points in a 2D homogeneous medium: 
  %   time = time vector on which the GF will be interpolated : i.e [0:1:100] [s]
  %   dist = distance between the two points [km] : i.e 50  
  %   vel  = velocity of the medium [km/s]        : ie. 3 km/s 
  %   Q    = attenuation factor                   : i.e 50
 if isnan(dist) ; dist=0; end
  nt = numel(time);
  gf = zeros(1,nt);
  t0 = dist/vel;

  rho= 1 ;
  fe=1./(time(2)-time(1));
  w= 2*pi*[0:fe/nt:fe/2 -fe/2:fe/nt:-fe/nt];
  gf1=-i/(4*rho*vel^2);
  gf2= sqrt((2*vel)/(pi*dist)); % geom : marche mais pas correct : manque dependance en omega
  gf3 =exp(-i*w*dist/vel);
  gf4 =exp(-w*dist/(2*vel*Q));
  gf5 =exp(i*pi/4);
  gf=gf1.*gf2.*gf3.*gf4.*gf5;
  m0=ceil(numel(gf)/2);
  gf(m0+1:end) = conj(gf([m0:-1:2]));
  gf=real(ifft(gf));
end

