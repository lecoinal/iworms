#!/bin/bash
#OAR --project iste-equ-ondes
#OAR -n compute_Dt
#OAR -l /nodes=1/core=1,walltime=00:10:00

set -e

date

# Modules loading
source /soft/env.bash
module load MATLAB/R2017a

nstep=$1
SNR=$2
nbpair=$3
#DEC=$4 # FIXE ISTEP
file=$4 # /nfs_scratch/lecoinal/oar.dt_synthetic.379450/SNR_100_SW2_CC0_decGF1_nstep720_nbpair3081.h5

# example:
# oarsub -S "./submit_Dt_decalage.sh 720 1 3081 FIXE"
# oarsub -S "./submit_Dt_decalage.sh 720 100 3081 /nfs_scratch/lecoinal/oar.dt_synthetic.379450/SNR_100_SW2_CC0_decGF1_nstep720_nbpair3081.h5"

TMPDIR=$SHARED_SCRATCH_DIR/$USER/oar.compute_Dt.$OAR_JOB_ID
mkdir -p $TMPDIR
sed -e "s/<<<nstep>>>/$nstep/" -e "s/<<<SNR>>>/$SNR/" -e "s/<<<nbpair>>>/$nbpair/" compute_Dt_skel.m > $TMPDIR/compute_Dt.m
cd $TMPDIR
#ln -sf $SHARED_SCRATCH_DIR/$USER/SYNTHETIC_GF_IWORMS/DECALAGE_${DEC}/SNR_${SNR}_SW2_CC0_decGF1_nstep${nstep}_nbpair${nbpair}.h5 .
ln -sf $file .

# Launch compute job
matlab -nodisplay -nosplash < compute_Dt.m > result.out

date
