clear

SW=2;
decGF=0;idecGF=1;


nstep=<<<nstep>>>;
SNR=<<<SNR>>>;
nbpair=<<<nbpair>>>;

nbsta=ceil(sqrt(2*nbpair))

infile=['SNR_',num2str(SNR),'_SW',int2str(SW),'_CC0_decGF',int2str(decGF),'_nstep',int2str(nstep),'_nbpair',int2str(nbpair),'.h5'];
outfile=['Dt_SNR_',num2str(SNR),'_SW',int2str(SW),'_CC0_decGF',int2str(decGF),'_nstep',int2str(nstep),'_nbpair',int2str(nbpair),'.h5'];

dt=h5read(infile,'/dt_bypair');

N=ceil(sqrt(2*length(dt)));
invN=1/N;

dt_2D=zeros(N,N);
Dt=nan*zeros(nstep,N);

for ihour=1:nstep;
   ii=0;ihour
   for in=1:N;
      for jn=in+1:N;
         ii=ii+1;
         dt_2D(in,jn)=dt(ihour,ii);
         dt_2D(jn,in)=-dt(ihour,ii);
      end
   end

   % optionnel : on impose des décalages:
   decalage=['no'];
   % decale sta 1 de + 1 sec
   %decalage=['sta1p1'];
   %decalage=['sta1p1-sta2m2'];
   %decalage=['sta1p1-sta2m2-staNm1'];
   %decalage=['staNm1'];
   %dt_2D(1,:)=dt_2D(1,:)+1;
   %dt_2D(:,1)=dt_2D(:,1)-1;
   % decale sta 2 de -2 sec
   %dt_2D(2,:)=dt_2D(2,:)-2;
   %dt_2D(:,2)=dt_2D(:,2)+2;
   % decale sta N (ref) de -1 sec
   %dt_2D(N,:)=dt_2D(N,:)-1;
   %dt_2D(:,N)=dt_2D(:,N)+1;
   % fin option decalage

   for in=1:N;
      % On calcule la somme des (N-1) elements non nuls de chaque ligne
      % (dt(i,1)+dt(i,2)+...+dt(i,N))
      sumdt(ihour,in)=sum(dt_2D(in,:));
   end
   % On choisit la station N comme référence, on suppose donc Dt(N)=0
   for ref=1:N; % ref = N-1;
      for in=1:N;
         % On calcule les differences de sommes 2 a 2
         %sdtN=sum(dt_2D(N,:));
         Dt(ihour,in,ref)=invN*(sumdt(ihour,in)-sumdt(ihour,ref));
      end
   end
end

hdf5write(outfile, '/Dt_allref', Dt)
Dt_mean_ref=mean(Dt,3);
hdf5write(outfile, '/Dt_meanref', Dt_mean_ref, 'WriteMode', 'append')

% compute mean and var of Dt for each period
meanDt_T1=mean(Dt(1:240,:,:));varDt_T1=var(Dt(1:240,:,:));
meanDt_T2=mean(Dt(241:480,:,:));varDt_T2=var(Dt(241:480,:,:));
meanDt_T3=mean(Dt(481:720,:,:));varDt_T3=var(Dt(481:720,:,:));
hdf5write(outfile, '/meanDt_T1', meanDt_T1, 'WriteMode', 'append')
hdf5write(outfile, '/meanDt_T2', meanDt_T2, 'WriteMode', 'append')
hdf5write(outfile, '/meanDt_T3', meanDt_T3, 'WriteMode', 'append')
hdf5write(outfile, '/varDt_T1', varDt_T1, 'WriteMode', 'append')
hdf5write(outfile, '/varDt_T2', varDt_T2, 'WriteMode', 'append')
hdf5write(outfile, '/varDt_T3', varDt_T3, 'WriteMode', 'append')

% distribution pour 3 reference N,N-1,1
bw=0.01/SNR; % binwidth
figure('units','normalized','outerposition',[0 0 1 1],'visible','off');clf;
grid on;hold on;
%refcol=['+-c';'+-b';'+-m'];
refcol=colormap(parula(N));
iref=0;
XX=[-idecGF/SNR:bw:idecGF/SNR];
for ref = 1:N; % [N N-1 1];
  iref=iref+1
  Dtref=squeeze(Dt(:,:,ref));
  maxabsDt=max(abs(Dtref(:)));
  maxabs=bw*ceil(maxabsDt/bw);edge=[-maxabs:bw:maxabs];
  h=histogram(Dtref,edge);X=h.BinEdges(1:end-1)+0.5*h.BinWidth;Y=h.Values;
  Q=trapz(Y);Y(:)=Y(:)/Q;
  f = fit(X.',Y.','gauss1')
mu(iref) = f.b1;
sigma1(iref) = f.c1 / sqrt(2);
sigma2(iref) = 2 / (sqrt(2*pi) * f.a1);
  YY(:,iref)=f.a1*exp(-((XX-f.b1)/f.c1).^2);
end
  Dt_mean_ref=mean(Dt,3);
  maxabs=max(abs(Dt_mean_ref(:)));
  maxabs=bw*ceil(maxabs/bw);edge=[-maxabs:bw:maxabs];
  h=histogram(Dt_mean_ref,edge);X=h.BinEdges(1:end-1)+0.5*h.BinWidth;Y=h.Values;
  Q=trapz(Y);Y(:)=Y(:)/Q;
  f = fit(X.',Y.','gauss1')
mu(iref+2) = f.b1;
sigma1(iref+2) = f.c1 / sqrt(2);
sigma2(iref+2) = 2 / (sqrt(2*pi) * f.a1);
%  legend('data refN','fit gauss1 refN','data refN-1','fit gauss1 refN-1','data ref1','fit gauss1 ref1','data meanref','fit gauss1 meanref');
iref=iref+1;YY(:,iref)=NaN;
iref=iref+1;YY(:,iref)=f.a1*exp(-((XX-f.b1)/f.c1).^2);
figure('units','normalized','outerposition',[0 0 1 1],'visible','off');clf;
grid on;hold on;
h=imagesc(XX,1:N+2,YY');colorbar;
set(h,'alphadata',~isnan(YY'));
title({['normalized Dt value distribution among ',int2str(nstep),'h * ',int2str(nbsta),' stations'];['ref 1 day - SNR:',num2str(SNR),' - SW:',num2str(SW),' (bins de largeur fixe 0.01/SNR sec)']});
print('-dpng',['Dt_gaussianfit3D_SNR_',num2str(SNR),'_SW',num2str(SW),'_decGF',int2str(decGF),'_nstep',int2str(nstep),'_nbpair',int2str(nbpair),'.png'])

hdf5write(outfile, '/mu', mu, 'WriteMode', 'append')
hdf5write(outfile, '/sigma1', sigma1, 'WriteMode', 'append')
hdf5write(outfile, '/sigma2', sigma2, 'WriteMode', 'append')

