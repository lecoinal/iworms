#!/bin/bash
#OAR --project iste-equ-ondes
#OAR -n dt_synthetic
#OAR -l /nodes=1/core=1,walltime=10:00:00
##OAR -p network_address='ist-calcul10.ujf-grenoble.fr'

date

START=$(date +%s.%N)

cat $OAR_NODE_FILE

# Modules loading
source /soft/env.bash
module load MATLAB/R2017a

nstep=$1  # always 720
SNR=$2
REF=$3 # 240 (T/3) or 720 (T)
LIN=$4 # 1: linear derive 1sec au total / 0: creneau 1sec
filesta=$5
niter=$6

# example:
# oarsub -S "./submit_decalage_Tover3.sh 720 100 240 1 dist_SW_synthetiques.dat 1"

TMPDIR=$SHARED_SCRATCH_DIR/$USER/oar.dt_synthetic.$OAR_JOB_ID
mkdir -p $TMPDIR
sed -e "s/<<<nstep>>>/$nstep/" -e "s/<<<SNR>>>/$SNR/" -e "s/<<<REF>>>/$REF/" -e "s/<<<LIN>>>/$LIN/" -e "s/<<<filesta>>>/$filesta/" -e "s/<<<NITER>>>/$niter/" synthetic_GF_decalage_Tover3_skel.m > $TMPDIR/synthetic_GF_decalage.m
cp $filesta $TMPDIR/.
cd $TMPDIR

# Launch compute job
matlab -nodisplay -nosplash < synthetic_GF_decalage.m > result.out

END=$(date +%s.%N)
DIFF=$(echo "$END - $START" | bc)
echo "ElapsedTime: $DIFF sec"
