#!/usr/bin/env python
# -*- coding: utf-8 -*-

import matplotlib as mpl
import matplotlib.pyplot as plt
import h5py
import numpy as np
import sys

from datetime import datetime
from matplotlib import dates

import pandas as pd

from scipy.fftpack import fft

from matplotlib.colors import LogNorm
from matplotlib.ticker import LogFormatterMathtext

mpl.rcParams['font.size'] = 12
mpl.rcParams["figure.figsize"] = [7.4, 4.8]
mpl.rcParams["lines.linewidth"] = 1.0
mpl.rcParams["figure.dpi"] = 600.

#idir = '/data/projects/f_image/lecointre/IWORMS/TEST_DONNEES_REELLES/GRAPHE_COMPLET_132_150_923/'
idir = '/summer/f_image/lecointre/IWORMS/TEST_DONNEES_REELLES/GRAPHE_COMPLET_132_150_923/'
ifile = 'out_39415926.h5'
#ifile = 'out_ref411.h5'

################################################################################

# Read meteo, time and temperature

temp = pd.read_pickle('KCAMOUNT4_ts_MayJune2014.pkl')

time=np.array(temp.DateUTC)
dt_x = dates.date2num([datetime.strptime(d, "%Y-%m-%d %H:%M:%S") for d in time])
#print(dt_x)
#print(dt_x[0:2])  # en jours
#print(86400*(dt_x[1]-dt_x[0]))

# 1 mai 2014 : jour 121
# dt_x[0] = 1 mai 2014 à 7h00 = 16191.29166667

dt_x = dt_x - dt_x[0] + 121 + 7./24.
####dt_x = dt_x - 735365 + 132 - 8./24. # cale sur temps iworms

# Attention ce sont des dates UTC
# Les données San Jacinto (Los Angeles) sont en UTC - 8h (en été)
dt_x = dt_x - 8./24.

ibeg = np.argmin(np.abs(dt_x-132))
iend = np.argmin(np.abs(dt_x-151))

TC=np.array(temp.TemperatureC)

print(ibeg)
print(iend)

fig = plt.figure()
ax20 = fig.add_subplot(312)
ax20.set_yticks([])
ax20.grid(axis='x')
ax2 = ax20.twinx()
ax2.plot(dt_x[ibeg:iend], TC[ibeg:iend], '-k')
#ax2.grid()
ax2.set_ylabel("Temperature (°C)",color='k')
ax2.tick_params('y', colors='k')
ax2.set_xlim((132,151))
ax2.set_xticks(np.arange(132,151))
ax2.set_xticklabels([])

###############################################################################

# spectre temperature sur toute la période ? ou sur les 19days ?
# bouche les trous meteo
#dt_x = dt_x - dt_x[0] + 121 + 7./24.   # le 1er element est le 2014-05-01 (2014121) 07:00:00
#dt_x_interp = np.arange(dt_x[0], dt_x[-1], 1./48.)  # step 30min = 1/48 day  # sur toute la periode
dt_x_interp = np.arange(dt_x[ibeg], dt_x[iend], 1./48.)  # step 30min = 1/48 day  # sur les 19days
Ntmeteo = len(dt_x_interp)
Fsmeteo = 1. / 1800. # 30min
frqmeteo = np.arange(Ntmeteo)/(Ntmeteo/Fsmeteo) # two sides frequency range
frqmeteo = frqmeteo[range(int(Ntmeteo/2))] # one side frequency range
print(Ntmeteo)
#print(frqmeteo)
print(np.shape(frqmeteo))
TCi = np.interp(dt_x_interp, dt_x, TC)
Y = np.fft.rfft(TCi)/len(dt_x_interp)
Y = Y[range(int(len(dt_x_interp)/2))]
specTCi = abs(Y)

##################################

# ref R3010, calcul sans abs(coh) et calcul sans elements diagonaux
iref=411
#h5f = h5py.File(idir+"/"+ifile, "r")
#Dt = np.transpose(h5f["Dt_ref"][0,:,:])
#Dtw = np.transpose(h5f["Dtw_ref"][0,:,:])
h5f = h5py.File(idir+"/"+ifile, "r")
Dt = np.transpose(h5f["Dt"][:,:])
Dtw = np.transpose(h5f["Dtw"][:,:])
Coh = np.transpose(h5f["Coh"][:,:])
h5f.close()

# temps iworms centré sur les pas de temps de 2h
time_iworms = np.arange(735365+1/24, 735384+5/(6*24)-11/(6*24), 1/(6*24))
time_iworms = time_iworms - 735365 + 132

print("time_iworms")
print(time_iworms)

# plot avec Dtw, Coh

plt.rcParams['xtick.bottom'] = plt.rcParams['xtick.labelbottom'] = False
plt.rcParams['xtick.top'] = plt.rcParams['xtick.labeltop'] = True
ax01 = fig.add_subplot(311)
ax01.plot(time_iworms, 1e3*np.nanmean(Dtw,axis=0), '-',color='grey', label="Dtw")
ax01.set_ylabel(r'$\widetilde{\Delta} (ms)$', color='grey')
ax01.tick_params('y', colors='grey')
ax01.set_xlim((132, 151))
ax01.set_xticks(np.arange(132,151))
#ax01.set_xticklabels(" ")
ax01.grid(axis='x')
ax02 = ax01.twinx()
ax02.plot(time_iworms,np.nanmean(Coh,axis=0),'-k', label="Coh")
ax02.set_ylabel(r'$Coh$', color='k')
ax02.tick_params('y', colors='k')
ax02.set_xlim((132, 151))
ax02.set_xticks(np.arange(132,151))
x2tklbl = ["132", "", 134, "", "136", "", "138", "", "140", "", "142", "", "144", "", "146", "", "148", "", "150"]
ax02.set_xticklabels(x2tklbl)
#ax02.grid()

#mpl.rcParams['lines.linewidth'] = 0.5
#mpl.rcParams['font.size'] = 8

xtk = np.array([1./(5*24*3600), 1./(3*24*3600), 1./(2*24*3600), 1./(24*3600), 1./(12*3600), 1./(8*3600), 1./(6*3600), 1./(3*3600), 1./(2*3600), 1./3600, 1./(2*600)])
xtklbl = ["5d", "3d", "2d", "1d", "12h", "8h", "6h", "3h", "2h", "1h", "20min"]


#####################################

Nt = 2725 # 36 # number of sample points
Nsta = 923 # 1108 # number of stations

time = 132.0 + np.arange(Nt)/144.0
tdt = time
Fs = 1./600. #sample spacing  # une mesure de Dt toute les 10min (toute les 600sec)
Ts = 1.0/Fs
frq = np.arange(Nt)/(Nt/Fs) # two sides frequency range
frq = frq[range(int(Nt/2))] # one side frequency range
print(frq)
print(np.shape(frq))

vsta=np.arange(Nsta)
Xg,Yg=np.meshgrid(frq,vsta)

ibeg = np.argmin(np.abs(frq-1./(5*24*3600)))
iend = np.argmin(np.abs(frq-1./(2*600)))

## ref R3010 , calcul sans abs(coh) et calcul sans elements diagonaux
iref=411
h5f = h5py.File(idir+"/"+ifile, "r")
absFlag = 0

ivar = 0
spectreMedian = np.nan*np.ones((4,int(Nt/2)))
cmin=np.array(([1e-5,1e-5,1e-3,1e-3]))
YY=np.nan*np.ones((4,Nsta,int(Nt/2)))
tYY=np.nan*np.ones((4,Nsta,Nt))
varname=[]
#var = "Dtw_ref"
var = "Dtw"
varname.append(var)
if var == "Coh":
    vv = np.transpose(h5f[var][:,:])
else:
    print(var)
    #vv = np.transpose(h5f[var][0,:,:])
    vv = np.transpose(h5f[var][:,:])

nsta = np.shape(vv)[0]
print("nsta",nsta)

boxD = np.mean(vv, axis=0)
Y = np.fft.rfft(boxD)/Nt
mY = abs(Y[range(int(Nt/2))])
mY = 930 * mY / np.max(mY)
for ista in np.arange(Nsta):
    boxD = vv[ista,:]
    tYY[ivar,ista,:]=vv[ista,:]
    Y = np.fft.rfft(boxD)/Nt
    Y = Y[range(int(Nt/2))]
    YY[ivar,ista,:] = abs(Y)
mY = np.nanmean(YY[ivar,:,:],axis=0)
#if var == "Dtw_ref":
if var == "Dtw":
    save_spec_Dtw = mY[ibeg:iend]
    save_frq_Dtw = frq[ibeg:iend]
ivar += 1
h5f.close()



print(np.shape(frqmeteo))
print(np.shape(specTCi))
print(np.shape(save_frq_Dtw))
print(np.shape(save_spec_Dtw))

plt.rcParams['xtick.bottom'] = plt.rcParams['xtick.labelbottom'] = True
plt.rcParams['xtick.top'] = plt.rcParams['xtick.labeltop'] = False
ax01 = fig.add_subplot(313)
ax01.plot(save_frq_Dtw, save_spec_Dtw, '-',color='grey', label=r"$\widetilde{\Delta}$")
ax01.set_ylabel(r'$\widetilde{\Delta}$ spectrum', color='grey')
ax01.tick_params('y', colors='grey',labelleft=False)
ax01.set_xlim((1./(5*24*3600), 1./(2*600)))
ax01.set_xticks(xtk, minor=False)
ax01.set_xticklabels(xtklbl, minor=False)
ax01.grid(axis='x')
ax02 = ax01.twinx()
ax02.semilogx(frqmeteo,specTCi,'-k', label="Temp")
ax02.set_ylabel('T° spectrum', color='k')
ax02.tick_params('y', colors='k',labelright=False)
ax02.set_xlim((1./(5*24*3600), 1./(2*600)))
ax02.set_ylim((0, 6))
ax02.set_xticks(xtk, minor=False)
ax02.set_xticklabels(xtklbl, minor=False)
ax02.set_xlabel('frequency')
ax02.text(1./(8*24*3600), 5, "c)")
ax02.text(1./(8*24*3600), 12, "b)")
ax02.text(1./(8*24*3600), 20, "a)")
ax02.text(1./(0.8*24*3600), 22.5, "Julian day of year 2014")
#ax02.grid()
plt.savefig('./SJ_temp_Dtw_Coh.png', bbox_inches=0)

###################################################################################


