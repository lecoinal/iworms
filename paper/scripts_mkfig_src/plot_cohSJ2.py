#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import h5py

# colormap tab10 with removing two colors
col = []
for index in range(10):
    if ( (index < 6) or (index > 7) ):
        col.append(list(plt.cm.tab10(index)))

mpl.rcParams["figure.figsize"] = [11.4, 7.0]
mpl.rcParams["lines.linewidth"] = 0.9 # 0.7
mpl.rcParams["figure.dpi"] = 600.
mpl.rcParams["font.size"] = 13.

if __name__ == '__main__':

    day0 = np.arange(132, 151, 1/144)

    print(np.shape(day0))
    ii = 0

    plt.clf()
    fid, ax = plt.subplots()
    plt.grid()

    iexp = '39473095'
    hf = h5py.File('mCoh/mCoh_'+iexp+'.h5','r')
    mc = hf['/mCoh'][:]
    hf.close()
    plt.plot(day0[0:np.size(mc)], mc,'-',color=col[ii],label="10min, [-2s:2s]")
    print('blue circles 19d', np.mean(mc))
    ii += 1

    iexp = '39482671'
    hf = h5py.File('mCoh/mCoh_'+iexp+'.h5','r')
    mc = hf['/mCoh'][:]
    hf.close()
    plt.plot(day0[0:np.size(mc)], mc,'-',color=col[ii],label="10min, [-1s:1s]")
    print('orange circles 19d', np.mean(mc))
    ii += 1

    iexp = '39482672'
    hf = h5py.File('mCoh/mCoh_'+iexp+'.h5','r')
    mc = hf['/mCoh'][:]
    hf.close()
    plt.plot(day0[0:np.size(mc)], mc,'-',color=col[ii],label="10min, [0:-1s] U [0:1s]")
    print('green circles 19d', np.mean(mc))
    ii += 1

    iexp = '39441205'
    hf = h5py.File('mCoh/mCoh_'+iexp+'.h5','r')
    mc = hf['/mCoh'][:]
    hf.close()
#    plt.plot(day0[0:np.size(mc)], mc,'-',color=col[ii],label=r"$10min, 2h, t_{zoom} [-2:2]sec$")
    print('red circles 19d', np.mean(mc))
    ii += 1

    iexp = '39441236'
    hf = h5py.File('mCoh/mCoh_'+iexp+'.h5','r')
    mc = hf['/mCoh'][:]
    hf.close()
#    plt.plot(day0[0:np.size(mc)], mc,'-',color=col[ii],label=r"$10min, 2h, t_{zoom} [-1:1]sec$")
    print('purple circles 19d', np.mean(mc))
    ii += 1

    iexp = '39415926'
    hf = h5py.File('mCoh/mCoh_'+iexp+'.h5','r')
    mc = hf['/mCoh'][:]
    hf.close()
    plt.plot(day0[0:np.size(mc)], mc,'-',color=col[ii],label="2h, [0:-1s] U [0:1s]")
    print('brown circles 19d', np.mean(mc))
    ii += 1

    iexp = '39441120'
    hf = h5py.File('mCoh/mCoh_'+iexp+'.h5','r')
    mc = hf['/mCoh'][:]
    hf.close()
#    plt.plot(day0[0:np.size(mc)], mc,'-',color=col[ii],label=r"$10min, 2h, [0:1]$")
    print('yellow circles 19d', np.mean(mc))
    ii += 1

    iexp = '39441171'
    hf = h5py.File('mCoh/mCoh_'+iexp+'.h5','r')
    mc = hf['/mCoh'][:]
    hf.close()
#    plt.plot(day0[0:np.size(mc)], mc,'-',color=col[ii],label=r"$10min, 2h, [0:-1]$")
    print('cyan circles 19d', np.mean(mc))
    ii += 1

    plt.legend(ncol=2, loc=9)
    plt.xlabel("day of year 2014")
    plt.ylabel(r"425503-pair averaged coherence ($coh$)")
    plt.xlim([131.9,151.1])
    plt.ylim([0.0,1.0])
    plt.xticks(np.arange(132,151),rotation='vertical')

#    rect = plt.Rectangle((128, 0.0), 4, 1.0, color='k', alpha=0.2)
#    ax.add_patch(rect)
#    rect = plt.Rectangle((151, 0.0), 8, 1.0, color='k', alpha=0.2)
#    ax.add_patch(rect)

    plt.savefig("coh_paramdelta_SJ.png",dpi=600)
    
