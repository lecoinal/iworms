#!/usr/bin/env python
# -*- coding: utf-8 -*-

# sur ist-oar
# source /soft/env.bash
# module load python/python3.7
# python3 spatial_meanstd.py Dtw 

# sur gricad clusters
# source /applis/site/guix-start.sh
# refresh_guix pyplot
# python3 spatial_meanstd.py Dtw

import matplotlib.pyplot as plt
import h5py
import numpy as np
import sys

import scipy.io as sio

import matplotlib as mpl
mpl.rcParams["figure.dpi"] = 600.

rep = "./"

var = sys.argv[1] # Dt ou Dtw ou Coh

iref=55 # 411

#h5f = h5py.File("/bettik/lecoinal/RESOLVE/TIMEERRORS/EXP020/Dt_115_147.h5", "r")
h5f = h5py.File("/bettik/lecoinal/RESOLVE/TIMEERRORS/EXP023/Dt_115_147.h5", "r")
#h5f = h5py.File("/bettik/lecoinal/oar.71491439/out.h5", "r")
vv = h5f[var][:,:]
h5f.close()

remove_1d=False

if remove_1d:
    from scipy import signal
## remove 1d notch frequency
## notch filter to remove 24h frequency
    fs = 1.0 / (10.0 * 60.0)   # Sample frequency (Hz)
    f0 = 1.0 / (24.0 * 3600.0) # Frequency to be removed from signal (Hz)
    Q = 30.0  # Quality factor
## Design notch filter
    b, a = signal.iirnotch(f0, Q, fs)
## remove 1d and 2d
#    f0 = 1.0 / (2.0 * 24.0 * 3600.0) # Frequency to be removed from signal (Hz)
#    bb, aa = signal.iirnotch(f0, Q, fs)
## remove all under 1d
#    f0 = 1.0 / (24.0 * 3600.0) # Frequency to be removed from signal (Hz)
#    nyq = fs*0.5
#    bbb, aaa = signal.butter(4, f0/nyq, btype="high")
    nsta = np.shape(vv)[1]   # number of stations or number of pairs
    print("nsta or npair",nsta)
    for ista in np.arange(nsta):
        tmp = signal.filtfilt(b, a, vv[:,ista])
        #tmp2 = signal.filtfilt(bb, aa, tmp)
        #tmp3 = signal.filtfilt(bbb, aaa, tmp2)
        vv[:,ista] = tmp ## tmp3




mat_contents = sio.loadmat("/bettik/PROJECTS/pr-resolve/lecoinal/ZENODO_UGO/DATA_NANNI_etal_2020TC.mat")
print(sorted(mat_contents.keys()))
data_cat = mat_contents['data_cat']
tt_met = data_cat[8,:]-365 # +1 ici je pense quil y a une erreur ds le readme sur zenodo, le 01/01/17 c'est l'indice 1, pas l'indice 0...
print(np.shape(tt_met))
precip = data_cat[0,:]
sub_water_discharge = data_cat[5,:]

mat_contents = sio.loadmat("/bettik/PROJECTS/pr-resolve/lecoinal/ZENODO_UGO/ARG_Temp_degC.mat")
data_temp = mat_contents['ARG_Temp_degC']
mat_contents = sio.loadmat("/bettik/PROJECTS/pr-resolve/lecoinal/ZENODO_UGO/ARG_Tdoy_meteo.mat")
data_tdoy = mat_contents['ARG_Tdoy_meteo']
tt_temp = data_tdoy-365 # +1  ???

mat_contents = sio.loadmat("/bettik/PROJECTS/pr-resolve/lecoinal/ZENODO_UGO/ARG_sliding_vel_mmperhour_dt5sec.mat")
#print(sorted(mat_contents.keys()))
slvel = mat_contents['ARG_sliding_vel_mmperhour_dt5sec']
mat_contents = sio.loadmat("/bettik/PROJECTS/pr-resolve/lecoinal/ZENODO_UGO/ARG_sliding_time_5sec_secfrom2017.mat")
#print(sorted(mat_contents.keys()))
slt = (mat_contents['ARG_sliding_time_5sec_secfrom2017']-365*86400)/86400
#print(np.shape(slt))
#print(slt)

tt = np.linspace(115, 148, 4735+1, endpoint=True)

reorder = True
if (var == "Dtw") or (var == "Dt") or (var == "Coh"):
    yy = np.arange(1,99+1)
    ymax=100 # 99
    if reorder:
        # reorder vv along distance from reference station
        gr = np.loadtxt('/bettik/lecoinal/RESOLVE/METRIC/zreseau_ZO_2018_115.txt',usecols = (1,2,3))
        x = gr[:,0]
        y = gr[:,1]
        z = gr[:,2]
        iref=55
        dist = 1e-3*np.sqrt((x[:]-x[iref])**2 + (y[:]-y[iref])**2 + (z[:]-z[iref])**2)
        isort = np.argsort(dist)
        dist = dist[isort]
        ntim = np.shape(vv)[0]   # number of time
        for itim in np.arange(ntim):
            vv[itim,:] = vv[itim,isort]
        #yy[0:-1] = dist
else:
    yy = np.arange(1,4754+1)
    ymax=4800 # 4754

cmax = np.nanmax(np.abs(vv))
cmin = np.nanmin(vv)

print('cmin,cmax:',cmin,cmax)

xmin=114.9
xmax=148.1
ymin=0

print(np.shape(tt))
print(np.shape(yy))
print(np.shape(vv))

plt.clf()
fig = plt.figure()
ax = fig.add_subplot(111) # , projection='3d', azim=-60, elev=55)
if (var == "Dtw") or (var == "Dt") or (var == "dt"):
    plt.rcParams['image.cmap'] = 'bwr'
    #cax = plt.pcolormesh(tt,yy,vv.transpose(), vmin=-0.65*cmax, vmax=0.65*cmax)
    cax = plt.pcolormesh(tt,yy,vv.transpose(), vmin=-0.02, vmax=0.02)
#    clr1 = 'green'
#    clr2 = 'grey'
#    clr3 = 'yellow'
else:
    plt.rcParams['image.cmap'] = 'gist_rainbow'
    #cax = plt.pcolormesh(tt,yy,vv.transpose(), vmin=cmin, vmax=cmax)
    if (var == "Coh"):
        cax = plt.pcolormesh(tt,yy,vv.transpose(), vmin=0.4, vmax=cmax)
    else:
        cax = plt.pcolormesh(tt,yy,vv.transpose(), vmin=0.0, vmax=cmax)
#    clr1 = 'white'
#    clr2 = 'white'
#    clr3 = 'white'

clr3 = 'black'
clr1 = 'green'
clr2 = 'grey'
clr0 = 'yellow'

plt.plot(tt_met,ymax+(0.2*ymax)*precip[:]/np.max(precip), linewidth=0.5, color=clr1, label='precip') # precip
plt.plot(tt_met,ymax+(0.2*ymax)*sub_water_discharge[:]/np.max(sub_water_discharge), linewidth=0.3, color=clr2, label='sub_water_discharge') # sub_water_discharge
plt.plot(tt_temp,ymax+(0.1*ymax)*data_temp[:]/np.max(data_temp), linewidth=0.3, color=clr3, label='temp')

s1d = int(np.floor(np.max(tt_temp)))-int(np.floor(np.min(tt_temp)))
data_maxt = np.zeros(s1d)
time_maxt = np.zeros(s1d)
icpt = 0
for iitt in np.arange(np.floor(np.min(tt_temp)),np.floor(np.max(tt_temp))):
    # select only one day temp
    t1d = data_temp[np.where((tt_temp >= iitt) & (tt_temp < iitt+1))]
    if t1d.size > 0:
        # find max and argmax
        vmax = np.max(t1d)
        amax = np.argmax(t1d)
        # find corresponding time
        t1d = tt_temp[np.where((tt_temp >= iitt) & (tt_temp < iitt+1))]
        tmax = t1d[amax]
        data_maxt[icpt] = vmax
        time_maxt[icpt] = tmax
        icpt += 1
ax.plot(time_maxt,ymax+(0.1*ymax)*data_maxt/np.max(data_temp), color='r',marker='o',ms=3,mfc="None",linestyle="None",markeredgewidth=0.3)
from matplotlib.ticker import MultipleLocator
ax.xaxis.set_minor_locator(MultipleLocator(1))
secax = ax.secondary_xaxis('top')
secax.set_xticklabels([])
secax.xaxis.set_minor_locator(MultipleLocator(1))
#ax.grid(which='both', axis='x', linestyle='--', linewidth=1)

#plt.plot(slt,ymax+(0.1*ymax)*slvel[:]/np.max(slvel), linewidth=0.3, color=clr0, label='sliding velocity')
plt.legend(loc='upper right', bbox_to_anchor=(1.0, 1.15), fontsize='xx-small')

if (var == "Dtw"):
    plt.title(r'$\widetilde{\Delta}$ (s)', loc='left')
elif (var == "Dt"):
    plt.title(r'$\Delta$ (s)', loc='left')
elif (var == "Coh"):
    plt.title(r'Coh', loc='left')
elif (var == "dt"):
    plt.title(r'$\delta$ (s)', loc='left')
else:
    plt.title(r'coh', loc='left')
plt.colorbar(cax)
plt.axis([xmin, xmax, ymin, 1.1*ymax])

plt.savefig('./'+var+'.png', dpi=600, bbox_inches=0)


