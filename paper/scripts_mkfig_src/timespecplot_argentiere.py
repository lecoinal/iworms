#!/usr/bin/env python
# -*- coding: utf-8 -*-

# sur ist-oar
# source /soft/env.bash
# module load python/python3.7
# python3 spatial_meanstd.py Dtw 

# sur gricad clusters
# source /applis/site/guix-start.sh
# refresh_guix pyplot
# python3 spatial_meanstd.py Dtw

import matplotlib.pyplot as plt
import h5py
import numpy as np
import sys

from matplotlib.colors import LogNorm
from matplotlib.ticker import LogFormatterMathtext


import scipy.io as sio

import matplotlib as mpl
mpl.rcParams["figure.dpi"] = 600.
mpl.rcParams["lines.linewidth"] = 0.7

rep = "./"

var = sys.argv[1] # Dt ou Dtw ou Coh

iref=55 # 411

#h5f = h5py.File("/bettik/lecoinal/RESOLVE/TIMEERRORS/EXP020/Dt_115_147.h5", "r")
h5f = h5py.File("/bettik/lecoinal/RESOLVE/TIMEERRORS/EXP023/Dt_115_147.h5", "r")
#h5f = h5py.File("/bettik/lecoinal/oar.71491439/out.h5", "r")
vv = h5f[var][:,:]
h5f.close()



remove_1d=False

if remove_1d:
    from scipy import signal
## remove 1d notch frequency
## notch filter to remove 24h frequency
    fs = 1.0 / (10.0 * 60.0)   # Sample frequency (Hz)
    f0 = 1.0 / (24.0 * 3600.0) # Frequency to be removed from signal (Hz)
    Q = 30.0  # Quality factor
## Design notch filter
    b, a = signal.iirnotch(f0, Q, fs)
## remove 1d and 2d
    f0 = 1.0 / (2.0 * 24.0 * 3600.0) # Frequency to be removed from signal (Hz)
    bb, aa = signal.iirnotch(f0, Q, fs)
## remove all under 1d
    f0 = 1.0 / (24.0 * 3600.0) # Frequency to be removed from signal (Hz)
    nyq = fs*0.5
    bbb, aaa = signal.butter(4, f0/nyq, btype="high")
    nsta = np.shape(vv)[1]   # number of stations or number of pairs
    print("nsta or npair",nsta)
    for ista in np.arange(nsta):
        tmp = signal.filtfilt(b, a, vv[:,ista])
        tmp2 = signal.filtfilt(bb, aa, tmp)
        tmp3 = signal.filtfilt(bbb, aaa, tmp2)
        vv[:,ista] = tmp3 ## tmp3




mat_contents = sio.loadmat("/bettik/PROJECTS/pr-resolve/lecoinal/ZENODO_UGO/DATA_NANNI_etal_2020TC.mat")



mat_contents = sio.loadmat("/bettik/PROJECTS/pr-resolve/lecoinal/ZENODO_UGO/ARG_Temp_degC.mat")
data_temp = mat_contents['ARG_Temp_degC']
mat_contents = sio.loadmat("/bettik/PROJECTS/pr-resolve/lecoinal/ZENODO_UGO/ARG_Tdoy_meteo.mat")
data_tdoy = mat_contents['ARG_Tdoy_meteo']
tt_temp = data_tdoy-365 # +1  ???
print('tt_temp',tt_temp)

ibeg = np.argmin(np.abs(tt_temp-115))
iend = np.argmin(np.abs(tt_temp-148))
t_temp = tt_temp[ibeg:iend]
temp = data_temp[ibeg:iend]
Ntmeteo = len(t_temp)
Fsmeteo = 1. / 1800. # 30min
frqmeteo = np.arange(Ntmeteo)/(Ntmeteo/Fsmeteo) # two sides frequency range
frqmeteo = frqmeteo[range(int(Ntmeteo/2))] # one side frequency range
Y = np.fft.rfft(temp)/Ntmeteo
specmeteo = abs(Y[range(int(Ntmeteo/2))])



tt = np.linspace(115, 148, 4735, endpoint=False)

xmin=114.9
xmax=148.1
Nsta = 98
Nt = 4735 
Fs = 1./600. #sample spacing  # une mesure de Dt toute les 10min (toute les 600sec)
frq = np.arange(Nt)/(Nt/Fs) # two sides frequency range
frq = frq[range(int(Nt/2))] # one side frequency range

ibeg = np.argmin(np.abs(frq-1./(5*24*3600)))  # 5-jours
iend = np.argmin(np.abs(frq-1./(2*600)))      # 20-min

save_frq = frq[ibeg:iend]
#save_spec=np.nan*np.ones((Nsta,int(Nt/2)))
save_spec=np.nan*np.ones((Nsta,iend-ibeg))

if var == 'Dt':
   cmin = 1e-7
   cmax = 1e-3
elif var == 'Dtw':
   cmin = 1e-7
   cmax = 1e-3
else:
   cmin = 1e-5
   cmax = 1e-2


for ista in np.arange(Nsta):

    #print(ista)
    vvv = vv[:,ista]
    vvm = np.nanmean(vvv)
    print(ista,vvm)
#    if np.isnan(vvv).any():
#        vvv[np.isnan(vvv)] = vvm

    Y = np.fft.rfft(vvv)/Nt
    mY = abs(Y[range(int(Nt/2))])
    save_spec[ista,:] = mY[ibeg:iend]
    if np.isnan(save_spec[ista,:]).any():
        print(ista,save_spec[ista,:])
        print(Y)
        print(vvv)

xtk = np.array([1./(5*24*3600), 1./(3*24*3600), 1./(2*24*3600), 1./(24*3600), 1./(12*3600), 1./(9*3600), 1./(6*3600), 1./(3*3600), 1./(2*3600), 1./3600, 1./(2*600)])
xtklbl = ["5d", "3d", "2d", "1d", "12h", "9h", "6h", "3h", "2h", "1h", "20mn"]

plt.clf()
#plt.pcolor
im01 = plt.pcolormesh(save_frq,np.arange(Nsta),save_spec[:,:], cmap='gray_r', norm=LogNorm(vmin=cmin, vmax=cmax))
plt.xscale('log')
plt.xlim(1./(5*24*3600), 1./(2*600))
plt.ylim(0, 100) #1110)
plt.xticks(xtk, xtklbl)
plt.ylabel('station index')
plt.xlabel('frequency')
plt.title(var)
plt.colorbar(im01, orientation='horizontal',format=LogFormatterMathtext())
# median des spectres
#ax02 = plt.twinx()
mean_spec = np.nanmean(save_spec[:,:],axis=0)
plt.plot(save_frq,100*mean_spec/np.max(mean_spec),'-r',label="98 stations averaged spectrum (arbitrary unit)")
#ax02.set_xticks(xtk, xtklbl)
#ax02.set_xlim(1./(5*24*3600), 1./(2*600))
#ax02.set_ylabel("98 stations-averaged spectrum", color='r')
#ax02.tick_params('y', colors='r')

plt.plot(frqmeteo,100*specmeteo/np.max(specmeteo),'-b',label="temperature spectrum (arbitrary unit)")

#ax02.plot(frqmeteo,np.max(np.nanmean(save_spec[:,:],axis=0))*specmeteo/np.max(specmeteo),'-b',label="temp (arbitrary unit)")
#ax02.set_ylabel("Temperature spectrum", color='b')
#ax02.tick_params('y', colors='b')
#plt.xlim(1./(5*24*3600), 1./(2*600))

plt.legend(loc='upper right', bbox_to_anchor=(1.0, 1.15), fontsize='xx-small')

plt.savefig('./ARG_spec_'+var+'.png', bbox_inches=0)






#    ymax=np.nanmax(vv[:,ista])
#    ymin=np.nanmin(vv[:,ista])
#    plt.clf()
#    plt.figure(figsize=(6, 3))
#    plt.plot(tt,vv[:,ista])
#    clr1 = 'green'
#    clr2 = 'grey'
#    clr3 = 'yellow'
#    plt.plot(tt_temp,ymax*data_temp[:]/np.max(data_temp), color=clr3) # sub_water_discharge
#    plt.xticks(np.arange(115,148),rotation='vertical')
#    plt.grid(True)
#    if (var == "Dtw"):
#        plt.title(r'$\widetilde{\Delta}$ (s)') #, loc='right')
#    elif (var == "Dt"):
#        plt.title(r'$\Delta$ (s)')
#    elif (var == "Coh"):
#        plt.title(r'Coh')
#    plt.axis([xmin, xmax, ymin, ymax])
#
#    plt.savefig('./'+var+'_'+str(ista)+'.png', dpi=600, bbox_inches=0)
