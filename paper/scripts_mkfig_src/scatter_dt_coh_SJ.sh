#!/bin/bash
#OAR --project iste-equ-ondes
#OAR -n scatter_Dt_coh
#OAR -l /nodes=1/core=1,walltime=01:05:00
##OAR -t besteffort

# oarsub -S "./scatter_dt_coh_SJ.sh"

set -e

source /soft/env.bash

date

START=$(date +%s.%N)

cat $OAR_NODE_FILE

##################################

PEXE="scatter_dt_coh_SJ.py"

module load python/python3.9

python3.9 -u $PEXE 

END=$(date +%s.%N)
DIFF=$(echo "$END - $START" | bc)
echo "ElapsedTime: $DIFF sec"

