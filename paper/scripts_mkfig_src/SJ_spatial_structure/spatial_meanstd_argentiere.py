#!/usr/bin/env python
# -*- coding: utf-8 -*-

# sur ist-oar
# source /soft/env.bash
# module load python/python3.7
# python3 spatial_meanstd_argentiere.py Dtw 

# sur gricad clusters
# source /applis/site/guix-start.sh
# refresh_guix pyplot
# python3 spatial_meanstd_argentiere.py Dtw

# montage -geometry 3840x2880 -tile 3x2 mean_Coh.png mean_Dtw.png mean_Dt.png std_Coh.png std_Dtw.png std_Dt.png EXP023.png

import matplotlib.pyplot as plt
import h5py
import numpy as np
import sys
import scipy.io as sio

import matplotlib as mpl
mpl.rcParams["figure.dpi"] = 600.
mpl.rcParams["contour.linewidth"] = 0.7

rep = "./"

var = sys.argv[1] # Dt ou Dtw ou Coh


# topo (bedrock) and ice thickness
mat_contents = sio.loadmat("/bettik/PROJECTS/pr-resolve/lecoinal/ZENODO_UGO/NANNI_thickness_argentiere.mat")
print(sorted(mat_contents.keys()))
Xtopo = mat_contents['X']
Ytopo = mat_contents['Y']
bedrock = mat_contents['Zbed_christian']
thickness = mat_contents['thickness']

gr = np.loadtxt('/bettik/lecoinal/RESOLVE/METRIC/zreseau_ZO_2018_115.txt',usecols = (1,2,3))

print(np.shape(gr))
print(np.min(gr[:,0]), np.max(gr[:,0]))
print(np.min(gr[:,1]), np.max(gr[:,1]))
xmin = np.min(gr[:,0])-10 # 0.0001
xmax = np.max(gr[:,0])+10 # 0.0001
ymin = np.min(gr[:,1])-10 # 0.0001
ymax = np.max(gr[:,1])+10 # 0.0001
xyax = [xmin, xmax, ymin, ymax]

x = gr[:,0]
y = gr[:,1]
z = gr[:,2] 

## ref R3010, calcul avec abs(coh) et calcul avec elements diagonaux
#h5f = h5py.File(rep+"/out_923.h5", "r")
#vvm = np.nanmean(h5f[var][:,:], axis=1)
#vvs = np.nanstd(h5f[var][:,:], axis=1)
#iref=411
#h5f.close()

# ref R3010, calcul sans abs(coh) et calcul sans elements diagonaux

iref=55 # 411
##iref=0
#h5f = h5py.File(rep+"/out_ref"+str(iref)+".h5", "r")
#if (var == "Dtw_ref") or (var == "Dt_ref"):
#    vv = h5f[var][0,:,:]
#else:
#    vv = h5f[var][:,:]
#h5f.close()

h5f = h5py.File("/bettik/lecoinal/RESOLVE/TIMEERRORS/EXP023/Dt_115_147.h5", "r")
vv = h5f[var][:,:]
h5f.close()

## remove 1d notch frequency
## notch filter to remove 24h frequency
#from scipy import signal
#fs = 1.0 / (10.0 * 60.0)   # Sample frequency (Hz)
#f0 = 1.0 / (24.0 * 3600.0) # Frequency to be removed from signal (Hz)
#Q = 30.0  # Quality factor
## Design notch filter
#b, a = signal.iirnotch(f0, Q, fs)
## remove 1d and 2d
#f0 = 1.0 / (2.0 * 24.0 * 3600.0) # Frequency to be removed from signal (Hz)
#bb, aa = signal.iirnotch(f0, Q, fs)
### remove all under 1d
##f0 = 1.0 / (24.0 * 3600.0) # Frequency to be removed from signal (Hz)
##nyq = fs*0.5
##bbb, aaa = signal.butter(4, f0/nyq, btype="high")
#

#nsta = np.shape(vv)[1]
#print("nsta",nsta)
#for ista in np.arange(nsta):
#    tmp = signal.filtfilt(b, a, vv[:,ista])
#    tmp2 = signal.filtfilt(bb, aa, tmp)
##    tmp3 = signal.filtfilt(bbb, aaa, tmp2)
#    vv[:,ista] = tmp2




vvm = np.nanmean(vv, axis=0)
vvs = np.nanstd(vv, axis=0)

#ytk = np.array([33.536, 33.538, 33.540, 33.542])
#ytklbl = ["33.536°", "33.538°", "33.540°", "33.542°"]
#xtk = np.array([-116.594, -116.592, -116.590, -116.588])
#xtklbl = ["-116.594°", "-116.592°", "-116.590°", "-116.588°"]

if (var == "Dtw") or (var == "Dt"):

    plt.rcParams['image.cmap'] = 'bwr'
    plt.clf()
    fig = plt.figure()
    ax = fig.add_subplot(111) # , projection='3d', azim=-60, elev=55)
    plt.contour(Xtopo, Ytopo, thickness, np.array([50, 100, 150, 200, 250]), colors='k')
    plt.contour(Xtopo, Ytopo, bedrock, np.array([2000, 2050, 2100, 2150, 2200, 2250, 2300, 2350, 2400, 2450, 2500]), colors='g')
    plt.contour(Xtopo, Ytopo, thickness+bedrock, np.arange(2000, 3000, 10), colors='b')
    plt.plot(gr[iref,0], gr[iref,1],'ok',mfc='none')
    #cmax = 0.5*np.max(np.abs(vvm[:]))
    print("maxabs mean",np.max(np.abs(vvm[:])))
    cmax = 1e-3 # 3e-5 # 1e-3
    #cmax = np.max(np.abs(vvm[:]))
    cax = plt.scatter(gr[:,0], gr[:,1], s=30, c=vvm[:], vmin=-cmax, vmax=cmax, edgecolor='none')
    #plt.scatter(gr[iref,0], gr[iref,1], s=15, c=vvm[iref], marker="*", vmin=-cmax, vmax=cmax, edgecolor='none')
    if (var == "Dtw"):
        plt.title(r'33-day averaged $\widetilde{\Delta}$ (s)') #, loc='right')
    else:
        plt.title(r'33-day averaged $\Delta$ (s)')
    tck = 1e-4*np.array([-10 , -8, -6, -4, -2, 0, 2, 4, 6, 8, 10])
    plt.colorbar(cax, ticks=tck)

    plt.axis(xyax)
    #plt.xticks(rotation=30)
    plt.tight_layout()
#    ax.set_yticks(ytk, minor=False)
#    ax.set_yticklabels(ytklbl, minor=False)
#    ax.set_xticks(xtk, minor=False)
#    ax.set_xticklabels(xtklbl, minor=False)

    plt.savefig('./mean_'+var+'.png', dpi=600, bbox_inches=0)

    plt.rcParams['image.cmap'] = 'gist_rainbow'
    #print(vvs[iref])
    #vvs[iref]=9999.
    #cmin=np.min(vvs)
    #vvs[iref]=0.0
    #cmax=np.max(vvs)
    print("maxstd",np.max(vvs[:]))
    cmax = np.max(vvs[:])
    vvs[iref]=9999.
    print("minstd",np.min(vvs[:]))
    cmin = np.min(vvs[:])
    vvs[iref]=0.
#    cmin = 1.0e-3
#    cmax = 4.5e-3
    plt.clf()
    fig = plt.figure()
    ax = fig.add_subplot(111) # , projection='3d', azim=-60, elev=55)
    plt.contour(Xtopo, Ytopo, thickness, np.array([50, 100, 150, 200, 250]), colors='k')
    plt.contour(Xtopo, Ytopo, bedrock, np.array([2000, 2050, 2100, 2150, 2200, 2250, 2300, 2350, 2400, 2450, 2500]), colors='g')
    plt.contour(Xtopo, Ytopo, thickness+bedrock, np.arange(2000, 3000, 10), colors='b')
    plt.plot(gr[iref,0], gr[iref,1],'ok',mfc='none')
    plt.scatter(gr[:,0], gr[:,1], s=30, c=vvs[:], vmin=cmin, vmax=cmax, edgecolor='none')
    #plt.scatter(gr[iref,0], gr[iref,1], s=15, c=vvs[iref], marker="*", vmin=cmin, vmax=cmax, edgecolor='none')
    if (var == "Dtw"):
        plt.title(r'33-day standard deviation $\widetilde{\Delta}$ (s)') #, loc='right')
    else:
        plt.title(r'33-day standard deviation $\Delta$ (s)')
    plt.colorbar()
    plt.axis(xyax)
    #plt.xticks(rotation=30)
    plt.tight_layout()
#    ax.set_yticks(ytk, minor=False)
#    ax.set_yticklabels(ytklbl, minor=False)
#    ax.set_xticks(xtk, minor=False)
#    ax.set_xticklabels(xtklbl, minor=False)
    plt.savefig('./std_'+var+'.png', dpi=600, bbox_inches=0)

else:

    print("min",np.min(vvm[:]))
    print("max",np.max(vvm[:]))

    plt.rcParams['image.cmap'] = 'gist_rainbow'

#    cmin = -2e-4 # 0.48
#    cmax = 12e-4 # 0.74
    cmin = np.min(vvm[:]) # 0.48
    cmax = np.max(vvm[:]) # 0.74
    plt.clf()
    fig = plt.figure()
    ax = fig.add_subplot(111) # , projection='3d', azim=-60, elev=55)
    plt.contour(Xtopo, Ytopo, thickness, np.array([50, 100, 150, 200, 250]), colors='k')
    plt.contour(Xtopo, Ytopo, bedrock, np.array([2000, 2050, 2100, 2150, 2200, 2250, 2300, 2350, 2400, 2450, 2500]), colors='g')
    plt.contour(Xtopo, Ytopo, thickness+bedrock, np.arange(2000, 3000, 10), colors='b')
    plt.plot(gr[iref,0], gr[iref,1],'ok',mfc='none')
    plt.scatter(gr[:,0], gr[:,1], s=30, c=vvm[:], vmin=cmin, vmax=cmax, edgecolor='none') # , vmin=cmin, vmax=cmax)
    #plt.scatter(gr[iref,0], gr[iref,1], s=15, c=vvm[iref], marker="*", vmin=cmin, vmax=cmax, edgecolor='none') # , vmin=cmin, vmax=cmax)
    plt.title(r'33-day averaged coherence $Coh$') #, loc='right')
    plt.colorbar()
    plt.axis(xyax)
    #plt.xticks(rotation=30)
    plt.tight_layout()
#    ax.set_yticks(ytk, minor=False)
#    ax.set_yticklabels(ytklbl, minor=False)
#    ax.set_xticks(xtk, minor=False)
#    ax.set_xticklabels(xtklbl, minor=False)
    plt.savefig('./mean_'+var+'.png', dpi=600, bbox_inches=0)

    plt.clf()
    plt.hist(vvm)
    plt.savefig('./histmean_'+var+'.png', dpi=200, bbox_inches=0)

    print("min",np.min(vvs[:]))
    print("max",np.max(vvs[:]))

    cmin = np.min(vvs[:]) # 0.03 # 0.09
    cmax = np.max(vvs[:]) # 0.07 # 0.16
    plt.clf()
    fig = plt.figure()
    ax = fig.add_subplot(111) # , projection='3d', azim=-60, elev=55)
    plt.contour(Xtopo, Ytopo, thickness, np.array([50, 100, 150, 200, 250]), colors='k')
    plt.contour(Xtopo, Ytopo, bedrock, np.array([2000, 2050, 2100, 2150, 2200, 2250, 2300, 2350, 2400, 2450, 2500]), colors='g')
    plt.contour(Xtopo, Ytopo, thickness+bedrock, np.arange(2000, 3000, 10), colors='b')
    plt.plot(gr[iref,0], gr[iref,1],'ok',mfc='none')
    plt.scatter(gr[:,0], gr[:,1], s=30, c=vvs[:], vmin=cmin, vmax=cmax, edgecolor='none') # , vmin=cmin, vmax=cmax)
    #plt.scatter(gr[iref,0], gr[iref,1], s=15, c=vvs[iref], marker="*", vmin=cmin, vmax=cmax, edgecolor='none') # , vmin=cmin, vmax=cmax)
    plt.title(r'33-day standard deviation Coherence')
    plt.colorbar()
    plt.axis(xyax)
    #plt.xticks(rotation=30)
    plt.tight_layout()
#    ax.set_yticks(ytk, minor=False)
#    ax.set_yticklabels(ytklbl, minor=False)
#    ax.set_xticks(xtk, minor=False)
#    ax.set_xticklabels(xtklbl, minor=False)
    #ax.set_xlim((-116.596, -116.588))
    #ax.set_ylim((33.536, 33.542))
    plt.savefig('./std_'+var+'.png', dpi=600, bbox_inches=0)

    plt.clf()
    plt.hist(vvs)
    plt.savefig('./histstd_'+var+'.png', dpi=200, bbox_inches=0)

