#!/bin/bash
#OAR --project resolve
#OAR -n make_movie
#OAR -l /nodes=1/core=1,walltime=3:00:00

set -e

source /applis/site/guix-start.sh
refresh_guix pyplot

#ffmpeg           	4.4.1	out	/gnu/store/bdkkqh9lwx8c1wl4a2kr85ll1mzfac8q-ffmpeg-4.4.1
#python-matplotlib	3.5.1	out	/gnu/store/inlr765y2qwvlkr5f8zzdhrp0lvmgkjx-python-matplotlib-3.5.1
#python-h5py      	3.6.0	out	/gnu/store/68nggkk7rxkmxn8k6iadh3fiqyb9cqvs-python-h5py-3.6.0
#python           	3.9.9	out	/gnu/store/sz7lkmic6qrhfblrhaqaw0fgc4s9n5s3-python-3.9.9



date

START=$(date +%s.%N)

cat $OAR_NODE_FILE

var=$1 # Dt, Dtw, Coh

PEXE="spatial_movie_argentiere.py"

TMPDIR=/$SHARED_SCRATCH_DIR/$USER/oar.$OAR_JOB_ID
mkdir -p $TMPDIR
cp $PEXE $TMPDIR/.
cd $TMPDIR

python3 -u $PEXE $var

# ffmpeg
ffmpeg -r 15/1 -i ${var}_%04d.png -c:v libx264 -vf "fps=25,format=yuv420p" ${var}.mp4

rm *png

END=$(date +%s.%N)
DIFF=$(echo "$END - $START" | bc)
echo "ElapsedTime: $DIFF sec"
