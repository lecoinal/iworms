#!/usr/bin/env python
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import h5py
import numpy as np
import sys
import time
import scipy.io as sio

import matplotlib as mpl
mpl.rcParams["figure.dpi"] = 600.

#mat_contents = sio.loadmat("/data/projects/f_image/lecointre/IWORMS/TEST_DONNEES_REELLES/tomo/tomo_SJFZ_multifreq.mat")
mat_contents = sio.loadmat("./tomo.mat")

fault = mat_contents['fault']

rep = "./"

var = sys.argv[1] # Dt ou Dtw ou Coh

plt.rcParams['image.cmap'] = 'bwr'

gr = np.loadtxt('./xyz.txt')

xmin = np.min(gr[:,0])-0.0001
xmax = np.max(gr[:,0])+0.0001
ymin = np.min(gr[:,1])-0.0001
ymax = np.max(gr[:,1])+0.0001
xyax = [xmin, xmax, ymin, ymax]

x = gr[:,0]
y = gr[:,1]
z = gr[:,2] 

xgrid = np.linspace(x.min(), x.max(), np.shape(gr)[0])
ygrid = np.linspace(y.min(), y.max(), np.shape(gr)[0])
xgrid, ygrid = np.meshgrid(xgrid, ygrid)
from scipy import interpolate
zgrid = interpolate.griddata((x,y),z, (xgrid, ygrid))
levels = np.arange(1400, 1500, 5)
levelslbl = np.arange(1400, 1500, 20)

# ref R3010, calcul sans abs(coh) et calcul sans elements diagonaux
iref=411

#h5f = h5py.File("/data/projects/f_image/lecointre/IWORMS/TEST_DONNEES_REELLES/GRAPHE_COMPLET_132_150_923/out_39415926.h5", "r")
h5f = h5py.File("./in.h5", "r")
vv = h5f[var][:,:]
h5f.close()

## remove 1d notch frequency
## notch filter to remove 24h frequency
#from scipy import signal
#fs = 1.0 / (10.0 * 60.0)   # Sample frequency (Hz)
#f0 = 1.0 / (24.0 * 3600.0) # Frequency to be removed from signal (Hz)
#Q = 30.0  # Quality factor
## Design notch filter
#b, a = signal.iirnotch(f0, Q, fs)
## remove 1d and 2d
#f0 = 1.0 / (2.0 * 24.0 * 3600.0) # Frequency to be removed from signal (Hz)
#bb, aa = signal.iirnotch(f0, Q, fs)
### remove all under 1d
##f0 = 1.0 / (24.0 * 3600.0) # Frequency to be removed from signal (Hz)
##nyq = fs*0.5
##bbb, aaa = signal.butter(4, f0/nyq, btype="high")
#

#nsta = np.shape(vv)[1]
#print("nsta",nsta)
#for ista in np.arange(nsta):
#    tmp = signal.filtfilt(b, a, vv[:,ista])
#    tmp2 = signal.filtfilt(bb, aa, tmp)
##    tmp3 = signal.filtfilt(bbb, aaa, tmp2)
#    vv[:,ista] = tmp2

nstep = 2725

if (var == "Dtw") or (var == "Dt"):
    cmax = 1e-2
    cmin = -cmax
    tck = 1e-3*np.array([-10 , -8, -6, -4, -2, 0, 2, 4, 6, 8, 10])
else:   # Coherence
    cmin = 0.4 # 48
    cmax = 0.8 # 74
    tck = np.array([0.4, 0.45, 0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8])


for istep in range(nstep):

    tstart = time.time()
    plt.clf()
    plt.contour(xgrid, ygrid, zgrid, levels, colors=('grey',), linestyle="dashed", linewidth=0.5, alpha=0.5)
    CS = plt.contour(xgrid, ygrid, zgrid, levelslbl, colors=('grey',))
#    plt.clabel(CS, fontsize=9, inline=1 , color='g')
    plt.plot(fault[:,0],fault[:,1],'k')
    plt.plot(gr[iref,0], gr[iref,1],'ok',mfc='none')
    cax = plt.scatter(gr[:,0], gr[:,1], s=15, c=vv[istep,:], vmin=cmin, vmax=cmax, edgecolor='none') # , vmin=cmin, vmax=cmax)
    plt.title('timestep: '+'{:04d}'.format(istep)+' - jd: '+'{:7.3f}'.format(float(132)+float(istep)/144.0))
    plt.colorbar(cax, ticks=tck)
    plt.grid()
    plt.axis(xyax)
    plt.xticks(rotation=30)
    plt.tight_layout()
    plt.savefig('./'+var+'_'+'{:04d}'.format(istep)+'.png', dpi=600, bbox_inches=0)
    elps = time.time() - tstart
    print(istep, " : ", elps, "sec")

