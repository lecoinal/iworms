#!/usr/bin/env python
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import h5py
import numpy as np
import sys
import time

import scipy.io as sio

import matplotlib as mpl
mpl.rcParams["figure.dpi"] = 600.
mpl.rcParams["contour.linewidth"] = 0.7

rep = "./"

var = sys.argv[1] # Dt ou Dtw ou Coh


gr = np.loadtxt('/bettik/lecoinal/RESOLVE/METRIC/zreseau_ZO_2018_115.txt',usecols = (1,2,3))

xmin = np.min(gr[:,0])-10
xmax = np.max(gr[:,0])+10
ymin = np.min(gr[:,1])-10
ymax = np.max(gr[:,1])+10
xyax = [xmin, xmax, ymin, ymax]

x = gr[:,0]
y = gr[:,1]
z = gr[:,2] 

iref=55

#h5f = h5py.File("/bettik/lecoinal/RESOLVE/TIMEERRORS/EXP020/Dt_115_147.h5", "r")
h5f = h5py.File("/bettik/lecoinal/RESOLVE/TIMEERRORS/EXP021/Dt_115_147.h5", "r")
vv = h5f[var][:,:]
h5f.close()


remove_1d=True

if remove_1d:
    from scipy import signal
## remove 1d notch frequency
## notch filter to remove 24h frequency
    fs = 1.0 / (10.0 * 60.0)   # Sample frequency (Hz)
    f0 = 1.0 / (24.0 * 3600.0) # Frequency to be removed from signal (Hz)
    Q = 30.0  # Quality factor
## Design notch filter
    b, a = signal.iirnotch(f0, Q, fs)
## remove 1d and 2d
#    f0 = 1.0 / (2.0 * 24.0 * 3600.0) # Frequency to be removed from signal (Hz)
#    bb, aa = signal.iirnotch(f0, Q, fs)
## remove all under 1d
#    f0 = 1.0 / (24.0 * 3600.0) # Frequency to be removed from signal (Hz)
#    nyq = fs*0.5
#    bbb, aaa = signal.butter(4, f0/nyq, btype="high")
    nsta = np.shape(vv)[1]   # number of stations or number of pairs
    print("nsta or npair",nsta)
    for ista in np.arange(nsta):
        tmp = signal.filtfilt(b, a, vv[:,ista])
        #tmp2 = signal.filtfilt(bb, aa, tmp)
        #tmp3 = signal.filtfilt(bbb, aaa, tmp2)
        vv[:,ista] = tmp ## tmp3





# topo (bedrock) and ice thickness
mat_contents = sio.loadmat("/bettik/PROJECTS/pr-resolve/lecoinal/ZENODO_UGO/NANNI_thickness_argentiere.mat")
print(sorted(mat_contents.keys()))
Xtopo = mat_contents['X']
Ytopo = mat_contents['Y']
bedrock = mat_contents['Zbed_christian']
thickness = mat_contents['thickness']

nstep = 4735

if (var == "Dtw") or (var == "Dt"):
    plt.rcParams['image.cmap'] = 'bwr'
    cmax = 1e-2
    cmin = -cmax
    tck = 1e-3*np.array([-10 , -8, -6, -4, -2, 0, 2, 4, 6, 8, 10])
else:   # Coherence
    plt.rcParams['image.cmap'] = 'gist_rainbow'
    cmin = 0.7 # 48
    cmax = 1.0 # 74
    tck = np.array([0.7, 0.75, 0.8, 0.85, 0.9, 0.95, 1.0])


for istep in range(nstep):

    tstart = time.time()
    plt.clf()
    plt.contour(Xtopo, Ytopo, thickness, np.array([50, 100, 150, 200, 250]), colors='k')
    plt.contour(Xtopo, Ytopo, bedrock, np.array([2000, 2050, 2100, 2150, 2200, 2250, 2300, 2350, 2400, 2450, 2500]), colors='g')
    plt.contour(Xtopo, Ytopo, thickness+bedrock, np.arange(2000, 3000, 10), colors='b')
    plt.plot(gr[iref,0], gr[iref,1],'ok',mfc='none')
    cax = plt.scatter(gr[:,0], gr[:,1], s=30, c=vv[istep,:], vmin=cmin, vmax=cmax, edgecolor='none') # , vmin=cmin, vmax=cmax)
    plt.title('timestep: '+'{:04d}'.format(istep)+' - jd: '+'{:7.3f}'.format(float(115)+float(istep)/144.0))
    plt.colorbar(cax, ticks=tck)
    plt.grid()
    plt.axis(xyax)
    plt.xticks(rotation=30)
    plt.tight_layout()
    plt.savefig('./'+var+'_'+'{:04d}'.format(istep)+'.png', dpi=600, bbox_inches=0)
    elps = time.time() - tstart
    print(istep, " : ", elps, "sec")

