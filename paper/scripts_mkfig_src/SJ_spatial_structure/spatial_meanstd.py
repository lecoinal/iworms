#!/usr/bin/env python
# -*- coding: utf-8 -*-

# sur ist-oar
# source /soft/env.bash
# module load python/python3.7
# python3 spatial_meanstd.py Dtw 

# sur gricad clusters
# source /applis/site/guix-start.sh
# refresh guix pyplot
# python3 spatial_meanstd.py Dtw
# python3 spatial_meanstd.py Dt
# python3 spatial_meanstd.py Coh

import matplotlib.pyplot as plt
import h5py
import numpy as np
import sys

import scipy.io as sio
from scipy import interpolate

import matplotlib as mpl


var = sys.argv[1] # Dt ou Dtw ou Coh
#sat = np.float(sys.argv[2]) # 1.0   # 0.2   # only for Dt Dtw

#################

mpl.rcParams["figure.dpi"] = 600.

rep = "./"

# faille
#mat_contents = sio.loadmat("/data/projects/f_image/lecointre/IWORMS/TEST_DONNEES_REELLES/tomo/tomo_SJFZ_multifreq.mat")
mat_contents = sio.loadmat("/summer/f_image/lecointre/IWORMS/TEST_DONNEES_REELLES/tomo/tomo_SJFZ_multifreq.mat")
print(sorted(mat_contents.keys()))
fault = mat_contents['fault']
print(np.shape(fault))
print(np.nanmin(fault[:,0]),np.nanmax(fault[:,0]))
print(np.nanmin(fault[:,1]),np.nanmax(fault[:,1]))

# echelle 100m en zonale (x0-x1) et en méridien (y0-y1)
x0 = -116.595
y0 = 33.536
x1 = x0 + 100 * 360 / (2 * np.pi * 6400000)
y1 = y0 + 100 * 360 / (2 * np.pi * 6400000 * np.cos(y0 * np.pi / 180))

######################

# plot station elevation

plt.rcParams['image.cmap'] = 'gist_rainbow'

ytk = np.array([33.536, 33.538, 33.540, 33.542])
ytklbl = ["33.536°", "33.538°", "33.540°", "33.542°"]
xtk = np.array([-116.594, -116.592, -116.590, -116.588])
xtklbl = ["-116.594°", "-116.592°", "-116.590°", "-116.588°"]

gr = np.loadtxt('/summer/f_image/lecointre/IWORMS_SANJACINTO/xyz1108.txt')

print(np.shape(gr))
print(np.min(gr[:,0]), np.max(gr[:,0]))
print(np.min(gr[:,1]), np.max(gr[:,1]))
border = 0.0001
xmin = np.min(gr[:,0])-border
xmax = np.max(gr[:,0])+border
ymin = np.min(gr[:,1])-border
ymax = np.max(gr[:,1])+border
xyax = [xmin, xmax, ymin, ymax]

plt.rcParams['image.cmap'] = 'gist_rainbow'
plt.clf()
fig = plt.figure()
ax = fig.add_subplot(111) #, projection='3d', azim=-60, elev=55)
irefall = 487
plt.plot(gr[irefall,0], gr[irefall,1],'ok',mfc='none')
plt.plot(fault[:,0],fault[:,1], 'k') #zf, '-',color='grey')
plt.plot(np.array([x0,x1]),np.array([y0,y0]),'-k')
plt.plot(np.array([x0,x0]),np.array([y0,y1]),'-k')
plt.text(0.5*(x0+x1),y0-0.0001,'100m',horizontalalignment='center',verticalalignment='top',color='k')
plt.text(x0-0.0001,0.5*(y0+y1),'100m',rotation='90',horizontalalignment='right',verticalalignment='center',color='k')
#sc = ax.scatter(x, y, s=7, c=z, marker='o', edgecolor='none')
cax = plt.scatter(gr[:,0], gr[:,1], s=15, c=gr[:,2], edgecolor='none')
plt.title(r'a) Elevation')
plt.axis(xyax)
#plt.tight_layout()
ax.set_yticks(ytk, minor=False)
ax.set_yticklabels(ytklbl, minor=False)
ax.set_xticks(xtk, minor=False)
ax.set_xticklabels(xtklbl, minor=False)
clb = plt.colorbar(cax)
clb.ax.set_title('m')
plt.savefig('./SJarray_2D.png', dpi=600, bbox_inches=0)


#######################


# update gr (923 instead of 1108)
gr = np.loadtxt('./xyz.txt')

# but do not update xyax
#print(np.shape(gr))
#print(np.min(gr[:,0]), np.max(gr[:,0]))
#print(np.min(gr[:,1]), np.max(gr[:,1]))
#xmin = np.min(gr[:,0])-0.0001
#xmax = np.max(gr[:,0])+0.0001
#ymin = np.min(gr[:,1])-0.0001
#ymax = np.max(gr[:,1])+0.0001
#xyax = [xmin, xmax, ymin, ymax]

xgrid = np.linspace(gr[:,0].min(), gr[:,0].max(), np.shape(gr)[0])
ygrid = np.linspace(gr[:,1].min(), gr[:,1].max(), np.shape(gr)[0])
xgrid, ygrid = np.meshgrid(xgrid, ygrid)
zgrid = interpolate.griddata((gr[:,0],gr[:,1]),gr[:,2], (xgrid, ygrid))
levels = np.arange(1400, 1500, 5)
levelslbl = np.arange(1400, 1500, 20)

## ref R3010, calcul avec abs(coh) et calcul avec elements diagonaux
#h5f = h5py.File(rep+"/out_923.h5", "r")
#vvm = np.nanmean(h5f[var][:,:], axis=1)
#vvs = np.nanstd(h5f[var][:,:], axis=1)
#iref=411
#h5f.close()

# ref R3010, calcul sans abs(coh) et calcul sans elements diagonaux

iref=411
##iref=0
#h5f = h5py.File(rep+"/out_ref"+str(iref)+".h5", "r")
#if (var == "Dtw_ref") or (var == "Dt_ref"):
#    vv = h5f[var][0,:,:]
#else:
#    vv = h5f[var][:,:]
#h5f.close()

#h5f = h5py.File("/data/projects/f_image/lecointre/IWORMS/TEST_DONNEES_REELLES/GRAPHE_COMPLET_132_150_923/out_39222590.h5", "r")
h5f = h5py.File("/summer/f_image/lecointre/IWORMS/TEST_DONNEES_REELLES/GRAPHE_COMPLET_132_150_923/out_39222590.h5", "r")
vv = h5f[var][:,:]
h5f.close()

## remove 1d notch frequency
## notch filter to remove 24h frequency
#from scipy import signal
#fs = 1.0 / (10.0 * 60.0)   # Sample frequency (Hz)
#f0 = 1.0 / (24.0 * 3600.0) # Frequency to be removed from signal (Hz)
#Q = 30.0  # Quality factor
## Design notch filter
#b, a = signal.iirnotch(f0, Q, fs)
## remove 1d and 2d
#f0 = 1.0 / (2.0 * 24.0 * 3600.0) # Frequency to be removed from signal (Hz)
#bb, aa = signal.iirnotch(f0, Q, fs)
### remove all under 1d
##f0 = 1.0 / (24.0 * 3600.0) # Frequency to be removed from signal (Hz)
##nyq = fs*0.5
##bbb, aaa = signal.butter(4, f0/nyq, btype="high")
#

#nsta = np.shape(vv)[1]
#print("nsta",nsta)
#for ista in np.arange(nsta):
#    tmp = signal.filtfilt(b, a, vv[:,ista])
#    tmp2 = signal.filtfilt(bb, aa, tmp)
##    tmp3 = signal.filtfilt(bbb, aaa, tmp2)
#    vv[:,ista] = tmp2




vvm = np.nanmean(vv, axis=0)
vvs = np.nanstd(vv, axis=0)


if (var == "Dtw") or (var == "Dt"):

#    plt.rcParams['image.cmap'] = 'bwr'
    plt.clf()
    fig = plt.figure()
    ax = fig.add_subplot(111) # , projection='3d', azim=-60, elev=55)
    plt.contour(xgrid, ygrid, zgrid, levels, colors=('grey',), linestyle="dashed", linewidth=0.5, alpha=0.5)
    CS = plt.contour(xgrid, ygrid, zgrid, levelslbl, colors=('grey',))
    plt.clabel(CS, fontsize=9, inline=1)# , color='g')
    plt.plot(fault[:,0],fault[:,1],'k')
    plt.plot(gr[iref,0], gr[iref,1],'ok',mfc='none')
    #cmax = 0.5*np.max(np.abs(vvm[:]))
    print("maxabs mean",np.max(np.abs(vvm[:])))
    cmax = 1.0 # 1e-3 # 3e-5 # 1e-3
    cax = plt.scatter(gr[:,0], gr[:,1], s=15, c=1e3*vvm[:], vmin=-cmax, vmax=cmax, edgecolor='none')
    #plt.scatter(gr[iref,0], gr[iref,1], s=15, c=vvm[iref], marker="*", vmin=-cmax, vmax=cmax, edgecolor='none')
    plt.plot(np.array([x0,x1]),np.array([y0,y0]),'-k')
    plt.plot(np.array([x0,x0]),np.array([y0,y1]),'-k')
    plt.text(0.5*(x0+x1),y0-0.0001,'100m',horizontalalignment='center',verticalalignment='top',color='k')
    plt.text(x0-0.0001,0.5*(y0+y1),'100m',rotation='90',horizontalalignment='right',verticalalignment='center',color='k')
    if (var == "Dtw"):
        plt.title(r'd) 19-day averaged $\widetilde{\Delta}$') #, loc='right')
    else:
        plt.title(r'19days averaged $\Delta t$ (msec)', loc='right')
    tck = 1e-1*np.array([-10 , -8, -6, -4, -2, 0, 2, 4, 6, 8, 10])
    clb = plt.colorbar(cax, ticks=tck)
    clb.ax.set_title('ms')
    plt.axis(xyax)
    #plt.xticks(rotation=30)
#    plt.tight_layout()
    ax.set_yticks(ytk, minor=False)
    ax.set_yticklabels(ytklbl, minor=False)
    ax.set_xticks(xtk, minor=False)
    ax.set_xticklabels(xtklbl, minor=False)

    plt.savefig('./mean_'+var+'.png', dpi=600, bbox_inches=0)

#    plt.rcParams['image.cmap'] = 'bwr' # 'gist_rainbow'
    #print(vvs[iref])
    #vvs[iref]=9999.
    #cmin=np.min(vvs)
    #vvs[iref]=0.0
    #cmax=np.max(vvs)
    print("maxstd",np.max(vvs[:]))
    vvs[iref]=9999.
    print("minstd",np.min(vvs[:]))
    vvs[iref]=0.
    cmin = 1.0 #e-3
    cmax = 4.5 #e-3
    plt.clf()
    fig = plt.figure()
    ax = fig.add_subplot(111) # , projection='3d', azim=-60, elev=55)
    plt.contour(xgrid, ygrid, zgrid, levels, colors=('grey',), linestyle="dashed", linewidth=0.5, alpha=0.5)
    CS = plt.contour(xgrid, ygrid, zgrid, levelslbl, colors=('grey',))
    plt.clabel(CS, fontsize=9, inline=1)# , color='g')
    plt.plot(fault[:,0],fault[:,1],'k')
    plt.plot(gr[iref,0], gr[iref,1],'ok',mfc='none')
    plt.scatter(gr[:,0], gr[:,1], s=15, c=1e3*vvs[:], vmin=cmin, vmax=cmax, edgecolor='none')
    plt.plot(np.array([x0,x1]),np.array([y0,y0]),'-k')
    plt.plot(np.array([x0,x0]),np.array([y0,y1]),'-k')
    plt.text(0.5*(x0+x1),y0-0.0001,'100m',horizontalalignment='center',verticalalignment='top',color='k')
    plt.text(x0-0.0001,0.5*(y0+y1),'100m',rotation='90',horizontalalignment='right',verticalalignment='center',color='k')
    #plt.scatter(gr[iref,0], gr[iref,1], s=15, c=vvs[iref], marker="*", vmin=cmin, vmax=cmax, edgecolor='none')
    if (var == "Dtw"):
        plt.title(r'b) 19-day standard deviation $\widetilde{\Delta}$') #, loc='right')
    else:
        plt.title(r'd) 19-day standard deviation $\Delta$') #, loc='right')
    clb = plt.colorbar()
    clb.ax.set_title('ms')
    plt.axis(xyax)
    #plt.xticks(rotation=30)
#    plt.tight_layout()
    ax.set_yticks(ytk, minor=False)
    ax.set_yticklabels(ytklbl, minor=False)
    ax.set_xticks(xtk, minor=False)
    ax.set_xticklabels(xtklbl, minor=False)
    plt.savefig('./std_'+var+'.png', dpi=600, bbox_inches=0)

else:

    print("min",np.min(vvm[:]))
    print("max",np.max(vvm[:]))

#################
# just to compare with tomo mordret, use the same colormap
    #plt.rcParams['image.cmap'] = 'jet'
    #plt.rcParams['image.cmap'] = 'jet_r'
#    rgb = np.loadtxt('./listrgb.txt')
#    rgb /= 256.
#    from matplotlib.colors import LinearSegmentedColormap
#    cm = LinearSegmentedColormap.from_list('mordret', rgb, N=np.shape(rgb)[0])
#    plt.rcParams['image.cmap'] = cm
#####################

#    cmin = -2e-4 # 0.48
#    cmax = 12e-4 # 0.74
    cmin = 0.48
    cmax = 0.74
    plt.clf()
    fig = plt.figure()
    ax = fig.add_subplot(111) # , projection='3d', azim=-60, elev=55)
    plt.contour(xgrid, ygrid, zgrid, levels, colors=('grey',), linestyle="dashed", linewidth=0.5, alpha=0.5)
    CS = plt.contour(xgrid, ygrid, zgrid, levelslbl, colors=('grey',))
    plt.clabel(CS, fontsize=9, inline=1)# , color='g')
    plt.plot(fault[:,0],fault[:,1],'k')
    plt.plot(gr[iref,0], gr[iref,1],'ok',mfc='none')
    plt.scatter(gr[:,0], gr[:,1], s=15, c=vvm[:], vmin=cmin, vmax=cmax, edgecolor='none') # , vmin=cmin, vmax=cmax)
    plt.plot(np.array([x0,x1]),np.array([y0,y0]),'-k')
    plt.plot(np.array([x0,x0]),np.array([y0,y1]),'-k')
    plt.text(0.5*(x0+x1),y0-0.0001,'100m',horizontalalignment='center',verticalalignment='top',color='k')
    plt.text(x0-0.0001,0.5*(y0+y1),'100m',rotation='90',horizontalalignment='right',verticalalignment='center',color='k')
    #plt.scatter(gr[iref,0], gr[iref,1], s=15, c=vvm[iref], marker="*", vmin=cmin, vmax=cmax, edgecolor='none') # , vmin=cmin, vmax=cmax)
    plt.title(r'c) 19-day averaged coherence') #, loc='right')
    clb = plt.colorbar()
    clb.ax.set_title(r'$Coh$')
    plt.axis(xyax)
    #plt.xticks(rotation=30)
#    plt.tight_layout()
    ax.set_yticks(ytk, minor=False)
    ax.set_yticklabels(ytklbl, minor=False)
    ax.set_xticks(xtk, minor=False)
    ax.set_xticklabels(xtklbl, minor=False)
    plt.savefig('./mean_'+var+'.png', dpi=600, bbox_inches=0)

    plt.clf()
    plt.hist(vvm)
    plt.savefig('./histmean_'+var+'.png', dpi=200, bbox_inches=0)

    print("min",np.min(vvs[:]))
    print("max",np.max(vvs[:]))

    cmin = 0.03 # 0.09
    cmax = 0.07 # 0.16
    plt.clf()
    fig = plt.figure()
    ax = fig.add_subplot(111) # , projection='3d', azim=-60, elev=55)
    plt.plot(fault[:,0],fault[:,1],'k')
    plt.plot(gr[iref,0], gr[iref,1],'ok',mfc='none')
    plt.scatter(gr[:,0], gr[:,1], s=15, c=vvs[:], vmin=cmin, vmax=cmax, edgecolor='none') # , vmin=cmin, vmax=cmax)
    #plt.scatter(gr[iref,0], gr[iref,1], s=15, c=vvs[iref], marker="*", vmin=cmin, vmax=cmax, edgecolor='none') # , vmin=cmin, vmax=cmax)
    plt.title(r'19days stddev Coherence', loc='right')
    plt.colorbar()
    plt.axis(xyax)
    #plt.xticks(rotation=30)
#    plt.tight_layout()
    ax.set_yticks(ytk, minor=False)
    ax.set_yticklabels(ytklbl, minor=False)
    ax.set_xticks(xtk, minor=False)
    ax.set_xticklabels(xtklbl, minor=False)
    #ax.set_xlim((-116.596, -116.588))
    #ax.set_ylim((33.536, 33.542))
    plt.savefig('./std_'+var+'.png', dpi=600, bbox_inches=0)

    plt.clf()
    plt.hist(vvs)
    plt.savefig('./histstd_'+var+'.png', dpi=200, bbox_inches=0)

#################
