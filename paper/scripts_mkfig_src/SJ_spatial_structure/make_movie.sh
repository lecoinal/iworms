#!/bin/bash
#OAR --project iste-equ-ondes
#OAR -n make_movie
#OAR -l /nodes=1/core=1,walltime=60:00:00

set -e

source /soft/env.bash
module load python/python3.7

date

START=$(date +%s.%N)

cat $OAR_NODE_FILE

var=$1 # Dt, Dtw, Coh

PEXE="spatial_movie.py"

TMPDIR=/$SHARED_SCRATCH_DIR/$USER/oar.$OAR_JOB_ID
mkdir -p $TMPDIR
cp $PEXE $TMPDIR/.
cp xyz.txt $TMPDIR/.
cd $TMPDIR
ln -s /data/projects/f_image/lecointre/IWORMS/TEST_DONNEES_REELLES/GRAPHE_COMPLET_132_150_923/out_39415926.h5 in.h5
ln -s /data/projects/f_image/lecointre/IWORMS/TEST_DONNEES_REELLES/tomo/tomo_SJFZ_multifreq.mat tomo.mat

python3.7 -u $PEXE $var

# ffmpeg
ffmpeg -r 15/1 -i ${var}_%04d.png -c:v libx264 -vf "fps=25,format=yuv420p" ${var}.mp4

rm *png

END=$(date +%s.%N)
DIFF=$(echo "$END - $START" | bc)
echo "ElapsedTime: $DIFF sec"
