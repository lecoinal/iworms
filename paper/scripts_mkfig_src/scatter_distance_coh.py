#!/usr/bin/env python
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import h5py
import numpy as np
import sys

plt.rcParams['image.cmap'] = 'gist_rainbow'
plt.rcParams["lines.linewidth"] = 0.7

#rep = "/data/projects/f_image/lecointre/IWORMS_SANJACINTO/IWORMS_SJ_EXP003_923/ALL/"
rep = "/data/projects/f_image/lecointre/IWORMS/TEST_DONNEES_REELLES/GRAPHE_COMPLET_132_150_923/"

#h5f = h5py.File(rep+"/out.h5", "r")
h5f = h5py.File(rep+"/out_39415926.h5", "r")

dist = np.loadtxt('./pair_dist_3.txt')

print(dist.shape)
print(np.min(dist),np.max(dist))

aa, be = np.histogram(dist, bins=[0, 100, 500, 900])
print(aa)

#bin_edge = [0, 100, 500, 821]
bin_edge = np.arange(-1, 1.1, 0.1)

aa_1 = np.zeros((len(bin_edge)-1,))
aa_2 = np.zeros((len(bin_edge)-1,))
aa_3 = np.zeros((len(bin_edge)-1,))
aa_0 = np.zeros((len(bin_edge)-1,))

#for jd in np.arange(132, 151):
#  print(jd)
#  coh = h5f[str(jd)+"/coh"][:,:]
#  coh_1 = coh.flatten()  # 
#  a_1, be = np.histogram(coh_1, bins=bin_edge)
#  aa_0 += a_1
#  coh_1 = coh[:,dist < 100].flatten()  # 
#  a_1, be = np.histogram(coh_1, bins=bin_edge)
#  aa_1 += a_1
#  coh_1 = coh[:,(dist >= 100) & (dist < 500)].flatten()  # 
#  a_1, be = np.histogram(coh_1, bins=bin_edge)
#  aa_2 += a_1
#  coh_1 = coh[:,(dist >= 500) & (dist < 900)].flatten()  # 
#  a_1, be = np.histogram(coh_1, bins=bin_edge)
#  aa_3 += a_1

for it in np.arange(0,2725,25):
  print(it)
  coh = h5f["/coh"][it:it+25,:]
  coh_1 = coh.flatten()  # 
  a_1, be = np.histogram(coh_1, bins=bin_edge)
  aa_0 += a_1
  coh_1 = coh[:,dist < 100].flatten()  # 
  a_1, be = np.histogram(coh_1, bins=bin_edge)
  aa_1 += a_1
  coh_1 = coh[:,(dist >= 100) & (dist < 500)].flatten()  # 
  a_1, be = np.histogram(coh_1, bins=bin_edge)
  aa_2 += a_1
  coh_1 = coh[:,(dist >= 500) & (dist < 900)].flatten()  # 
  a_1, be = np.histogram(coh_1, bins=bin_edge)
  aa_3 += a_1

h5f.close()

plt.clf()
plt.grid()
plt.title('Distribution of coherence values \n among all 2725 timesteps')
plt.plot(bin_edge[0:-1],aa_0,'o-',label='among all 425,503 pairs')
plt.plot(bin_edge[0:-1],aa_1,'o-',label='among '+str(aa[0])+' pairs whose node interdistance < 100m')
plt.plot(bin_edge[0:-1],aa_2,'o-',label='among '+str(aa[1])+' pairs whose 100m < n. i. < 500m')
plt.plot(bin_edge[0:-1],aa_3,'o-',label='among '+str(aa[2])+' pairs whose 500m < n. i. < 821m')
plt.legend()
plt.xlabel('coherence value (binwidth = 0.1)')
plt.savefig('zcohdist.png')

