#!/bin/bash
#OAR --project resolve
#OAR -n scatter_Dt_coh
#OAR -l /nodes=1/core=1,walltime=00:05:00
##OAR -t besteffort

# oarsub -S "./scatter_Dt_Coh_argentiere.sh Dt Coh"
# oarsub -S "./scatter_Dt_Coh_argentiere.sh Dtw Coh"

set -e

#source /soft/env.bash
source /applis/site/guix-start.sh
refresh_guix pyplot
# guix package -m manifest_pyplot.scm -p $GUIX_USER_PROFILE_DIR/pyplot

date

START=$(date +%s.%N)

cat $OAR_NODE_FILE

##################################

varDt="$1" # Dt or Dtw
varCoh="$2" # Coh or AbsCoh

PEXE="scatter_Dt_Coh_argentiere.py"

#module load python/python3.9

#python3.9 -u $PEXE $varDt $varCoh
python3 -u $PEXE $varDt $varCoh

END=$(date +%s.%N)
DIFF=$(echo "$END - $START" | bc)
echo "ElapsedTime: $DIFF sec"

