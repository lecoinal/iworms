#!/usr/bin/env python
# -*- coding: utf-8 -*-

import h5py
import numpy as np
import sys
import math
from scipy.signal import butter, lfilter
import matplotlib.pyplot as plt
import argparse
import matplotlib as mpl

mpl.rcParams["figure.figsize"] = [7.4, 4.8]
mpl.rcParams["lines.linewidth"] = 0.5
mpl.rcParams["lines.markersize"] = 1.0
mpl.rcParams["figure.dpi"] = 600.
mpl.rcParams["font.size"] = 9.

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="measure dt and compute Dt on synthetic data")
    parser.add_argument("--dec", default=0, type=int,
                        help="set option to apply a dcreasing factor of GF amplitude for distance > 20km (default=0 : disable)")
    parser.add_argument("--per", default="T2", type=str,
                        help="period: well synchronized T1 period or one-sec desynchronized T2 period (default='T2')")

    args = parser.parse_args()
    decGF = args.dec # sys.argv[2]  #  0 : no GF amplitude decrease with distance
                                    # 1 : after 20km, decrease GF amplitude with factor 1/sqrt(dist/20km)
    varD = "dt"
    per = args.per

    fc = 0.15
    period = 1./fc

    nbpair = 3081
    nbsta = 79

    SW = 2

    snr = np.array([0.01, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1, 1.5, 2, 3, 4, 5, 10, 100])
    snrc = []
    snrc.append("0.01")
    snrc.append("0.1")
    snrc.append("0.2")
    snrc.append("0.3")
    snrc.append("0.4")
    snrc.append("0.5")
    snrc.append("0.6")
    snrc.append("0.7")
    snrc.append("0.8")
    snrc.append("0.9")
    snrc.append("1.0")
    snrc.append("1.5")
    snrc.append("2.0")
    snrc.append("3.0")
    snrc.append("4.0")
    snrc.append("5.0")
    snrc.append("10.0")
    snrc.append("100.0")
 
    # plot S1 par rapport S2 :
    sta = 0
    ref = 1

    staref = np.array([sta, ref]) # 2   # delta = 1 (dt(1,j)) ou 2 (dt(2,j))
 
    if per == 'T2':
        ib=240
        ie=480
    else: # T1
        ib=0
        ie=240

    mT2 = np.empty((len(snr),ie-ib, 2),dtype=np.float32)   # 78
    mcohT2 = np.empty((len(snr),ie-ib, 2),dtype=np.float32)  # 78
    mT2_step = np.empty((len(snr),ie-ib,nbsta-1, 2),dtype=np.float32)   # 240,78
    mcohT2_step = np.empty((len(snr),ie-ib,nbsta-1, 2),dtype=np.float32)  # 240,78

    #idir = "/nfs_scratch/lecoinal/zzzzz/"
    #idir = "/bettik/lecoinal/zzzzz/"
    idir = "/summer/f_image/lecointre/IWORMS/SYNTHETIC/"

    for ii in np.arange(len(snr)):
        print(snr[ii])
        #filename = idir+"/SNR"+snrc[ii]+"_SW"+str(SW)+"_CC0_decGF"+str(decGF)+"_REF240_LIN0_nstep480_nbpair"+str(nbpair)+".h5"
        #filename = idir+"/2Tover3/SNR"+snrc[ii]+"_SW"+str(SW)+"_CC0_decGF"+str(decGF)+"_REF240_LIN0_nstep480_nbpair"+str(nbpair)+".h5"
        filename = idir+"/SNR"+snrc[ii]+"_SW"+str(SW)+"_CC0_decGF"+str(decGF)+"_REF240_LIN0_nstep480_nbpair"+str(nbpair)+".h5"
        f = h5py.File(filename,'r')

        #if delta == 1:
        # les (1,j)
        # suppose que sta == 0
        mT2_step[ii,:,:, sta] = fc * f['/'+varD][ib:ie,0:nbsta-1]  # les petits dt liés à la première station
        mcohT2_step[ii,:,:, sta] = f['/coh'][ib:ie,0:nbsta-1]  # les coh liees a la premiere station : le nbsta-1 premieres paires
        #else:  # delta == 2
        # les (2,j)
        # suppose que ref == 1
        mT2_step[ii,:,0, ref] = -1.0 * fc * f['/'+varD][ib:ie,0]
        mT2_step[ii,:,1:, ref] = fc * f['/'+varD][ib:ie,nbsta-1:nbsta-1+nbsta-2]
        mcohT2_step[ii,:,0, ref] = f['/coh'][ib:ie,0]
        mcohT2_step[ii,:,1:, ref] = f['/coh'][ib:ie,nbsta-1:nbsta-1+nbsta-2]

        mT2[ii,:,:] = np.mean(mT2_step[ii,:,:,:],axis=1)   # 78
        mcohT2[ii,:,:] = np.mean(mcohT2_step[ii,:,:,:],axis=1)
        f.close()
       
#    from random import randint
#    col = []
#    for ii in range(len(snr)):
#        col.append('#%06X' % randint(0, 0xFFFFFF))
#    print(col)
    col = ['#549CFD', '#D4DA42', '#53392B', '#F6D68A', '#63BBD0', '#6D5E40', '#DBF6EB', '#F6CAC3', '#7D8358', '#27F112', '#7AFEB3', '#8AE46B', '#E6001D', '#A250F6', '#F9AC0D', '#9B20FB', '#CFFEAD', '#478DF4']

    print('dt min/max')
    print(np.nanmin(mT2_step))
    print(np.nanmax(mT2_step))
    dt_edges = np.arange(-0.52,0.52,0.005)
    print(dt_edges)
    print(np.shape(dt_edges))
    X = dt_edges[0:-1]+0.5*np.diff(dt_edges)
    #print(X)
    #print(np.shape(X))
    #X = dt_edges

    # create the density plot of mT2_step for each snr ii
    plt.clf()
    plt.grid()
    ytk = np.arange(len(snr))
    ytklbl = snrc
    for ii in np.arange(len(snr)):
        dtloc = mT2_step[ii,:,:,0].flatten()
        H, xedges = np.histogram(dtloc, bins=(dt_edges)) #, density=True)
        yy1 = ii+H/np.max(H)
        #plt.plot(X,ii+H/np.max(H),color=col[ii],label="snr="+snrc[ii])
        if ii == 0:
            plt.plot(X,yy1,color='C0',label="$\delta^{1,j}$")
        else:
            plt.plot(X,yy1,color='C0')
        #plt.fill_between(X, yy1, np.min(yy1), color='#fdb0aa')
        plt.fill_between(X, yy1, np.min(yy1), facecolor='C0', alpha=0.4)
        dtloc = mT2_step[ii,:,:,1].flatten()
        #dt_edges = np.arange(np.nanmin(dtloc)-0.01,np.nanmax(dtloc)+0.01,0.01)
        H, xedges = np.histogram(dtloc, bins=(dt_edges)) #, density=True)
        yy2 = ii+H/np.max(H)
        #plt.plot(X,ii+H/np.max(H),color=col[ii],label="snr="+snrc[ii])
        if ii == 0:
            plt.plot(X,yy2,color='C1',label="$\delta^{2,j}$")
        else:
            plt.plot(X,yy2,color='C1')
        #plt.fill_between(X, yy2, np.min(yy2), color='#539ecd')
        plt.fill_between(X, yy2, np.min(yy2), facecolor='C1', alpha=0.4)
        #yy = np.min([yy1,yy2],axis=0)
        #plt.fill_between(X,yy, np.min(yy), color='w') 
    plt.legend(loc=2)
    plt.yticks(ytk, ytklbl)
    plt.ylabel(r"$SNR$")
    plt.title('Normalized probability density functions \n'+
              'of inter-station phase differences among 78 pairs and \n'+
              'among 240 timesteps '+r'from the $T_2$ period (fixed binwidths of $0.5\%T_0$)') # related to station '+str(delta))
#    if delta == 1:
#        plt.xlabel(r"$\delta^{1,j} (sec)$ (fixed binwidths of 0.05sec)") #,fontsize=20)
#    else:
#        plt.xlabel(r"$\delta^{2,j} (sec)$ (fixed binwidths of 0.05sec)") #,fontsize=20)
#    plt.xlabel(r"$ red: \delta^{1,j} , blue: \delta^{2,j} (sec)$ (fixed binwidths of 0.05sec)") #,fontsize=20)

    
    xtk = np.array([-.5, -.3, -.15, 0., .15, .3, .5])
    xtklbl = [r"$-0.5T_0$", r"$-0.3T_0$", r"$-0.15T_0$", r"$0$", r"$0.15T_0$", r"$0.3T_0$", r"$0.5T_0$"]
    plt.xticks(xtk,xtklbl)

    plt.ylim([-.2,ii+1+0.2])

    plt.tight_layout()
    plt.savefig(per+"synthetic_obs_"+varD+"_SNR_decGF"+str(decGF)+"_sta"+str(sta)+"_ref"+str(ref)+".png",dpi=600)
 
    for delta in staref:
        #delta = ii-1
        #print(delta)

        plt.clf()
        plt.grid()
    #    plt.plot(mcohT2_step.flatten(),mT2_step.flatten(),'.',color='blue',label="at each time step of the period")
        plt.plot(mcohT2_step[0,:,:,delta].flatten(),mT2_step[0,:,:,delta].flatten(),'.',markersize=1,color=col[0],label="for each 78 edges related to station "+str(delta+1))
        plt.plot(mcohT2[0,:,delta], mT2[0,:,delta], 'o',color=col[0], fillstyle='none', markersize=5,label="averaged along 78 edges")
        plt.legend(loc=4)
        for ii in np.arange(len(snr)):
            plt.plot(mcohT2_step[ii,:,:,delta].flatten(),mT2_step[ii,:,:,delta].flatten(),'.',markersize=1,color=col[ii],label="for each 78 edges related to station "+str(delta+1))
        for ii in np.arange(len(snr)):
            plt.plot(mcohT2[ii,:,delta], mT2[ii,:,delta], 'o',color=col[ii], fillstyle='none', markersize=5,label="averaged along 78 edges")
    #    for ii in np.arange(len(snr)):
    #        stdDt = np.std(mT2_step[ii,:])
    #        stdcoh = np.std(mcohT2_step[ii,:])
    #        plt.plot(mcohT2[ii]+2*stdcoh*np.array([-1,1]),mT2[ii]*np.array([1,1]),'k-')
    #        plt.plot(mcohT2[ii]*np.array([1,1]),mT2[ii]+2*stdDt*np.array([-1,1]),'k-')
    #        if ( ii%2 ):
    #            if ((ii > 14) and (decGF==0)):  # a partir de SNR > 4 
    #                plt.text(mcohT2[ii],mT2[ii]-2*stdDt-0.1*(ii-13),snrc[ii], horizontalalignment='center', verticalalignment='top')
    #            else:
    #                plt.text(mcohT2[ii],mT2[ii]-2*stdDt,snrc[ii], horizontalalignment='center', verticalalignment='top')
    #        else:
    #            if ((ii > 14) and (decGF==0)):  # a partir de SNR > 4 
    #            	plt.text(mcohT2[ii],mT2[ii]+2*stdDt+0.1*(ii-13),snrc[ii], horizontalalignment='center', verticalalignment='bottom')
    #            else:
    #            	plt.text(mcohT2[ii],mT2[ii]+2*stdDt,snrc[ii], horizontalalignment='center', verticalalignment='bottom')
        if delta == 0:
            plt.xlabel(r"$coh^{1,j}$")
            plt.ylabel(r"$\delta^{1,j}$") #,fontsize=20)
        else:
            plt.xlabel(r"$coh^{2,j}$")
            plt.ylabel(r"$\delta^{2,j}$") #,fontsize=20)
        plt.title(per+" period (240 timesteps), decGF="+str(decGF)+", station:"+str(sta+1)+", master station:"+str(ref+1))
#        if (per == 'T2') and (delta == 1):
#            xlim = plt.gca().get_xlim()
#            plt.plot(xlim,0.9*np.array([1., 1.]),'r--')
#            plt.plot(xlim,1.1*np.array([1., 1.]),'r--')
        plt.savefig(per+"synthetic_obs_"+varD+str(delta)+"_coh_SNR_decGF"+str(decGF)+"_sta"+str(sta)+"_ref"+str(ref)+".png",dpi=600)
#plt.show()
