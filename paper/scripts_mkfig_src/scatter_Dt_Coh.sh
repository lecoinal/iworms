#!/bin/bash
#OAR --project iste-equ-ondes
#OAR -n scatter_Dt_coh
#OAR -l /nodes=1/core=1,walltime=00:05:00
##OAR -t besteffort

# oarsub -S "./scatter_Dt_Coh.sh Dt Coh"
# oarsub -S "./scatter_Dt_Coh.sh Dtw Coh"

set -e

source /soft/env.bash

date

START=$(date +%s.%N)

cat $OAR_NODE_FILE

##################################

varDt="$1" # Dt or Dtw
varCoh="$2" # Coh or AbsCoh

PEXE="scatter_Dt_Coh.py"

module load python/python3.9

python3.9 -u $PEXE $varDt $varCoh

END=$(date +%s.%N)
DIFF=$(echo "$END - $START" | bc)
echo "ElapsedTime: $DIFF sec"

