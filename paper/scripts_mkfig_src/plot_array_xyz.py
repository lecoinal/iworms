#!/usr/bin/env python
# -*- coding: utf-8 -*-

# sur ist-oar
# source /soft/env.bash
# module load python/python3.7
# python3 plot_array_xyz.py 


import matplotlib.pyplot as plt
import h5py
import numpy as np
import sys

import scipy.io as sio

import matplotlib as mpl
mpl.rcParams["figure.dpi"] = 600.

#mat_contents = sio.loadmat("/data/projects/f_image/lecointre/IWORMS/TEST_DONNEES_REELLES/tomo/tomo_SJFZ_multifreq.mat")
mat_contents = sio.loadmat("/summer/f_image/lecointre/IWORMS/TEST_DONNEES_REELLES/tomo/tomo_SJFZ_multifreq.mat")

from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

print(sorted(mat_contents.keys()))
fault = mat_contents['fault']
print(np.shape(fault))
print(np.nanmin(fault[:,0]),np.nanmax(fault[:,0]))
print(np.nanmin(fault[:,1]),np.nanmax(fault[:,1]))

rep = "./"

#plt.rcParams['image.cmap'] = 'gist_rainbow'
plt.rcParams['image.cmap'] = 'bwr'


#gr = np.loadtxt('EXP003_923/xyz1108.txt')
#gr = np.loadtxt('/data/projects/f_image/lecointre/IWORMS_SANJACINTO/xyz1108.txt')
gr = np.loadtxt('/summer/f_image/lecointre/IWORMS_SANJACINTO/xyz1108.txt')

#sel_gr = np.loadtxt('EXP003_923/xyz.txt')

print(np.shape(gr))
print(np.min(gr[:,0]), np.max(gr[:,0]))
print(np.min(gr[:,1]), np.max(gr[:,1]))
border = 0.0001
xmin = np.min(gr[:,0])-border
xmax = np.max(gr[:,0])+border
ymin = np.min(gr[:,1])-border
ymax = np.max(gr[:,1])+border
#xyax = [xmin, xmax, ymin, ymax]

ff = np.pi/180.0

Rt=6378000; # en m
# dist = rT * DACOS( DSIN(rlat1) * DSIN(rlat2) + DCOS(rlat1) * DCOS(rlat2) * DCOS(rlon2-rlon1) )

zf = np.nan * np.zeros((np.shape(fault)[0],))
for ifa in np.arange(np.shape(fault)[0]):
  if (fault[ifa,0] > xmin) and (fault[ifa,0] < xmax) and (fault[ifa,1] > ymin) and (fault[ifa,1] < ymax):
#    dist = (fault[ifa,0]-gr[:,0])**2 + (fault[ifa,1]-gr[:,1])**2  # attention distance en "degre"
    dist = Rt * np.arccos( np.sin(ff*fault[ifa,1]) * np.sin(ff*gr[:,1]) + np.cos(ff*fault[ifa,1]) * np.cos(ff*gr[:,1]) * np.cos(ff*(gr[:,0]-fault[ifa,0])))
  #if np.min(dist) < 0.00001:
    ii = np.argmin(dist)
    print(dist[ii])
    #zf[ifa] = 1440 # altitude "moyenne" sur la trace de passage de la faille
    #zf[ifa] = 1420 # projeter sur altitude des fonds de thalweg
    
    #zf[ifa] = gr[ii,2]
    if dist[ii] < 50:  # on retient une limite dist de 20m (un peu plus d'une demie distance)
# la dist entre 2 rangées de station c'est 30m ? 93 (km/1°lat33.5) / 3 / 1000 = 31m
        zf[ifa] = gr[ii,2]
    else:
        fault[ifa,:] = np.nan
  else:
    fault[ifa,:] = np.nan

ytk = np.array([33.536, 33.538, 33.540, 33.542])
ytklbl = [" ", "33.538°", "33.540°", "33.542°"]
xtk = np.array([-116.594, -116.592, -116.590, -116.588])
xtklbl = [" ", "-116.592°", "-116.590°", "-116.588°"]


x = gr[:,0]
y = gr[:,1]
z = gr[:,2] 
#xgrid, ygrid = np.meshgrid(x, y)

# fig = plt.figure()
# ax = fig.add_subplot(111, projection='3d', azim=-60, elev=55)
# plt.plot(fault[:,0],fault[:,1], zf, '-',color='grey')
# sc = ax.scatter(x, y, z, s=7, c=z, marker='o', edgecolor='none')
# #sc = ax.plot_surface(xgrid, ygrid, z, rstride=1, cstride=1, linewidth=0, antialiased=False)
# #ax.set_xlabel('longitude')
# #ax.set_ylabel('latitude')
# #ax.set_zlabel('elevation (m)')
# ax.set_yticks(ytk, minor=False)
# ax.set_yticklabels(ytklbl, minor=False)
# ax.set_xticks(xtk, minor=False)
# ax.set_xticklabels(xtklbl, minor=False)
# ax.set_xlim((-116.596, -116.588))
# ax.set_ylim((33.536, 33.542))
# plt.setp(ax.get_xticklabels(), color="blue")
# plt.setp(ax.get_yticklabels(), color="blue")
# #cbaxes = inset_axes(ax, width="3%", height="60%", bbox_to_anchor=(0, 0, 1, 1), loc=2) #, borderpad=5.0) 
# cbaxes = inset_axes(ax, width="3%", height="80%", loc=3, borderpad=5.0) 
# plt.colorbar(sc, cax=cbaxes, orientation='vertical')
# #plt.colorbar(sc)
# #plt.show()
# plt.savefig('./SJarray.png', dpi=200, bbox_inches=0)


# echelle 100m en zonale (x0-x1) et en méridien (y0-y1)

x0 = -116.595
y0 = 33.536
x1 = x0 + 100 * 360 / (2 * np.pi * 6400000)
y1 = y0 + 100 * 360 / (2 * np.pi * 6400000 * np.cos(y0 * np.pi / 180))



#fig = plt.figure()
#ax = fig.add_subplot(111) #, projection='3d', azim=-60, elev=55)
#plt.plot(fault[:,0],fault[:,1], zf, '-',color='grey')
#plt.plot(np.array([x0,x1]),np.array([y0,y0]),'-k')
#plt.plot(np.array([x0,x0]),np.array([y0,y1]),'-k')
#plt.text(0.5*(x0+x1),y0-0.0001,'100m',horizontalalignment='center',verticalalignment='top',color='k')
#plt.text(x0-0.0001,0.5*(y0+y1),'100m',rotation='90',horizontalalignment='right',verticalalignment='center',color='k')
#sc = ax.scatter(x, y, s=7, c=z, marker='o', edgecolor='none')
##sc2 = ax.scatter(sel_gr[:,0], sel_gr[:,1], s=7, c=sel_gr[:,2], marker='o', edgecolor='none')
#ytklbl = ["33.536°", "33.538°", "33.540°", "33.542°"]
#xtklbl = ["-116.594°", "-116.592°", "-116.590°", "-116.588°"]
#ax.set_yticks(ytk, minor=False)
#ax.set_yticklabels(ytklbl, minor=False)
#ax.set_xticks(xtk, minor=False)
#ax.set_xticklabels(xtklbl, minor=False)
#ax.set_xlim((-116.596, -116.587))
#ax.set_ylim((33.535, 33.543))
##plt.setp(ax.get_xticklabels(), color="blue")
##plt.setp(ax.get_yticklabels(), color="blue")
#cbar = plt.colorbar(sc, orientation='vertical')
#cbar.set_label('elevation (m)', rotation=90)
#plt.savefig('./SJarray_2D_bwr.png', dpi=200, bbox_inches=0)

plt.rcParams['image.cmap'] = 'gist_rainbow'
fig = plt.figure()
ax = fig.add_subplot(111) #, projection='3d', azim=-60, elev=55)
plt.plot(fault[:,0],fault[:,1], zf, '-',color='grey')
plt.plot(np.array([x0,x1]),np.array([y0,y0]),'-k')
plt.plot(np.array([x0,x0]),np.array([y0,y1]),'-k')
plt.text(0.5*(x0+x1),y0-0.0001,'100m',horizontalalignment='center',verticalalignment='top',color='k')
plt.text(x0-0.0001,0.5*(y0+y1),'100m',rotation='90',horizontalalignment='right',verticalalignment='center',color='k')
sc = ax.scatter(x, y, s=7, c=z, marker='o', edgecolor='none')
ytklbl = ["33.536°", "33.538°", "33.540°", "33.542°"]
xtklbl = ["-116.594°", "-116.592°", "-116.590°", "-116.588°"]
ax.set_yticks(ytk, minor=False)
ax.set_yticklabels(ytklbl, minor=False)
ax.set_xticks(xtk, minor=False)
ax.set_xticklabels(xtklbl, minor=False)
ax.set_xlim((-116.596, -116.587))
ax.set_ylim((33.535, 33.543))
cbar = plt.colorbar(sc, orientation='vertical')
cbar.set_label('elevation (m)', rotation=90)
plt.savefig('./SJarray_2D.png', dpi=200, bbox_inches=0)







#xgrid = np.linspace(x.min(), x.max(), np.shape(gr)[0])
#ygrid = np.linspace(y.min(), y.max(), np.shape(gr)[0])
#xgrid, ygridi, zgrid = np.meshgrid(x, y, z)
#from scipy import interpolate
#zgrid = interpolate.griddata((x,y),z, (xgrid, ygrid))
#levels = np.arange(1400, 1500, 5)
#levelslbl = np.arange(1400, 1500, 20)




### ref R3010, calcul avec abs(coh) et calcul avec elements diagonaux
##h5f = h5py.File(rep+"/out_923.h5", "r")
##vvm = np.nanmean(h5f[var][:,:], axis=1)
##vvs = np.nanstd(h5f[var][:,:], axis=1)
##iref=411
##h5f.close()
#
## ref R3010, calcul sans abs(coh) et calcul sans elements diagonaux
#iref=411
##iref=0
#h5f = h5py.File(rep+"/out_ref"+str(iref)+".h5", "r")
#if (var == "Dtw_ref") or (var == "Dt_ref"):
#    vv = h5f[var][0,:,:]
#else:
#    vv = h5f[var][:,:]
#h5f.close()
#
## remove 1d notch frequency
## notch filter to remove 24h frequency
#from scipy import signal
#fs = 1.0 / (10.0 * 60.0)   # Sample frequency (Hz)
#f0 = 1.0 / (24.0 * 3600.0) # Frequency to be removed from signal (Hz)
#Q = 30.0  # Quality factor
## Design notch filter
#b, a = signal.iirnotch(f0, Q, fs)
## remove 1d and 2d
#f0 = 1.0 / (2.0 * 24.0 * 3600.0) # Frequency to be removed from signal (Hz)
#bb, aa = signal.iirnotch(f0, Q, fs)
## remove all under 1d
#f0 = 1.0 / (24.0 * 3600.0) # Frequency to be removed from signal (Hz)
#nyq = fs*0.5
#bbb, aaa = signal.butter(4, f0/nyq, btype="high")
#
#
#nsta = np.shape(vv)[1]
#print("nsta",nsta)
##for ista in np.arange(nsta):
##    tmp = signal.filtfilt(b, a, vv[:,ista])
##    tmp2 = signal.filtfilt(bb, aa, tmp)
##    tmp3 = signal.filtfilt(bbb, aaa, tmp2)
##    vv[:,ista] = tmp3
#
#
#
#
#vvm = np.nanmean(vv, axis=0)
#vvs = np.nanstd(vv, axis=0)
#
#
#if (var == "Dtw_ref") or (var == "Dt_ref"):
#
#    plt.rcParams['image.cmap'] = 'bwr'
#    plt.clf()
#    plt.contour(xgrid, ygrid, zgrid, levels, colors=('grey',), linestyle="dashed", linewidth=1)
#    CS = plt.contour(xgrid, ygrid, zgrid, levelslbl, colors=('grey',))
#    plt.clabel(CS, fontsize=9, inline=1, color='g')
#    plt.plot(fault[:,0],fault[:,1],'k')
#    plt.plot(gr[iref,0], gr[iref,1],'ok',mfc='none')
#    #cmax = 0.5*np.max(np.abs(vvm[:]))
#    print("maxabs mean",np.max(np.abs(vvm[:])))
#    cmax = 1e-3 # 3e-5 # 1e-3
#    plt.scatter(gr[:,0], gr[:,1], s=15, c=vvm[:], vmin=-cmax, vmax=cmax, edgecolor='none')
#    #plt.scatter(gr[iref,0], gr[iref,1], s=15, c=vvm[iref], marker="*", vmin=-cmax, vmax=cmax, edgecolor='none')
#    #if (var == "Dtw_ref"):
#    #    plt.title(r'a) 19days averaged $\Delta t_{w}$ (sec)', loc='right')
#    #else:
#    #    plt.title(r'19days averaged $\Delta t$ (sec)', loc='right')
#    plt.colorbar()
#
#    plt.rcParams['image.cmap'] = 'gist_rainbow'
#    #print(vvs[iref])
#    #vvs[iref]=9999.
#    #cmin=np.min(vvs)
#    #vvs[iref]=0.0
#    #cmax=np.max(vvs)
#    print("maxstd",np.max(vvs[:]))
#    vvs[iref]=9999.
#    print("minstd",np.min(vvs[:]))
#    vvs[iref]=0.
#    cmin = 1e-3 # 1.5e-3
#    cmax = 3e-3 # 5e-3
#    plt.clf()
#    plt.plot(fault[:,0],fault[:,1],'k')
#    plt.plot(gr[iref,0], gr[iref,1],'ok',mfc='none')
#    plt.scatter(gr[:,0], gr[:,1], s=15, c=vvs[:], vmin=cmin, vmax=cmax, edgecolor='none')
#    #plt.scatter(gr[iref,0], gr[iref,1], s=15, c=vvs[iref], marker="*", vmin=cmin, vmax=cmax, edgecolor='none')
#    if (var == "Dtw_ref"):
#        plt.title(r'b) 19days stddev $\Delta t_{w}$ (sec)', loc='right')
#    else:
#        plt.title(r'19days stddev $\Delta t$ (sec)', loc='right')
#    plt.colorbar()
#    plt.axis(xyax)
#    plt.savefig('./std_'+var+'.png', dpi=200, bbox_inches=0)
#
#else:
#
#    print("min",np.min(vvm[:]))
#    print("max",np.max(vvm[:]))
#
#    cmin = -2e-4 # 0.48
#    cmax = 12e-4 # 0.74
#    plt.clf()
#    plt.plot(fault[:,0],fault[:,1],'k')
#    plt.plot(gr[iref,0], gr[iref,1],'ok',mfc='none')
#    plt.scatter(gr[:,0], gr[:,1], s=15, c=vvm[:], vmin=cmin, vmax=cmax, edgecolor='none') # , vmin=cmin, vmax=cmax)
#    #plt.scatter(gr[iref,0], gr[iref,1], s=15, c=vvm[iref], marker="*", vmin=cmin, vmax=cmax, edgecolor='none') # , vmin=cmin, vmax=cmax)
#    plt.title(r'c) 19days averaged Coherence', loc='right')
#    plt.colorbar()
#    plt.axis(xyax)
#    plt.savefig('./mean_'+var+'.png', dpi=200, bbox_inches=0)
#
#    plt.clf()
#    plt.hist(vvm)
#    plt.savefig('./histmean_'+var+'.png', dpi=200, bbox_inches=0)
#
#    print("min",np.min(vvs[:]))
#    print("max",np.max(vvs[:]))
#
#    cmin = 0.03 # 0.09
#    cmax = 0.07 # 0.16
#    plt.clf()
#    plt.plot(fault[:,0],fault[:,1],'k')
#    plt.plot(gr[iref,0], gr[iref,1],'ok',mfc='none')
#    plt.scatter(gr[:,0], gr[:,1], s=15, c=vvs[:], vmin=cmin, vmax=cmax, edgecolor='none') # , vmin=cmin, vmax=cmax)
#    #plt.scatter(gr[iref,0], gr[iref,1], s=15, c=vvs[iref], marker="*", vmin=cmin, vmax=cmax, edgecolor='none') # , vmin=cmin, vmax=cmax)
#    plt.title(r'19days stddev Coherence', loc='right')
#    plt.colorbar()
#    plt.axis(xyax)
#    plt.savefig('./std_'+var+'.png', dpi=200, bbox_inches=0)
#
#    plt.clf()
#    plt.hist(vvs)
#    plt.savefig('./histstd_'+var+'.png', dpi=200, bbox_inches=0)
#
