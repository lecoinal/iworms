#!/bin/bash
##OAR --project iste-equ-ondes
#OAR --project iworms
#OAR -n plot_dt_coh_snr
#OAR -l /nodes=1/core=1,walltime=00:05:00

set -e

#source /soft/env.bash
source /applis/site/guix-start.sh
refresh_guix pyplot

date

START=$(date +%s.%N)

cat $OAR_NODE_FILE

##################################

vardec="$1" # 0 or 1
varDt="$2" # Dt or Dtw
varper="$3" # T1 or T2

# python3 plot_dt_coh_snr.py --dec 0 --var Dt --per T2 --sitem "a) " # oarsub -S "./plot_dt_coh_snr.sh 0 Dt T2"
# python3 plot_dt_coh_snr.py --dec 0 --var Dt --per T1 --sitem "d) " # oarsub -S "./plot_dt_coh_snr.sh 0 Dt T1"
# python3 plot_dt_coh_snr.py --dec 1 --var Dt --per T2 --sitem "b) " # oarsub -S "./plot_dt_coh_snr.sh 1 Dt T2"
# python3 plot_dt_coh_snr.py --dec 0 --var Dtw --per T2 --sitem "c) " # oarsub -S "./plot_dt_coh_snr.sh 0 Dtw T2"
### python3 plot_dt_coh_snr.py --dec 1 --var Dtw --per T2 # oarsub -S "./plot_dt_coh_snr.sh 1 Dtw T2"
### python3 plot_dt_coh_snr.py --dec 1 --var Dt --per T1  # oarsub -S "./plot_dt_coh_snr.sh 1 Dt T1"

PEXE="plot_dt_coh_snr.py"

#module load python/python3.9

#python3.9 -u $PEXE --dec $vardec --var $varDt --per $varper
python3 -u $PEXE --dec $vardec --var $varDt --per $varper

END=$(date +%s.%N)
DIFF=$(echo "$END - $START" | bc)
echo "ElapsedTime: $DIFF sec"

