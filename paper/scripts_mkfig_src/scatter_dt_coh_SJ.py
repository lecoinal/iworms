#!/usr/bin/env python
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import matplotlib.colors as colors
import h5py
import numpy as np
import sys

plt.rcParams['image.cmap'] = 'gist_rainbow'
plt.rcParams["lines.linewidth"] = 0.7

rep = "/data/projects/f_image/lecointre/IWORMS/TEST_DONNEES_REELLES/GRAPHE_COMPLET_132_150_923/"

h5f = h5py.File(rep+"/out_39415926.h5", "r")

#dist = np.loadtxt('./pair_dist_3.txt')
#print(dist.shape)
#print(np.min(dist),np.max(dist))
#aa, be = np.histogram(dist, bins=[0, 100, 500, 900])
#print(aa)

coh = h5f["/coh"][:,:].flatten()
dt = h5f["/dt"][:,:].flatten()

print(np.shape(coh))
print(np.shape(dt))

xaxmin=-1.0
xaxmax=1.0
yaxmin=-0.05
yaxmax=0.05

print("coh range:",np.nanmin(coh),np.nanmax(coh))
print("dt range:",np.nanmin(dt),np.nanmax(dt))

if np.nanmin(coh)-0.01 < xaxmin:
  xaxmin = np.nanmin(coh)-0.01
if np.nanmax(coh)+0.01 > xaxmax:
  xaxmax = np.nanmax(coh)+0.01
if np.nanmin(dt)-0.001 < yaxmin:
  yaxmin = np.nanmin(dt)-0.001
if np.nanmax(dt)+0.001 > yaxmax:
  yaxmax = np.nanmax(dt)+0.001

#yaxmin = -0.06
#yaxmax = 0.06

# compute the 2D histogram data
coh_edges = np.arange(-1,1+0.01,0.01)
dt_edges = np.arange(np.nanmin(dt)-0.1,np.nanmax(dt)+0.1+0.001,0.001)
H, xedges, yedges = np.histogram2d(coh, dt, bins=(coh_edges, dt_edges))
H = H.T  # Let each row list bins with common y range.

coh = None
dt = None

Hnan = H.copy()
Hnan[Hnan == 0] = np.nan
# maske les bins avec occurrences < 5
#Hnan[Hnan < 5] = np.nan

Hcolorbar = H.copy()
Hcolorbar[Hcolorbar == 0] = 1

print('clim',Hcolorbar.min(),H.max())

lvl=np.array([10, 100, 1000, 10000, 100000, 1000000])
plt.clf()
fig, ax = plt.subplots(1, 1)
X, Y = np.meshgrid(xedges, yedges)
print(np.nanmax(Hnan))
pcm = ax.pcolormesh(X, Y, Hnan, norm=colors.LogNorm(vmin=5.0, vmax=H.max()))  # 1500 limite max pour dt/coh
ax.grid()
plt.title('2725 time-steps and 923 stations')

fig.colorbar(pcm, ax=ax, extend='max', pad=0.12)

plt.xlabel(r"coh")
plt.ylabel(r"$\delta$ (s)")
plt.xlim((xaxmin,xaxmax))
plt.ylim((yaxmin,yaxmax))
ax2 = ax.twinx()
ax2.set_ylim((yaxmin,yaxmax))
ytk = np.array([-0.50, -0.20, -0.10, -0.05, -0.02, 0.02, 0.05, 0.10, 0.20, 0.50]) * 1./4.35 # 5% de T, 10% de T, 20% de T
ax2.set_yticks(ytk)
ax2.set_yticklabels(["50%T","20%T","10%T","5%T","2%T","2%T","5%T","10%T","20%T","50%T"])
plt.contour(X[0:-1,0:-1], Y[0:-1,0:-1], Hnan, lvl, colors='k')
fig.savefig('./scatter_dt_coh_SJ_contours.png', dpi=200, bbox_inches=0)

h5f.close()
