#!/usr/bin/env python
# -*- coding: utf-8 -*-

import h5py
import numpy as np
import sys
import math
from scipy.signal import butter, lfilter
import matplotlib.pyplot as plt
import argparse
import matplotlib as mpl

mpl.rcParams["figure.figsize"] = [7.4, 4.8]
mpl.rcParams["lines.linewidth"] = 1.0
mpl.rcParams["lines.markersize"] = 1.0
mpl.rcParams["figure.dpi"] = 600.
mpl.rcParams["font.size"] = 12.

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="measure dt and compute Dt on synthetic data")
    parser.add_argument("--dec", default=0, type=int,
                        help="set option to apply a dcreasing factor of GF amplitude for distance > 20km (default=0 : disable)")
    parser.add_argument("--var", default="Dt", type=str,
                        help="plotted variable: clock error Dt or weighted clock error Dtw (default='Dt' : non weighted clock error)")
    parser.add_argument("--per", default="T2", type=str,
                        help="period: well synchronized T1 period or one-sec desynchronized T2 period (default='T2')")
    parser.add_argument("--sitem", default="", type=str,
                        help="item for prefixing the title of the figure: a), b), c), d) (default='')")

    args = parser.parse_args()
    decGF = args.dec # sys.argv[2]  #  0 : no GF amplitude decrease with distance
                                    # 1 : after 20km, decrease GF amplitude with factor 1/sqrt(dist/20km)
    varD = args.var
    per = args.per

    sitem = args.sitem

    nbpair = 3081
    nbsta = 79

    SW = 2

    snr = np.array([0.01, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1, 1.5, 2, 3, 4, 5, 10, 100])
    snrc = []
    snrc.append("0.01")
    snrc.append("0.1")
    snrc.append("0.2")
    snrc.append("0.3")
    snrc.append("0.4")
    snrc.append("0.5")
    snrc.append("0.6")
    snrc.append("0.7")
    snrc.append("0.8")
    snrc.append("0.9")
    snrc.append("1.0")
    snrc.append("1.5")
    snrc.append("2.0")
    snrc.append("3.0")
    snrc.append("4.0")
    snrc.append("5.0")
    snrc.append("10.0")
    snrc.append("100.0")
 
    # plot S1 par rapport S2 :
    sta = 0
    ref = 1
 
    mT2 = np.empty((len(snr),),dtype=np.float32)
    mcohT2 = np.empty((len(snr),),dtype=np.float32)
    mT2_step = np.empty((len(snr),240),dtype=np.float32)
    mcohT2_step = np.empty((len(snr),240),dtype=np.float32)

    if per == 'T2':
        ib=240
        ie=480
        strper='$T_2$'
    else: # T1
        ib=0
        ie=240
        strper='$T_1$'

    fc= 0.15  # entre 0.1 et 0.2 Hz

    #idir = "/nfs_scratch/lecoinal/zzzzz/"
    #idir = "/bettik/lecoinal/zzzzz/"
    idir = "/summer/f_image/lecointre/IWORMS/SYNTHETIC/"

    for ii in np.arange(len(snr)):
        print(snr[ii])
        #filename = idir+"/SNR"+snrc[ii]+"_SW"+str(SW)+"_CC0_decGF"+str(decGF)+"_REF240_LIN0_nstep480_nbpair"+str(nbpair)+".h5"
        #filename = idir+"/2Tover3/SNR"+snrc[ii]+"_SW"+str(SW)+"_CC0_decGF"+str(decGF)+"_REF240_LIN0_nstep480_nbpair"+str(nbpair)+".h5"
        filename = idir+"/SNR"+snrc[ii]+"_SW"+str(SW)+"_CC0_decGF"+str(decGF)+"_REF240_LIN0_nstep480_nbpair"+str(nbpair)+".h5"
        f = h5py.File(filename,'r')
        #try:
        mT2_step[ii,:] = f['/'+varD][ref,ib:ie,sta] * fc    # en nombre de periode
        #except:
        #    pass
        mT2[ii] = np.mean(mT2_step[ii,:])
        mcohT2_step[ii,:] = np.mean(f['/coh'][ib:ie,0:nbsta-1],axis=1)  # les coh liees a la premiere station : le nbsta-1 premieres paires
        mcohT2[ii] = np.mean(mcohT2_step[ii,:])
        f.close()
        
    plt.clf()
    plt.grid()
    plt.plot(mcohT2_step[0],mT2_step[0],'.',color='blue',label=r"at each time step of the "+strper+" period",zorder=10)
    plt.plot(mcohT2_step[1:],mT2_step[1:],'.',color='blue',zorder=10)
    plt.plot(mcohT2, mT2, 'o',color='black', fillstyle='none', markersize=5,label="averaged along these 240 time steps",zorder=15)
#    col = ['#549CFD', '#D4DA42', '#53392B', '#F6D68A', '#63BBD0', '#6D5E40', '#DBF6EB', '#F6CAC3', '#7D8358', '#27F112', '#7AFEB3', '#8AE46B', '#E6001D', '#A250F6', '#F9AC0D', '#9B20FB', '#CFFEAD', '#478DF4']
#    for ii in np.arange(len(snr)):
#        plt.plot(mcohT2[ii], mT2[ii], 'o',color=col[ii], markersize=5)
    plt.legend(loc=4)
    for ii in np.arange(len(snr)):
        stdDt = np.std(mT2_step[ii,:])
        stdcoh = np.std(mcohT2_step[ii,:])
        plt.plot(mcohT2[ii]+2*stdcoh*np.array([-1,1]),mT2[ii]*np.array([1,1]),'k-',zorder=15)
        plt.plot(mcohT2[ii]*np.array([1,1]),mT2[ii]+2*stdDt*np.array([-1,1]),'k-',zorder=15)
        if ( ii%2 ):
            if ((ii > 14) and (decGF==0)):  # a partir de SNR > 4 
                #plt.text(mcohT2[ii],mT2[ii]-2*stdDt-0.1*(ii-13),snrc[ii], horizontalalignment='center', verticalalignment='top')
                plt.text(mcohT2[ii],mT2[ii]-2*stdDt-0.01*(ii-13),snrc[ii], horizontalalignment='center', verticalalignment='top')
            else:
                plt.text(mcohT2[ii],mT2[ii]-2*stdDt,snrc[ii], horizontalalignment='center', verticalalignment='top')
        else:
            if ((ii > 14) and (decGF==0)):  # a partir de SNR > 4 
            	#plt.text(mcohT2[ii],mT2[ii]+2*stdDt+0.1*(ii-13),snrc[ii], horizontalalignment='center', verticalalignment='bottom')
            	plt.text(mcohT2[ii],mT2[ii]+2*stdDt+0.01*(ii-13),snrc[ii], horizontalalignment='center', verticalalignment='bottom')
            else:
            	plt.text(mcohT2[ii],mT2[ii]+2*stdDt,snrc[ii], horizontalalignment='center', verticalalignment='bottom')
    plt.xlabel(r"$Coh^{1}$")
    if varD == 'Dt':
        plt.ylabel(r"$\Delta^{1|2}$ (in number of periods)") #,fontsize=20)
    else:
        plt.ylabel(r"coherence-weighted $\widetilde{\Delta}^{1|2}$ (in number of periods)") #,fontsize=20)
    plt.title(r""+sitem+strper+" period, decGF="+str(decGF)+", station:"+str(sta+1)+", master station:"+str(ref+1))
    if per == 'T2':
        plt.plot([0, 1.1],0.15*np.array([1., 1.]),'r--',zorder=5)
    else:
        plt.plot([0, 1.1],0.0*np.array([1., 1.]),'r--',zorder=5)
    plt.gca().set_xlim([0, 1.1])
    #xlim = plt.gca().get_xlim()
    #plt.plot(xlim,0.02+np.array([0., 0.]),'r--')
    #plt.plot(xlim,-0.02+np.array([0., 0.]),'r--')

    ylim = plt.gca().get_ylim()
    print(ylim)
    if per == 'T2':
        plt.gca().set_ylim([-0.23044456765055657, 0.27363512143492696])
#        plt.gca().set_ylim([-0.4, 0.27363512143492696])

#    ax1 = plt.gca()
##    ylim = ax.get_ylim()
##    print(ylim)
#    ax2 = ax1.twinx()
#    def period2second(t):
#        return t/fc + 1
#    def convert_ax2(ax1):
#        y1, y2 = ax1.get_ylim()
#        ax2.set_ylim(period2second(y1), period2second(y2))
#        ax2.figure.canvas.draw()    
#
#    ax1.callbacks.connect("ylim_changed", convert_ax2)   
##    ax2.set_ylim(ylim)
##    ytk = np.array([-0.20, -0.10, -0.05, -0.02, 0.02, 0.05, 0.10, 0.20]) * 1./fc # 5% de T, 10% de T, 20% de T
##    ytk = np.array([-0.20, -0.10, -0.05, -0.02, 0.0, 0.02, 0.05, 0.10, 0.20]) * 1./fc # 5% de T, 10% de T, 20% de T
##    ax2.set_yticks(ytk)
##    print(ytk)
##    #ax2.set_yticklabels(["20%T","10%T","5%T","2%T","2%T","5%T","10%T","20%T"])

    plt.savefig(per+"synthetic_"+varD+"_coh_SNR_decGF"+str(decGF)+"_sta"+str(sta)+"_ref"+str(ref)+".png",dpi=600)
    #plt.show()
