#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
#import sys
#import math
import matplotlib.pyplot as plt
#import argparse
import matplotlib as mpl

mpl.rcParams["figure.figsize"] = [11.4, 5.8]
mpl.rcParams["lines.linewidth"] = 1.0
mpl.rcParams["lines.markersize"] = 1.0
mpl.rcParams["figure.dpi"] = 600.

if __name__ == '__main__':

    day = np.loadtxt('SJ_coh_1.txt')   # day, nbsta, nbpair
    meancoh = np.loadtxt('SJ_coh_2.txt')  # meancoh among all pairs
    stdcoh = np.loadtxt('SJ_coh_3.txt')   # stdcoh among all pairs


    plt.clf()
    fid, ax = plt.subplots()
    plt.grid()


    for id in np.arange(31):
        plt.text(0.5+day[id,0],0.01,'{:d}'.format(int(day[id,2])), rotation=90, horizontalalignment='center', verticalalignment='bottom')

    iexp = 7
    d = 0.5+day[:,0]
    plt.plot(d, meancoh[:,iexp],'o-',color='b',fillstyle='none',markersize=5,label=r"$step 10min, window 10min, t_{zoom} [-2:2]sec$")
#    for id in np.arange(31):
#        plt.plot(d[id]*np.array([1,1]), meancoh[id,iexp]+stdcoh[id,iexp]*np.array([-1,1]),color='b',linewidth=0.5)
    print('blue circles 31d, 19d', np.mean(meancoh[:,iexp]), np.mean(meancoh[4:23,iexp]))

    iexp = 6
#    d = d + 1./8.
    plt.plot(d, meancoh[:,iexp],'o-',color='y',fillstyle='none',markersize=5,label=r"$10min, 10min, [-1:1]$")
#    for id in np.arange(31):
#        plt.plot(d[id]*np.array([1,1]), meancoh[id,iexp]+stdcoh[id,iexp]*np.array([-1,1]),color='y',linewidth=0.5)
    print('yellow circles 31d, 19d', np.mean(meancoh[:,iexp]), np.mean(meancoh[4:23,iexp]))

#    iexp = 5
#    d = d + 1./8.
#    plt.plot(d, meancoh[:,iexp],'o-',color='g',fillstyle='none',markersize=5,label=r"$t_{zoom} [0:-1]+[0:1]sec$")
#    for id in np.arange(31):
#        plt.plot(d[id]*np.array([1,1]), meancoh[id,iexp]+stdcoh[id,iexp]*np.array([-1,1]),color='g',linewidth=0.5)

    iexp = 4
#    d = d + 1./8.
    plt.plot(d, meancoh[:,iexp],'o-',color='r',fillstyle='none',markersize=5,label=r"$10min, 10min, [0:-1]+[0:1]$")
#    for id in np.arange(31):
#        plt.plot(d[id]*np.array([1,1]), meancoh[id,iexp]+stdcoh[id,iexp]*np.array([-1,1]),color='r',linewidth=0.5)
    print('red circles 31d, 19d', np.mean(meancoh[:,iexp]), np.mean(meancoh[4:23,iexp]))

    iexp = 3
#    d = d + 1./8.
    plt.plot(d, meancoh[:,iexp],'o-',color='g',fillstyle='none',markersize=5,label=r"$2h, 2h, [0:-1]+[0:1]$")
#    for id in np.arange(31):
#        plt.plot(d[id]*np.array([1,1]), meancoh[id,iexp]+stdcoh[id,iexp]*np.array([-1,1]),color='g',linewidth=0.5)
    print('green circles 31d, 19d', np.mean(meancoh[:,iexp]), np.mean(meancoh[4:23,iexp]))

    iexp = 1
#    d = d + 1./8.
    plt.plot(d, meancoh[:,iexp],'o-',color='m',fillstyle='none',markersize=5,label=r"$10min, 2h, [0:1]$")
#    for id in np.arange(31):
#        plt.plot(d[id]*np.array([1,1]), meancoh[id,iexp]+stdcoh[id,iexp]*np.array([-1,1]),color='m',linewidth=0.5)
    print('magenta circles 31d, 19d', np.mean(meancoh[:,iexp]), np.mean(meancoh[4:23,iexp]))

    iexp = 0
#    d = d + 1./8.
    plt.plot(d, meancoh[:,iexp],'o-',color='c',fillstyle='none',markersize=5,label=r"$10min, 2h, [0:-1]$")
#    for id in np.arange(31):
#        plt.plot(d[id]*np.array([1,1]), meancoh[id,iexp]+stdcoh[id,iexp]*np.array([-1,1]),color='c',linewidth=0.5)
    print('cyan circles 31d, 19d', np.mean(meancoh[:,iexp]), np.mean(meancoh[4:23,iexp]))

    iexp = 2
#    d = d + 1./8.
    plt.plot(d, meancoh[:,iexp],'o-',color='k',fillstyle='none',markersize=5,label=r"$10min, 2h, [0:-1]+[0:1]$")
    for id in np.arange(31):
        plt.plot(d[id]*np.array([1,1]), meancoh[id,iexp]+stdcoh[id,iexp]*np.array([-1,1]),'--',color='k',linewidth=0.7)
    print('black circles 31d, 19d', np.mean(meancoh[:,iexp]), np.mean(meancoh[4:23,iexp]))

    #print(meancoh[:,2])
    #print(np.mean(meancoh[:,2]))
    #print(meancoh[4:23,2])
    #print(np.mean(meancoh[4:23,2]))

    plt.legend(ncol=3, loc=9)
    plt.xlabel("day of year 2014")
    plt.ylabel(r"averaged coherence among all $\delta$ measurements")
    plt.xlim([127.9,159.0])
    plt.ylim([0.0,1.0])
    plt.xticks(np.arange(128,159),rotation='vertical')


    rect = plt.Rectangle((128, 0.0), 4, 1.0, color='k', alpha=0.2)
    ax.add_patch(rect)
    rect = plt.Rectangle((151, 0.0), 8, 1.0, color='k', alpha=0.2)
    ax.add_patch(rect)

    plt.savefig("coh_paramdelta_SJ_old.png",dpi=600)
    
