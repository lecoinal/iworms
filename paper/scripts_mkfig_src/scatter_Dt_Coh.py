#!/usr/bin/env python
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import matplotlib.colors as colors
import h5py
import numpy as np
import sys

plt.rcParams['image.cmap'] = 'gist_rainbow'
plt.rcParams["lines.linewidth"] = 1.0
plt.rcParams["figure.figsize"] = [7.4, 4.8]
plt.rcParams["font.size"] = 12.

#rep = "/data/projects/f_image/lecointre/IWORMS/TEST_DONNEES_REELLES/GRAPHE_COMPLET_132_150_923"
rep = "/summer/f_image/lecointre/IWORMS/TEST_DONNEES_REELLES/GRAPHE_COMPLET_132_150_923"

varDt = sys.argv[1] # "Dtw" # Dt ou Dtw
varCoh = sys.argv[2] # "AbsCoh" # "Coh" ou "AbsCoh"

## ref R3010 ou R0101, calcul sans abs(coh) et calcul sans elements diagonaux
#iref=411
##iref=0
#h5f = h5py.File(rep+"/out_ref"+str(iref)+".h5", "r")
#Dt = h5f[varDt+"_ref"][0,:,:].flatten()
#Coh = h5f[varCoh][:,:].flatten()

h5f = h5py.File(rep+"/out_39415926.h5", "r")
Dt = h5f[varDt][:,:].flatten()*1e3 # en millisec
Coh = h5f[varCoh][:,:].flatten()

#mask = np.all(np.isnan(Dt), axis=1)
#print(mask.shape)
#Dt = Dt[~mask].flatten()
#Coh = Coh[~mask].flatten()

print(Dt.shape)

h5f.close()

xaxmin=-0.1
xaxmax=0.95
yaxmin=-0.05*1e3
yaxmax=0.05*1e3

print("Coh range:",np.nanmin(Coh),np.nanmax(Coh))
print("Dt range:",np.nanmin(Dt),np.nanmax(Dt))

if np.nanmin(Coh)-0.01 < xaxmin:
  xaxmin = np.nanmin(Coh)-0.01
if np.nanmax(Coh)+0.01 > xaxmax:
  xaxmax = np.nanmax(Coh)+0.01
if np.nanmin(Dt)-0.001 < yaxmin:
  yaxmin = np.nanmin(Dt)-0.001*1e3
if np.nanmax(Dt)+0.001 > yaxmax:
  yaxmax = np.nanmax(Dt)+0.001*1e3

yaxmin = -0.052*1e3 # -0.05
yaxmax = 0.052*1e3 # 0.05

# compute the 2D histogram data
coh_edges = np.arange(-1,1+0.01,0.01)
#dt_edges = np.arange(-0.1,0.1+0.001,0.001)
dt_edges = np.arange(np.nanmin(Dt)-0.1*1e3,np.nanmax(Dt)+0.1*1e3+0.001*1e3,0.001*1e3)
H, xedges, yedges = np.histogram2d(Coh, Dt, bins=(coh_edges, dt_edges))
H = H.T  # Let each row list bins with common y range.

Hnan = H.copy()
Hnan[Hnan == 0] = np.nan
# maske les bins avec occurrences < 5
Hnan[Hnan < 5] = np.nan

Hcolorbar = H.copy()
Hcolorbar[Hcolorbar == 0] = 1

print('clim',Hcolorbar.min(),H.max())

## pcolormesh can display actual edges:
#plt.clf()
#fig, ax = plt.subplots(1, 1)
#X, Y = np.meshgrid(xedges, yedges)
##pcm = ax.pcolormesh(X, Y, Hnan, norm=colors.LogNorm(vmin=Hcolorbar.min(), vmax=H.max()))
#pcm = ax.pcolormesh(X, Y, Hnan, norm=colors.LogNorm(vmin=1.0, vmax=21103.0))
#ax.grid()
#plt.title('2725 timesteps and 923 sensors between JD132-JD150')
#fig.colorbar(pcm, ax=ax, extend='max')
#if varCoh == "Coh":
#    plt.xlabel(r"$Coh$")
#else:
#    plt.xlabel(r"$AbsCoh$")
#if varDt == "Dt":
#    plt.ylabel(r"$\Delta$")
#else:
#    plt.ylabel(r"$\Delta w$")
#plt.xlim((xaxmin,xaxmax))
#plt.ylim((yaxmin,yaxmax))
#fig.savefig('./scatter_'+varDt+'_'+varCoh+'_pcolormesh.png', dpi=200, bbox_inches=0)

lvl=np.array([10, 100, 1000, 10000])
plt.clf()
fig, ax = plt.subplots(1, 1)
X, Y = np.meshgrid(xedges, yedges)
print(np.nanmax(Hnan))
#pcm = ax.pcolormesh(X, Y, Hnan, norm=colors.LogNorm(vmin=1.0, vmax=21103.0))
#pcm = ax.pcolormesh(X, Y, Hnan, norm=colors.LogNorm(vmin=5.0, vmax=21103.0))
pcm = ax.pcolormesh(X, Y, Hnan, norm=colors.LogNorm(vmin=5.0, vmax=15000.0))  # 15000 limite max pour Dtw/Coh et Dt/Coh
ax.grid()

fig.colorbar(pcm, ax=ax, extend='max', pad=0.12)

if varCoh == "Coh":
    plt.xlabel(r"Coh")
else:
    plt.xlabel(r"$AbsCoh$")
if varDt == "Dt":
    plt.ylabel(r"$\Delta$ (ms)")
    plt.title('b) 2725 time steps and 923 stations')
else:
    plt.ylabel(r"coherence-weighted $\widetilde{\Delta}$ (ms)")
    #ax.set_ylabel(r"$<\Delta>$ (s)", rotation=90, color='k', labelpad=-5)
    plt.title('a) 2725 time steps and 923 stations')
plt.xlim((xaxmin,xaxmax))
plt.ylim((yaxmin,yaxmax))
ax2 = ax.twinx()
ax2.set_ylim((yaxmin,yaxmax))
ytk = np.array([-0.20, -0.10, -0.05, -0.02, 0.02, 0.05, 0.10, 0.20]) * 1e3 * 1./4.35 # 5% de T, 10% de T, 20% de T
ax2.set_yticks(ytk)
ax2.set_yticklabels([r"$20\%T_0$",r"$10\%T_0$",r"$5\%T_0$",r"$2\%T_0$",r"$2\%T_0$",r"$5\%T_0$",r"$10\%T_0$",r"$20\%T_0$"])
plt.contour(X[0:-1,0:-1], Y[0:-1,0:-1], Hnan, lvl, colors='k')
fig.savefig('./scatter_'+varDt+'_'+varCoh+'_contours.png', dpi=200, bbox_inches=0)
