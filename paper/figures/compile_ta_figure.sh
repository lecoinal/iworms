#!/bin/sh

set -e 

#convert -crop 1245x460+55+260 SJFZ0.png SJFZ00.png # capture d'ecran carte USGS
##convert -size 2600x1400  xc:white SJFZ0c.png -gravity southwest -composite SJFZ00.png
##rm SJFZ0c.png

##########################

#convert -transparent white SJarray.png SJarray_tr.png # plot3d matplotlib avec positions stations

#####################

# background ARCGISIMAGE + GIS failles GeoPandas

#rsync -av ist-oar.ujf-grenoble.fr:~/ARCGIS/*_jacinto.png ./background/.
#rsync -av ist-ssh.ujf-grenoble.fr:~/ARCGIS/*_jacinto.png ./background/.

convert fault_all.png -transparent white tmp.png
# convert tmp.png -fuzz 10% -fill red +opaque none tmp_all.png # toutes les failles en rouge ?
mv tmp.png tmp_faults.png

f="./background/ESRI_Imagery_World_2D_jacinto.png"
convert $f -fill transparent -stroke white -strokewidth 5 -draw "rectangle 1226,895 1270,933" ./background/tmp.png

composite -compose Over -geometry +0+10 -gravity Center tmp_faults.png ./background/tmp.png tmp.png
rm ./background/tmp.png
convert tmp.png -crop 2075x1190+100+340 faults_$(basename $f)
identify faults_$(basename $f)
#convert -size 2500x1650  xc:white faults_$(basename $f) -gravity southwest -composite tmp.png
#mv tmp.png faults_$(basename $f)
#identify faults_$(basename $f)

rm tmp_faults.png 

#############################

#composite -compose Over -geometry +0+0 -gravity NorthEast SJarray_2D.png faults_$(basename $f) tmp.png # SJFZ000.png
#composite -compose Over -geometry +0+0 -gravity NorthWest SJFZ00.png SJFZ000.png tmp.png

convert -trim -border 25 -bordercolor white faults_$(basename $f) tmp.png
mv tmp.png faults_$(basename $f)
identify faults_$(basename $f)
convert -trim -border 25 -bordercolor white SJarray_2D.png tmp.png
mv tmp.png SJarray_2D.png
identify SJarray_2D.png

montage -geometry 2050x1400 -tile 1x2 faults_$(basename $f) SJarray_2D.png tmp.png

#rm faults_$(basename $f)
convert -trim -border 25 -bordercolor white tmp.png SJFZ.png
rm tmp.png



