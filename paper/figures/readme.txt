

# Fig 2
montage -tile 2x2 -geometry 4440x2880 pair1_decGF0_SNR08.png pair0_decGF0_SNR08.png pair1_decGF0_SNR50.png pair0_decGF0_SNR50.png Fig2.png

# Fig 3
montage -tile 2x2 -geometry 4440x2880 T2synthetic_Dt_coh_SNR_decGF0_sta0_ref1.png T2synthetic_Dtw_coh_SNR_decGF0_sta0_ref1.png T2synthetic_Dt_coh_SNR_decGF1_sta0_ref1.png T1synthetic_Dt_coh_SNR_decGF0_sta0_ref1.png Fig3.png

# Fig 4
montage -tile 2x1 -geometry 1480x960 scatter_Dtw_Coh_contours.png  scatter_Dt_Coh_contours.png Fig4.png

# Fig 6
montage -tile 2x2 -geometry 3840x2880 SJarray_2D.png null: std_Dtw.png mean_Coh.png Fig6.png
#montage -tile 2x2 -geometry 3840x2880 SJarray_2D.png mean_Dtw.png std_Dtw.png mean_Coh.png Fig6.png
#montage -tile 2x2 -geometry 3840x2880 SJarray_2D.png mean_Coh.png std_Dtw.png std_Dt.png Fig6d.png

