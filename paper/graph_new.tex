\section{Graph formalism for a seismic network}

Geophysical sensors connected by a set of cross-correlation functions, as studied in this paper, can be formalized as a graph \citep{spyrou2019}. To do so, we consider a geophone array with a fixed set of sensors, also called stations, $S_n = \{s_1,\ldots,s_n\}$. Each sensor is modeled by a node in the graph, and the NCCF between node $s_j$ and node $s_k$ is modeled by a link from $j$ to $k$ in the graph. Here, we write $e_{jk}$ as the edge of the graph corresponding to this link.

In this study, we assume that the link between $j$ and $k$ is symmetric, and so, the pair $s_j$, $s_k$ is unordered. This means that it does not make any difference if the peak in the NCCF appears in the positive or negative time of the cross-correlation function. 

With such a definition, we can still experience unconnected nodes. In our context, this means that none of the NCCFs between station $s_j$ and every other station in $S_n$ can be computed because the time series in $s_j$ is empty in the considered time step. As such, station $s_j$ becomes unusable in the cross-correlation method and should be removed from the sensor set.

Another unfavorable configuration of the graph occurs when an isolated subset of nodes in the graph is identified. In this situation, we can consider each subset of nodes independently. When studying stations with different technical capabilities, this condition can occur, such as NCCFs from land stations and OBS \citep{hable2018}. These specific cases are degenerated cases of the approach we suggest in this paper, and they are excluded from the study.

To simplify our methodological approach even further, here, we only examine graphs where each NCCF between stations $s_j$ and $s_k$ can be computed. Then, the graphs are defined as complete as every edge $e_{jk}$ exists. In the following, the measured phase differences between node $s_j$ and node $s_k$ are denoted as $\delta^{j,k}$, and the phase delay at node $s_j$ is denoted as $\Delta^j$.

Within this framework, the incidence matrix $G$ relies on the phase differences between each pair of stations (observation vector, $\delta$, of shape $[\frac{n \left( n-1 \right)}{2}]$) and the phase delay at each individual station (estimation vector, $\Delta$, of shape $[n]$):

\begin{equation}
G \Delta = \delta
\label{Eq.1}
\end{equation}

In practice, the vectors $\delta$ and $\Delta$ are defined as the following:

\begin{equation}
\delta = \begin{pmatrix}
\delta^{1,2} \\
\delta^{1,3} \\
\delta^{1,4} \\
\cdots \\
\delta^{n-1,n}
\end{pmatrix}
\end{equation}

and

\begin{equation}
\Delta = \begin{pmatrix}
\Delta^{1} \\
\Delta^{2} \\
\Delta^{3} \\
\cdots \\
\Delta^{n}
\end{pmatrix}
\end{equation}

The incidence matrix of the complete graph with $n$ nodes is a sparse matrix of shape $[\frac{n \left( n-1 \right)}{2},n]$ and is written as the following:

\begin{equation}
G = \begin{pmatrix}

1 & -1 & & \multicolumn{2}{c}{\text{\kern0.5em\smash{\raisebox{-1ex}{\Large 0}}}} \\
\vdots & & \multicolumn{2}{c}{\ddots} & \\
1 & \multicolumn{2}{c}{\text{\kern-0.5em\smash{\raisebox{0.75ex}{\Large 0}}}} & & -1 \\

0 & 1 & -1 & \multicolumn{2}{c}{\text{\kern0.5em\smash{\raisebox{-1ex}{\Large 0}}}} \\
\vdots & \vdots & & \ddots &  \\
0 & 1 & \multicolumn{2}{c}{\text{\kern-0.5em\smash{\raisebox{0.75ex}{\Large 0}}}} & -1\\

 & & \ldots & \\

0 & \cdots & 0 & 1 & -1
\end{pmatrix}
\label{Eq.2}
\end{equation}

The rows and columns of the incidence matrix $G$ relate to the edges (node pairs) and nodes, respectively. Each row relates a given edge $\delta^{j,k}$ to its arriving and leaving nodes $\Delta^j$ and $\Delta^k$, such as $\delta^{j,k} = \Delta^j - \Delta^k$.

We demonstrate the relationship $\delta^{k,j} = -\delta^{j,k}$.

The Laplacian matrix $\mathcal{L}$ of this graph with $n$ nodes is a square matrix of shape $[n,n]$ and is defined as $\mathcal{L} = G^{T} G$, with $G^{T}$ as the transpose of the incidence matrix.

\begin{equation}
\mathcal{L} = G^T G = \begin{pmatrix}
 n-1 & \multicolumn{2}{c}{\text{\kern0.5em\smash{\raisebox{-1ex}{\Large -1}}}} \\
 & \ddots &  \\
 \multicolumn{2}{c}{\text{\kern-0.5em\smash{\raisebox{0.75ex}{\Large -1}}}} & n-1
\end{pmatrix}
\label{laplacian}
\end{equation}

In \cite{sensschoenfelder2008}, the author proposed to solve Eq.~\eqref{Eq.1} through computation of a generalized inverse (pseudo-inverse) of the Laplacian matrix  $\mathcal{L}$. The pseudo-inverse of the Laplacian matrix requires a regularization procedure, as the rank of the Laplacian is $n-1$. We propose below to compute an explicit solution for $\Delta^j$ without any regularization requirement.

Multiplying each side of Eq.~\eqref{Eq.1} by $G^T$, we get the following system:

\begin{equation}
\mathcal{L} \Delta = G^T \delta
\label{Eq.4}
\end{equation}

which can be written as the following:

\begin{equation} %  \\
\begin{pmatrix}
(n-1) \Delta^1 - \Delta^2 - \cdots - \Delta^n \\
\vdots \\
- \Delta^1 - \Delta^2 - \cdots + (n-1) \Delta^n
\end{pmatrix} 
=
\begin{pmatrix}
\sum_{k=1, k\neq 1}^{n} \delta^{1,k} \\
\vdots \\
\sum_{k=1, k\neq n}^{n} \delta^{n,k}
\end{pmatrix} 
\label{Eq.4-1}
\end{equation}

This system of $n$ equations with $n$ unknowns (the $n$ phase delays $\Delta^k$ at each station $s_k$) is such that one of the $n$ equations is a linear combination of the $(n-1)$ others. The rank of $\mathcal{L}$ is $(n-1)$, and an infinity of solutions exist, as the absolute network time is not determined. 
Without making an hypothesis, we cannot go further. Then, we define one node $s_{k_0}$ as a master node ($\Delta^{k_0}=0$) and then deduce the other $\Delta^{k|k_0}$ for $k \neq k_0$. The notation $\Delta^{k|k_0}$ corresponds to the phase delay of node $s_k$, under the assumption that node $s_{k_0}$ exhibits no phase delay ($s_{k_0}$ is the master node).

Under the hypothesis that $\Delta^{k_0}=0$, the $k_0$-th equation of this system gives the following relationship:
\begin{equation}
- \sum_{\substack{k=1 \\ k \neq k_0}}^{n} \Delta^{k|k_0} = \sum_{\substack{k=1 \\ k\neq k_0}}^{n} \delta^{k_0,k}
\end{equation}

Then, we replace $\sum_{\substack{k=1 \\ k \neq k_0}}^{n} \Delta^{k|k_0}$ by $-\sum_{\substack{k=1 \\ k\neq k_0}}^{n} \delta^{k_0,k}$ in each of the $(n-1)$ other equations of the system of Eq.~\eqref{Eq.4-1}.

Finally, when considering $s_{k_0}$ as the master station, we get an explicit solution for each station $s_k$ as the following:
\begin{equation}
\Delta^{k|k_{0}} = 
\frac{1}{n} \left( \sum_{\substack{j=1 \\ j \neq k}}^{n} \delta^{k,j} - \sum_{\substack{j=1 \\ j \neq k_0}}^{n} \delta^{k_0,j} \right)
\label{Deltak}
\end{equation}

The Appendix \ref{annex_eigen} gives a demonstration of this expression through the diagonalization of the Laplacian matrix $\mathcal{L}$.

Here, we make an aside to contextualize our approach with respect to pre-existing study. If we consider successively each of the stations (including the station $s_k$ itself) as the master station, we demonstrate $n$ different solutions for $\Delta^{k|k_0}$ for station $s_k$ under the assumption that station $s_{k_0}$ is the reference. 
Also, we include the obvious solution for $k_0=k$: $\Delta^{k|k}=0$. If we average these $n$ different solutions for station $s_k$, no more dependence in $k_0$ is found, and we obtain another solution denoted $<\Delta^k>$:

\begin{align*}
<\Delta^k> &= \frac{1}{n} \sum_{k_0=1}^{n} \Delta^{k|k_0} \\
    &= \frac{1}{n} \sum_{k_0=1}^{n} \left( \frac{1}{n} \left( \sum_{\substack{j=1 \\ j \neq k}}^{n} \delta^{k,j} - \sum_{\substack{j=1 \\ j \neq k_0}}^{n} \delta^{k_0,j} \right) \right) \\
    &= \frac{1}{n^2} \left( n \sum_{\substack{j=1 \\ j \neq k}}^{n} \delta^{k,j} - \sum_{k_0=1}^{n} \sum_{\substack{j=1 \\ j \neq k_0}}^{n} \delta^{k_0,j} \right)
\end{align*}

As $\delta^{k_0,j} = - \delta^{j,k_0}$ (undirected graph), the double sum vanishes (all $n(n-1)$ terms, which are the non-diagonal terms of the antisymmetric matrix of $\delta$, cancel each other out two by two), and the expression leads to the following:

\begin{equation}
<\Delta^k> = \frac{1}{n} \left( \sum_{\substack{j=1 \\ j \neq k}}^{n} \delta^{k,j} \right)
\end{equation}
which is similar to the analytical solution proposed by \cite{hable2018}.

Note that the expression $<\Delta^k>$ is only an approximation of the exact solution $\Delta^{k|k_0}$ that requires, conversely, the choice of a master station. The solution $<\Delta^k>$ is equivalent to the addition of a closure relationship 
into Eq.~\eqref{Eq.1}, which requires that the sum of $<\Delta^k>$ is zero whatever the time step. In this case, the analytical solution for $<\Delta^k>$ means that the Laplacian matrix $\mathcal{L} = n I$, with $I$ as the Identity matrix, is valid in the first approximation for a very large number of stations $n$.

In this paper, we do not consider the solution $<\Delta^k>$ (by adding a closure relationship), and we solely focus on the exact solution $\Delta^{k|k_0}$, by defining a master station $k_0$ in a synthetic use case and in an experimental dataset.
