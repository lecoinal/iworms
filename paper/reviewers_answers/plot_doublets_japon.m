clear

jid='76653589';
sp='KWNH_SHZ_MUIH_SHZ';


load([jid,'/corr_',sp,'.h5']);
%tlag=-60:1/5:60;
tlag=clt;

time=2008:1/365.25:2013;

%cc=NaN*ones(1001,365);idx=0;
%for ii=1:365
%  idx+=iarrvalid(ii);
%  if iarrvalid(ii)==1
%    cc(:,ii)=dcorr(:,idx);
%  endif
%end

figure(1);clf;grid on;hold on;
pcolor(tlag,time,scorr');shading flat;colorbar;
ylabel('Date')
xlabel('correlation time lag (sec)')
title('correlations KWNH SHZ MUIH SHZ')
print('-dpng',[jid,'/f1.png']);


load([jid,'/dtot_',sp,'.h5'])
tmp=dtot(:,1);   % dtot stddt=dtoterr   corrval
tmp2=dtot(:,2);   % stddt=dtoterr   corrval
tmp3=dtot(:,3);   % corrval
clear dtot
dtot=NaN*ones(length(time),length(time));idx=0;
dtoterr=NaN*ones(length(time),length(time));
corrval=NaN*ones(length(time),length(time));
for ii=1:length(time)
  for jj=ii+1:length(time)
    idx+=1;
    dtot(ii,jj)=tmp(idx);
    dtoterr(ii,jj)=tmp2(idx);
    corrval(ii,jj)=tmp3(idx);
  end
end  
clear tmp tmp2 tmp3

dtot(dtot==0)=NaN;
figure(2);clf;grid on;hold on;
pcolor(time,time,100*dtot);shading flat;colorbar;
xlabel('Date');ylabel('Date')
title('dt/t (in %) KWNH SHZ MUIH SHZ')
print('-dpng',[jid,'/f2a.png']);

dtoterr(dtoterr==0)=NaN;
figure(3);clf;grid on;hold on;
pcolor(time,time,dtoterr);shading flat;colorbar;
xlabel('Date');ylabel('Date')
title('dt/t error')
print('-dpng',[jid,'/f2b.png']);

corrval(corrval==0)=NaN;
figure(4);clf;grid on;hold on;
pcolor(time,time,corrval);shading flat;colorbar;
xlabel('Date');ylabel('Date')
title('dt/t corrval KWNH SHZ MUIH SHZ')
print('-dpng',[jid,'/f2c.png']);

load([jid,'/dvov_',sp,'.h5'])
figure(5);clf;grid on;hold on;
plot(time,-100*(dtot-mean(dtot)),'x-b');
%plot(time,-100*(dtot),'.-b');
xlabel('Date')
ylabel('dv/v (in %) (after removing the 5-yr-avg value)')
title('KWNH SHZ MUIH SHZ')
load([jid,'/iwormsdvov_',sp,'.h5'])
plot(time,-100*(dvov-mean(dvov)),'+--g');
%plot(time,-100*(dvov),'.-g');
plot((2011+70/365.25)*[1 1],0.15*[-1 1],'--r');
legend('Bayesian least squares inversion','Graph','Tohoku-Oki EQ')
ylim(0.15*[-1 1])
print('-dpng',[jid,'/f3.png'],'-S1744,720');
xlim([2011 2012]);ylim(0.15*[-1 1])
print('-dpng',[jid,'/f3_zoom2011.png'],'-S1744,720');


%figure(1);hold on;
%plot(0+1000*100*(dtot-dtot(1)),1:365,'o-r');
%plot(0+1000*100*dvov,1:365,'+-b');
%xlim([-100 100]);
%print('-dpng','f1.png');

