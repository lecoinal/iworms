clear


% 5-day

%jid='76765665';pair='CI.BRE.__.Z.trace.int8_CI.DLA.__.Z.trace.int8';sp='CI.BRE.Z CI.DLA.Z';yl=0.2;
%jid='76738938';pair='CI.BRE.__.Z.trace.int8_CI.LLS.__.Z.trace.int8';sp='CI.BRE.Z CI.LLS.Z';yl=0.2;
%jid='76765698';pair='CI.LAF.__.Z.trace.int8_CI.LGB.__.Z.trace.int8';sp='CI.LAF.Z CI.LGB.Z';yl=0.3;
%time=2000:5/365.25:2021-5/365.25;


% 20-day

%jid='76783915';pair='CI.LAF.__.Z.trace.int8_CI.LGB.__.Z.trace.int8';sp='CI.LAF.Z CI.LGB.Z';yl=0.3;
jid='76783916';pair='CI.BRE.__.Z.trace.int8_CI.LLS.__.Z.trace.int8';sp='CI.BRE.Z CI.LLS.Z';yl=0.2;
%jid='76783917';pair='CI.BRE.__.Z.trace.int8_CI.DLA.__.Z.trace.int8';sp='CI.BRE.Z CI.DLA.Z';yl=0.2;
time=2000:20/365.25:2021-20/365.25;


load([jid,'/corr_',pair,'.h5']);
tlag=clt; % -100:1/5:100;


lt=length(time);

cc=NaN*ones(length(clt),lt);idx=0;
for ii=1:lt
  idx+=iarrvalid(ii);
  if iarrvalid(ii)==1
    cc(:,ii)=scorr(:,idx);
  endif
end

figure(1);clf;grid on;hold on;
pcolor(tlag,time,cc');shading flat;colorbar;
ylabel('Year')
xlabel('correlation time lag (sec)')
set(gca,'XMinorTick','on','YMinorTick','on')
title(['correlations ',sp])
ylim([2000 2021]);
print('-dpng',[jid,'/f1.png']);


load([jid,'/dtot_',pair,'.h5'])
tmp=dtot(:,1);   % dtot stddt=dtoterr   corrval
tmp2=dtot(:,2);   % stddt=dtoterr   corrval
tmp3=dtot(:,3);   % corrval
clear dtot
dtot=NaN*ones(lt,lt);idx=0;
dtoterr=NaN*ones(lt,lt);
corrval=NaN*ones(lt,lt);
for ii=1:lt
  for jj=ii+1:lt
    idx+=1;
    dtot(ii,jj)=tmp(idx);
    dtoterr(ii,jj)=tmp2(idx);
    corrval(ii,jj)=tmp3(idx);
  end
end  
clear tmp tmp2 tmp3

dtot(dtot==0)=NaN;
figure(2);clf;grid on;hold on;
pcolor(time,time,100*dtot);shading flat;colorbar;
xlabel('Year');ylabel('Year')
title(['dt/t (in %) ',sp])
xlim([2000 2021]);
ylim([2000 2021]);
set(gca,'XMinorTick','on','YMinorTick','on')
print('-dpng',[jid,'/f2a.png']);

dtoterr(dtoterr==0)=NaN;
figure(3);clf;grid on;hold on;
pcolor(time,time,dtoterr);shading flat;colorbar;
xlabel('Year');ylabel('Year')
title('dt/t error')
xlim([2000 2021]);
ylim([2000 2021]);
set(gca,'XMinorTick','on','YMinorTick','on')
print('-dpng',[jid,'/f2b.png']);

corrval(corrval==0)=NaN;
figure(4);clf;grid on;hold on;
pcolor(time,time,corrval);shading flat;colorbar;
xlabel('Year');ylabel('Year')
xlim([2000 2021]);
ylim([2000 2021]);
set(gca,'XMinorTick','on','YMinorTick','on')
title(['dt/t corrval ',sp])
print('-dpng',[jid,'/f2c.png']);

load([jid,'/dvov_',pair,'.h5'])
figure(5);clf;grid on;hold on;
%plot(1:365,-100*(dtot-(dtot(1))),'x-b');
plot(time,-100*(dtot),'.-b');
xlabel('Year')
ylabel('dv/v (in %)');% (after removing the 1-yr-time-averaged value)')
title(sp)
load([jid,'/iwormsdvov_',pair,'.h5'])
plot(time,-100*(dvov),'.-g');
%plot(1:365,-100*(dvov-(dvov(1))),'+--g');
legend('Bayesian least squares inversion','Graph')
xlim([2000 2021]);
ylim([-.2 .2]);
set(gca,'XMinorTick','on','YMinorTick','on')
print('-dpng',[jid,'/f3.png'],'-S1744,720');
%xlim([0 100]);
%print('-dpng',[jid,'/f3_zoom.png']);

wlen=5;
load([jid,'/dvov_',pair,'.h5'])
figure(5);grid on;hold on;
y = movmean (dtot, wlen);
plot(time,-100*(y),'.-b','linewidth',3);
xlabel('Year')
ylabel('dv/v (in %)');% (after removing the 1-yr-time-averaged value)')
title(sp)
load([jid,'/iwormsdvov_',pair,'.h5'])
y = movmean (dvov, wlen);
plot(time,-100*(y),'.-g','linewidth',3);
legend('Bayesian least squares inversion','Graph')
xlim([2000 2021]);
ylim([-yl yl]);
set(gca,'XMinorTick','on','YMinorTick','on')
set(gca,'YDir','reverse');
print('-dpng',[jid,'/f3movmean',int2str(wlen),'.png'],'-S1744,720');



