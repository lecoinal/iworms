% montage -tile 1x2 -geometry 872x654 FigS2a.png FigS2b.png FigS2.png


% octave test
clear all
nstep=2000; % 240; % 2000;%%Nb pas de temps

nsta=100; % 78; % 100;%%Nb stations
sigma = 0.1:0.1:5;%%%ecart type gaussienne

fc = 0.15;   % central frequency
T = 1.0 / fc;
Tover2 = 0.5 * T;

phase_shifts = [1/12, 1/6, 1/3]; %, 0.95 * 1/2, 2/3];   % in number of period, the last one exceeds T/2
%phase_shifts = [1/12, 1/6, 1/3, 1/2, 2/3];   % in number of period, the last one exceeds T/2
phase_shifts_in_sec = phase_shifts * T;
%phase_shifts_in_sec = [0.5, 1, 1.5, 2.0, 4.0];  % in sec here...

results=zeros(length(sigma),length(phase_shifts_in_sec));
resultsw=zeros(length(sigma),length(phase_shifts_in_sec));
stdresults=zeros(length(sigma),length(phase_shifts_in_sec));
stdresultsw=zeros(length(sigma),length(phase_shifts_in_sec));

sigma_over_T = sigma * fc

for ps=1:length(phase_shifts_in_sec)

  toto=zeros(nstep,length(sigma));
  totow=zeros(nstep,length(sigma));

  for jj=1:length(sigma)

    xx0 = 0.0 + sigma(jj) * randn(nsta,nstep); % gaussian distrib around 0
    xx1 = phase_shifts_in_sec(ps) + sigma(jj) * randn(nsta,nstep); %  # gaussian distrib around 1

    % wrap xx0 and xx1 on a periodic [-T/2, T/2] interval
    % T/2 equals pi
    phases = xx0 * pi / Tover2;
    phases = mod((phases + pi),(2 * pi)) - pi;
    xx0w = phases * Tover2 / pi;
    phases = xx1 * pi / Tover2;
    phases = mod((phases + pi),(2 * pi)) - pi;
    xx1w = phases * Tover2 / pi;

%    % probleme ici car T/2 wraps sur 0 ?
%    if ps == 4;
%      if jj == 1;
%        mean(xx1(:)),mean(xx1w(:))
%      end
%    end
    

%    figure(10);clf
%    subplot(221);
%    hist(xx0,20);
%    subplot(222);
%    hist(xx1,20);
%    subplot(223);
%    hist(mx0,20);
%    subplot(224);
%    hist(mx1,20);

    toto(:,jj)=mean(xx1-xx0,1);   % averaged over the station
    totow(:,jj)=mean(xx1w-xx0w,1);   % averaged over the station : same as Delta at given step and SNR

%    figure(1);clf;hold on;
%    plot(toto(:,jj),'ob')
%    plot(totow(:,jj),'*r')
%    xlabel('nstep')
%    ylabel('Delta')
%    title([num2str(sigma(jj))])

%    figure(2);clf
%    subplot(211)
%    hist(toto(:,jj),20);
%    xlim([-5 5])
%    subplot(212)
%    hist(totow(:,jj),20);
%    xlabel('Delta')
%    xlim([-5 5])
%    title([num2str(sigma(jj))])

%    pause(.1)
    clear xx0 xx1 xx0w xx1w
  end

  results(:,ps) = mean(toto,1);  % averaged over the steps
  resultsw(:,ps) = mean(totow,1);  % averaged over the steps : same as the circles in Fig 3 at a given SNR
  stdresults(:,ps) = std(toto,1);  % over the steps
  stdresultsw(:,ps) = std(totow,1);  % over the steps : same as the crosses in Fig 3 at a given SNR

end

figure(3);clf;hold on;grid on;
plot(sigma_over_T,fc*results(:,1),'ok')
plot(sigma_over_T,fc*results(:,2),'*k')
plot(sigma_over_T,fc*results(:,3),'vk')
%plot(sigma_over_T,fc*results(:,4),'^k')
%plot(sigma_over_T,fc*results(:,5),'+k')
plot(sigma_over_T,fc*resultsw(:,1),'og')
plot(sigma_over_T,fc*resultsw(:,2),'*g')
plot(sigma_over_T,fc*resultsw(:,3),'vg')
%plot(sigma_over_T,fc*resultsw(:,4),'^g')
%plot(sigma_over_T,fc*resultsw(:,5),'+g')
set (gca (), "xdir", "reverse")
xlabel('\sigma / T_0');
ylabel('Averaged values of phase delays over 2000 timesteps')
%legend('T/12','T/6','T/3','0.95% T/2','2T/3','T/12 wrap','T/6 wrap','T/3 wrap','0.95% T/2 wrap','2T/3 wrap')
%legend('T/12','T/6','T/3','T/12 wrap','T/6 wrap','T/3 wrap')
set (gca (), 'ytick', [0 fc*phase_shifts_in_sec(1) fc*phase_shifts_in_sec(2) fc*phase_shifts_in_sec(3)])
set (gca (), 'yticklabel', {'0','T_0/12','T_0/6','T_0/3'})
print('-dpng','FigS2a.png')

figure(4);clf;hold on;grid on;
plot(sigma_over_T,fc*stdresultsw(:,1),'og')
plot(sigma_over_T,fc*stdresultsw(:,2),'*g')
plot(sigma_over_T,fc*stdresultsw(:,3),'vg')
%plot(sigma_over_T,fc*stdresultsw(:,4),'^g')
%plot(sigma_over_T,fc*stdresultsw(:,5),'+g')
plot(sigma_over_T,fc*stdresults(:,1),'ok')
plot(sigma_over_T,fc*stdresults(:,2),'*k')
plot(sigma_over_T,fc*stdresults(:,3),'vk')
%plot(sigma_over_T,fc*stdresults(:,4),'^k')
%plot(sigma_over_T,fc*stdresults(:,5),'+k')
set (gca (), "xdir", "reverse")
xlabel('\sigma / T_0');
ylabel({'Standard deviation of phase delays';'(relatively to the period T_0)'})
%legend('T/12','T/6','T/3','0.95% T/2','2T/3','T/12 wrap','T/6 wrap','T/3 wrap','0.95% T/2 wrap','2T/3 wrap')
h=legend('\mu=T_0/12, modulo-wrapped inside [-T_0/2,T_0/2]',...
         '\mu=T_0/6, modulo-wrapped inside [-T_0/2,T_0/2]',...
         '\mu=T_0/3, modulo-wrapped inside [-T_0/2,T_0/2]',...
         '\mu=T_0/12','\mu=T_0/6','\mu=T_0/3');
set(h,"fontsize",10);
legend boxoff
legend left
print('-dpng','FigS2b.png')

%figure(4);clf;hold on;
%plot(sigma_over_Tc,results(:,1),'ob')
%plot(sigma_over_Tc,results(:,2),'*b')
%plot(sigma_over_Tc,results(:,3),'vb')
%plot(sigma_over_Tc,results(:,4),'^b')
%plot(sigma_over_Tc,results(:,5),'+b')
%plot(sigma_over_Tc,resultsw(:,1),'or')
%plot(sigma_over_Tc,resultsw(:,2),'*r')
%plot(sigma_over_Tc,resultsw(:,3),'vr')
%plot(sigma_over_Tc,resultsw(:,4),'^r')
%plot(sigma_over_Tc,resultsw(:,5),'+r')
%legend('0.5sec','1sec','1.5sec','2sec','4sec','0.5sec wrap','1sec wrap','1.5sec wrap','2sec wrap','4sec wrap')
%xlabel('sigma_over_Tc');
%ylabel('Delta')
