%rsync -av luke.ciment:/bettik/lecoinal/oar.SHUJUAN.76564252/corr*.h5 .
%receiving incremental file list
%corr_CI.CIA.__.Z_CI.CHN.__.Z.h5
%
%sent 43 bytes  received 2,329,234 bytes  517,617.11 bytes/sec
%total size is 2,328,536  speedup is 1.00
%$ rsync -av luke.ciment:/bettik/lecoinal/oar.SHUJUAN.76564252/*dtot*.h5 .
%receiving incremental file list
%dtot_CI.CIA.__.Z_CI.CHN.__.Z.h5
%
%sent 43 bytes  received 799,530 bytes  177,682.89 bytes/sec
%total size is 799,208  speedup is 1.00
%$ rsync -av luke.ciment:/bettik/lecoinal/oar.SHUJUAN.76564252/*dvov*.h5 .
%receiving incremental file list
%NWiniwormsdvov_CI.CIA.__.Z_CI.CHN.__.Z.h5
%dvov_CI.CIA.__.Z_CI.CHN.__.Z.h5
%iwormsdvov_CI.CIA.__.Z_CI.CHN.__.Z.h5
%
%sent 81 bytes  received 111,729 bytes  31,945.71 bytes/sec
%total size is 111,397  speedup is 1.00
%$ h5ls NWiniwormsdvov_CI.CIA.__.Z_CI.CHN.__.Z.h5
%NWindvov                 Dataset {40, 365}
%NWinwdvov                Dataset {40, 365}



clear

%CI.CIA 33.401898,-118.415021,477m
%CI.CHN 33.99879,-117.68044,208m
%               CI.CIA              CI.CHN           95093.533

%jid='76564252';
%jid='76633509';
%jid='76705571';pair='CI.BRE.__.Z.trace.int8_CI.DLA.__.Z.trace.int8';sp='CI.BRE.Z CI.DLA.Z';
jid='76706239';pair='CI.BRE.__.Z.trace.int8_CI.LLS.__.Z.trace.int8';sp='CI.BRE.Z CI.LLS.Z';
%jid='76706240';pair='CI.LAF.__.Z.trace.int8_CI.LGB.__.Z.trace.int8';sp='CI.LAF.Z CI.LGB.Z';

load([jid,'/corr_',pair,'.h5']);
tlag=clt; % -100:1/5:100;

cc=NaN*ones(length(clt),365);idx=0;
for ii=1:365
  idx+=iarrvalid(ii);
  if iarrvalid(ii)==1
    cc(:,ii)=scorr(:,idx);
  endif
end

figure(1);clf;grid on;hold on;
pcolor(tlag,1:365,cc');shading flat;colorbar;
ylabel('Day of year 2010')
xlabel('correlation time lag (sec)')
title(['correlations ',sp])
print('-dpng',[jid,'/f1.png']);


load([jid,'/dtot_',pair,'.h5'])
tmp=dtot(:,1);   % dtot stddt=dtoterr   corrval
tmp2=dtot(:,2);   % stddt=dtoterr   corrval
tmp3=dtot(:,3);   % corrval
clear dtot
dtot=NaN*ones(365,365);idx=0;
dtoterr=NaN*ones(365,365);
corrval=NaN*ones(365,365);
for ii=1:365
  for jj=ii+1:365
    idx+=1;
    dtot(ii,jj)=tmp(idx);
    dtoterr(ii,jj)=tmp2(idx);
    corrval(ii,jj)=tmp3(idx);
  end
end  
clear tmp tmp2 tmp3

dtot(dtot==0)=NaN;
figure(2);clf;grid on;hold on;
pcolor(1:365,1:365,100*dtot);shading flat;colorbar;
xlabel('Day of year 2010');ylabel('Day of year 2010')
title(['dt/t (in %) ',sp])
print('-dpng',[jid,'/f2a.png']);

dtoterr(dtoterr==0)=NaN;
figure(3);clf;grid on;hold on;
pcolor(1:365,1:365,dtoterr);shading flat;colorbar;
xlabel('Day of year 2010');ylabel('Day of year 2010')
title('dt/t error')
print('-dpng',[jid,'/f2b.png']);

corrval(corrval==0)=NaN;
figure(4);clf;grid on;hold on;
pcolor(1:365,1:365,corrval);shading flat;colorbar;
xlabel('Day of year 2010');ylabel('Day of year 2010')
title(['dt/t corrval ',sp])
print('-dpng',[jid,'/f2c.png']);

load([jid,'/dvov_',pair,'.h5'])
figure(5);clf;grid on;hold on;
%plot(1:365,-100*(dtot-(dtot(1))),'x-b');
plot(1:365,-100*(dtot),'.-b');
xlabel('Day of year 2010')
ylabel('dv/v (in %)');% (after removing the 1-yr-time-averaged value)')
title(sp)
load([jid,'/iwormsdvov_',pair,'.h5'])
plot(1:365,-100*(dvov),'.-g');
%plot(1:365,-100*(dvov-(dvov(1))),'+--g');
legend('Bayesian least squares inversion','Graph')
print('-dpng',[jid,'/f3.png']);
xlim([0 100]);
print('-dpng',[jid,'/f3_zoom.png']);
