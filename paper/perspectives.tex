\section{Conclusion and perspectives}

In this paper, we experiment with a direct method to compute station phase delays starting from the measurements of inter-station phase differences deduced from the phase variations of NCCFs.

We evaluate the potential of this direct method on a dataset of synthetic NCCFs, with simulation of a network of realistic geometry and density, for which we impose a phase shift of a given station with respect to the others. Next, we measure the robustness and accuracy of the phase delay estimation, and we evaluate the precision of this method to some parameters, such as the inter-station distances and the $SNR$.

Then, we demonstrate and explain a phenomenon of phase delay underestimation when the $SNR$ decreases. We introduce a coherence-weighted phase delay estimate that seems less vulnerable to this bias for intermediate quality signals.

Also, we apply this methodology for phase-delay estimation on an experimental dense seismic array of one thousand vertical component geophones deployed in the San Jacinto fault zone region for a 1-month period. A large proportion of phase-delay distribution is associated with high coherence levels, which shows reliable computation, and is less than $5\,\mathrm{ms}$ ($2\, \%$ of the signal period), which shows that the San Jacinto array is well synchronized.

More surprisingly, the standard deviation phase-delay and average coherence maps show spatially smooth imaging with a possible topographic influence, although no spatial smoothing is performed in our processing; the phase delay of each station is indeed evaluated independently from each of the others.

Our method captures wave propagation effects associated with local topography, suggesting that the topographic effect may be accounted for \citep{pilz2013,li2019} to fine-tune the measurements of inter-station phase differences and to compute more accurate phase delays at each station. This study does not result in any inversion methodology, but our results present clear similarities with structure-oriented monitoring studies.

We provided indications of the large scale and real time application of this direct method on large continuous datasets. It has the advantage of low memory consumption, as no inversion occurs. Computing phase delay from the inter-station phase differences is a linear process, and it can possibly be weighted by the coherence. Its memory and CPU costs do not evolve with the square of the number of stations involved. Computing the phase delay at each station is an easily parallelizable process, as it is computed independently of the other stations. Only the master station communicates its state to the other stations.

In the future, this methodology opens perspectives in the monitoring of seismic velocity variations \citep{brenguier2008,brenguier2014,gomez2018,taylor2020}. Indeed, applying the same mathematical formalism for each pair of stations, we would solve a system described by a complete graph where each node is a date and each edge is the measurement of the travel-time fluctuations between two dates. The expected advantages of the present direct method would be a reduction in the computation time, improved accuracy in seismic velocity variation estimations, and the use of smaller memory loads.

