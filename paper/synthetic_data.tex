\section{Numerical experiment with synthetic data} \label{syntheticpart}

We introduce synthetic data to describe in detail our methodological approach from 
the measurement of the phase differences between all station pairs to the evaluation of the explicit solution given by Eq.~\eqref{Deltak} of the phase delays estimated at each station. 
Using a synthetic dataset allows us to study the influence of specific parameters, like the inter-station distance and the 
signal-to-noise ratio (defined below) 
of the NCCFs, while excluding technical issues inherent to real data (e.g., sampling errors, gaps, spikes). Here, we do not study the influence of the seismic array geometry on the computation of phase delays: that is to say that we studied only one configuration of the array. However, changing the number of stations and/or their location could be possible in a further parametric study.

Figure \ref{map_synthetic} shows a set of $n=79$ synthetic stations located on the French southeastern quarter of the AlpArray network where the inter-station distances are less than $409\,\mathrm{km}$, and it covers a $160,000\,\mathrm{km^2}$ area that is delimited by the coordinates $ 43.07-46.00^{\circ}\,\mathrm{E}, 3.09-7.56^{\circ}\,\mathrm{N}$.

\begin{figure}
\centering
    \includegraphics[width=90mm]{figures/Fig1.png}
    \caption{Map of the locations and inter-station distance 
distribution of the $79$ synthetic stations. We use the French southeastern quarter of the 
AlpArray network: the networks 
(and associated dataset identifiers) are FR (doi:10.15778/RESIF.FR), 
G (doi:10.18715/GEOSCOPE.G), Z3 (doi:10.12686/alparray/z3\_2015), and ZH (doi:10.15778/RESIF.ZH2016).}
    \label{map_synthetic}
\end{figure}

We simulate the NCCFs between the resulting $3,081$ pairs of stations, along a duration of $480$ successive time steps, with an arbitrary time-step unit. Each NCCF is built from a reference signal, defined as a normalized synthetic Gaussian pulse, with a $F_0=0.15\,\mathrm{Hz}$ central frequency. 
Then, we apply a temporal shift to center the Gaussian pulse at a lag time corresponding to the current inter-station distance, assuming a $3\,\mathrm{km} \cdot \mathrm{s^{-1}}$ surface wave velocity. Note that \cite{stehly2007,sensschoenfelder2008,hable2018,weemstra2021} also focus on direct wave when detecting time shifts. 
Further, we sum up this delayed pulse with its time-reversal to get two symmetric pulses (i.e., the causal and acausal parts). We optionally apply a range-dependent attenuation of the simulated Green's function for large inter-station distances. In this case, for stations more than $d_0=20\,\mathrm{km}$ away, the amplitude of the Gaussian pulse can be normalized by $\sqrt{\frac{distance}{d_0}}$ \citep{larose2007}. 
To study the influence of the signal-to-noise ratio ($SNR$) of the NCCF, we create white noise as a uniform distribution filtered between $0.1$ and $0.2\,\mathrm{Hz}$ (i.e., the noise spectrum is centered at the $F_0=0.15\,\mathrm{Hz}$ signal) and normalized by its maximum absolute value. To adjust the noise amplitude, we introduce the $SNR$ value as a weighting parameter. We explore $SNR$ values between $100$ (high-quality signal) and $0.01$ (very noisy signal). The $SNR$-weighted noise is finally added to the synthetic NCCF.

Additionally, we repeat this operation for the $3,081$ pairs and $480$ time steps, to obtain a family of synthetic NCCFs $CC_{i}^{p}(t)$, with $i$ as the current time step, $p$ as the station pair index, and $t$ as the correlation lapse time. 
Next, the blue curves in Fig.~\ref{synt_dt} show examples of simulated reference signals (NCCFs) for two pairs of stations (a,b) $136\,\mathrm{km}$ and (c,d) $40\,\mathrm{km}$ apart. The gray curves in the top plots of Fig.~\ref{synt_dt} show white noise for (a,c) $SNR=0.8$ and (b,d) $SNR=5.0$.

\begin{figure*}
\centering
    \includegraphics[width=\textwidth]{figures/Fig2.png}
    \caption{Synthetic NCCF ($CC$) for a $136\,\mathrm{km}$ (a,b) and a $40\,\mathrm{km}$ (c,d) inter-station distance, and assuming a $3\,\mathrm{km}\cdot\mathrm{s^{-1}}$ surface-wave velocity. Blue curve, reference Gaussian pulse during the synchronized $T_1$ period; gray curve, added white noise (uniform law), normalized by signal-to-noise ratios of (a,c) $SNR=0.8$ and (b,d) $SNR=5.0$. Bottom plots: The $40\,\mathrm{s}$ lapsed time windows. Blue curve, Gaussian reference signal; black curve, Gaussian pulse with added noise at an arbitrary time step from the synchronized $T_1$ period; red curve, 1-s (i.e. $15\, \%$ of $T_0$) shifted Gaussian pulse with added noise at an arbitrary time step from the de-synchronized $T_2$ period.}
    \label{synt_dt}
\end{figure*}

We simulate a phase jump that occurs on station 1 by applying a temporal shift to the correlations of the $78$ pairs associated to this station. For this purpose, we split the $480$ time-step period into two equal periods of $Q=240$ time steps, named $T_1$ and $T_2$, and we apply the temporal shift only to the $T_2$ period. Thus, we have (1) a first $240$ time-step reference period, named $T_1$, without any time shift and (2) a second $240$ time-step period, named $T_2$, where station 1 is uniformly offset by $1\,\mathrm{s}$ with respect to the other $78$ stations. 
This 1-s time shift corresponds to a phase delay of $15\, \%$ of the signal period $T_0$. The black curves in the bottom plots in Fig.~\ref{synt_dt} show an example of the NCCF with added noise for a pair linked to station 1 at one arbitrary time step from the synchronized $T_1$ period. The red curves in Fig.~\ref{synt_dt} show an example of the de-synchronized signal with added noise at a time step from the $T_2$ period.

We measure the inter-station phase difference ($\delta^p(i)$) at each time step $i$ and for each station pair $p$ by selecting a $40\,\mathrm{s}$ window on every simulated NCCFs. The center of the window is fixed with reference to the inter-station distance and assuming a $3\,\mathrm{km} \cdot \mathrm{s^{-1}}$ velocity, so that the Gaussian pulse is centered on the window. When the extension of the window covers the positive and negative parts of the correlation (i.e., includes the zero-lag), we keep a fixed-length $40\,\mathrm{s}$ window by cutting out the negative part and right-shifting the $40\,\mathrm{s}$ window with the moving lower bound to zero-lag. 
This results in some non-centered pulses along the window for close stations, at less than $60\,\mathrm{km}$ apart (see Fig.~\ref{synt_dt}c,d). The dashed red lines in Fig.~\ref{synt_dt} delimit the $40\,\mathrm{s}$ lapsed time windows. For each station pair $p$, we compute a reference NCCF $CC_{ref}^{p}(t)$ as the average of the $240$ correlations over the whole $T_1$ period (Eq.~\eqref{def_refGF}).

\begin{equation}
\forall p , CC_{ref}^{p}(t) = \frac{\sum_{i \in T1}CC_{i}^{p}(t)}{Q} \: \mathrm{with} \: Q=240
\label{def_refGF}
\end{equation}

In this ideal case, we choose the reference period when the network is well synchronized. As a consequence, the $CC_{ref}^{p}(t)$ is close to the initial reference Gaussian pulse signal (as we average the uniform noise along $T_1$, the noise level diminishes in $CC_{ref}^{p}$). Next, we define a phase vector $\phi^p(t)$ along the time vector associated with the current station pair $p$:

\begin{equation}
\phi^p(t) = \exp^{2 j \pi F_0  t}
\label{def_phase}
\end{equation}

We measure the inter-station phase difference $\delta^p(i)$ for each pair of stations $p$ and at each time step $i$ as the following:

\begin{equation}
\forall (i,p) , \delta^{p}(i) = \frac{1}{2 \pi F_0} arg \left( \frac{CC_{i}^{p}(t) \cdot \phi^p(t)}{CC_{ref}^{p}(t) \cdot \phi^p(t)} \right)
\label{def_deltat}
\end{equation}

with $\cdot$ as the dot vector product along the time $t$ dimension. We estimate $\delta^p$ from the Discrete Fourier Transform (DFT) at the central frequency $F_0=\frac{1}{T_0}$ (Eq.~\eqref{def_deltat}) and not from the time-domain correlation between $CC_{i}^{p}$ and $CC_{ref}^{p}$, for several reasons: (1) limit the computational cost, (2)~avoid discrete $\delta$ values that depend on the sampling rate of the data, and (3) control the distribution support of the $\delta$ measurements, which are bounded in the interval $[-\frac{T_0}{2},\frac{T_0}{2}]$.

Also, we associate a coherence value to each measured $\delta^p(i)$. For station pair $p$ at time step $i$, the coherence $coh^{p}(i)$ is the cross-correlation amplitude at time lapse $-\delta^{p}(i)$, between the normalized $CC_{i}^{p}$ and the normalized reference $CC_{ref}^{p}$. We repeat these operations for all $3,081$ synthetic NCCFs and at all $480$ time steps of the $T_1$ and $T_2$ periods. Then, we finally get $[480,3081]$ $\delta$ and $coh$ values for each pair and at each time step.

Figure \ref{synt_dt} shows examples of inter-station phase differences ($\delta$) for an arbitrary time step from the de-synchronized $T_2$ period and for two arbitrary station pairs related to station 1, spaced $136\,\mathrm{km}$ (Fig.~\ref{synt_dt}a,b) and $40\,\mathrm{km}$ (Fig.~\ref{synt_dt}c,d) apart. We illustrate both cases simulating a noisy signal NCCF ($SNR=0.8$ in Fig.~\ref{synt_dt}a,c) and a high-quality signal NCCF ($SNR=5$ in Fig.~\ref{synt_dt}b,d). The red text in the bottom zoomed plots in Fig.~\ref{synt_dt} indicates the measured $\delta$ (in seconds) and the associated $coh$. Measuring $\delta$ for a $136\,\mathrm{km}$ inter-station distance and with a low $SNR=0.8$ (Fig.~\ref{synt_dt}a) results in an inaccurate $\delta$ observation ($1.49\,\mathrm{s}$ while expecting $1\,\mathrm{s}$, i.e. $22.4\, \%$ of $T_0$ while expecting $15\, \%$ of $T_0$), associated with a $0.62$ coherence level. The noise addition destroys the Gaussian pulse and pollutes the offset measurement. With a higher $SNR=5.0$ (Fig.~\ref{synt_dt}b), the coherence level reaches $0.99$, and the $\delta$ measurement is $0.97\,\mathrm{s}$ ($14.6\, \%$ of $T_0$), which is much closer to the $1\,\mathrm{s}$ expected. 
The situation is similar for the $40\,\mathrm{km}$ inter-station distance (Fig.~\ref{synt_dt}c,d), as we measure $\delta = 0.51\,\mathrm{s}$ ($7.7\, \%$ of $T_0$) with a $0.39$ coherence level when simulating a noisy NCCF ($SNR=0.8$ in Fig.~\ref{synt_dt}c), while we measure $\delta = 1.03\,\mathrm{s}$ ($15.5\, \%$ of $T_0$) with a $0.99$ coherence level with a high quality NCCF ($SNR=5$ in Fig.~\ref{synt_dt}d).

Now, we assume that station 2 is the master station ($k_0=2$), and we apply Eq.~\eqref{Deltak} on the $\delta$ observations to compute the phase delay $\Delta^{1|2}(i)$ of station 1 at each time step $i$ and for each $SNR$ value from the experiment. Also, we define an average coherence $Coh^{1}(i)$ as the averaged value of the $(n-1)$ coherence values associated with each $\delta$ observations related to this station 1:

\begin{equation}
\forall (i) , Coh^{1}(i) = \frac{1}{n-1} \sum_{\substack{j=1 \\ j\neq 1}}^{n} coh^{p=(1,j)}(i)
\label{def_Coh_0}
\end{equation}

Note that this averaged coherence value associated to station 1 does not depend on the choice of the master station.

In the following, we propose to replace the notation $p=(k,j)$ that defines the pair of stations $k$ and $j$ by $(k,j)$ in the exponent notation of $\delta$ and $coh$. Also, we propose to compute a coherence-weighted phase delay $\widetilde{\Delta}^{1|2}(i)$ for station 1 at each time step $i$ and for each $SNR$ value, still considering station 2 as the master station: 

\iftwocol{
\begin{multline}
\forall (i) , \widetilde{\Delta}^{1|2}(i) = \frac{1}{n-1} \\
  \left( \frac{\sum_{\substack{j=1 \\ j\neq 1}}^{n} \left( \delta ^{(1,j)}(i) * coh^{(1,j)}(i) \right)}{Coh^{1}(i)}
- \frac{\sum_{\substack{j=1 \\ j\neq 2}}^{n} \left( \delta ^{(2,j)}(i) * coh^{(2,j)}(i) \right)}{Coh^{2}(i)} \right)
\label{def_Deltaw}
\end{multline}
}
{
\begin{equation}
\forall (i) , \widetilde{\Delta}^{1|2}(i) = \frac{1}{n-1}
  \left( \frac{\sum_{\substack{j=1 \\ j\neq 1}}^{n} \left( \delta ^{(1,j)}(i) * coh^{(1,j)}(i) \right)}{Coh^{1}(i)}
- \frac{\sum_{\substack{j=1 \\ j\neq 2}}^{n} \left( \delta ^{(2,j)}(i) * coh^{(2,j)}(i) \right)}{Coh^{2}(i)} \right)
\label{def_Deltaw}
\end{equation}
}

In the case where each $(n-1)$ coherence associated with station $k$ would be equal to $1$, $\widetilde{\Delta}^{k|k_0}$ (Eq.~\eqref{def_Deltaw}) does not become completely equal to $\Delta^{k|k_0}$ (Eq.~\eqref{Deltak}), as the multiplying factor differs slightly: $(n-1)$ instead of $n$, and we exhibit $n \Delta^{k|k_0} = (n-1) \widetilde{\Delta}^{k|k_0}$. 
Indeed, we do not consider the edges that leave and return to the same node of the graph, which means that we consider the phase difference of auto-correlations as unknown ($\delta^{(k,k)}$ and the associated $coh^{(k,k)}$). 
In this study, we focus on dense arrays; therefore, $\frac{n-1}{n}$ is close to $1$, but we must remember that the $\widetilde{\Delta}$ expression in Eq.~\eqref{def_Deltaw} must be handled with care for an array with a restricted number of stations. Here, we draw a parallel with \cite{stehly2007,sensschoenfelder2008}. They propose to apply a threshold on the coherence to accept or reject the inter-station phase-difference measurements. In our approach, we keep all $\delta$ estimates whatever their associated coherence, but we introduce this coherence-weighted phase delay (Eq.~\eqref{def_Deltaw}) to address the coherence level issue.

\begin{figure*}
\centering
    \includegraphics[width=\textwidth]{figures/Fig3.png}
    \caption{Phase delays $\Delta^{1|2}$ (a,b,d) and coherence-weighted phase delays $\widetilde{\Delta}^{1|2}$ (c) during the $T_2$ period (a,b,c) and during the synchronized $T_1$ period (d) for the de-synchronized station (station 1) when considering station 2 as the master station, as a function of the mean coherence $Coh^{1}$ computed for station 1. We compute the inter-station phase differences $\delta$ with different $SNR$ values (labeled with text) and (b) with applying amplitude decay for inter-station distances greater than $20\,\mathrm{km}$. For each $SNR$ cluster: black circles, averaged $\Delta^{1|2}$ (a,b,d) or averaged $\widetilde{\Delta}^{1|2}$ (c); thin black crosses, twice the standard deviations over the $240$ time steps of the $T_2$ or $T_1$ period ($95\%$ confidence interval for a Gaussian distribution). Dashed red lines, expected value of $15\, \% T_0$ phase delay (a,b,c) or no phase delay (d).}
    \label{Fig3}
\end{figure*}

For each $SNR$ value, Fig.~\ref{Fig3}a shows the computed $\Delta^{1|2}(i)$ as a function of the corresponding averaged $Coh^{1}(i)$, at each time step $i$ of the de-synchronized $T_2$ period (blue dots), and also averaged along these $240$ time steps (black circles). The $95\%$ confidence intervals are indicated as thin black crosses. The dashed red line indicates the $15\, \%$ of $T_0$ phase delay expected value. When $SNR$ varies from $100$ to $0.01$, the average coherence $Coh^{1}$ decreases from $1.0$ to $0.2$, and it is associated with a larger spreading of the phase delays around their average value over the $T_2$ period. The $\Delta^{1|2}$ phase-delay estimation is highly reliable for $SNR$ greater than $1.5$ ($Coh^{1} > 0.85$), as the $\Delta^{1|2}$ estimation for each time step from $T_2$ is close to the $15\, \%$ of $T_0$ expected. 
Next, the $\Delta^{1|2}$ phase-delay estimation is still relatively correct for $SNR$ values greater than $0.7$ ($Coh^{1} > 0.6$), although the spreading among the $240$ time steps is not as clustered around $15\, \%$ of the signal period for $SNR=0.7$ compared to $SNR=100$. 
When comparing to the same analysis for the synchronized $T_1$ period (Fig.~\ref{Fig3}d), the computed $\Delta^{1|2}$ phase-delay estimations no longer appear so clustered around their theoretical zero mean value when the $SNR$ varies from $1.5$ to $1$ and the corresponding average $Coh^{1}$ is less than $0.8$. Regarding again the $T_2$ de-synchronized period (Fig.~\ref{Fig3}a), for $SNR$ less than $0.7$ ($Coh^{1} < 0.6$), the average coherence $Coh^{1}$ dramatically decreases, and the estimated phase delay is largely underestimated. When $SNR$ reaches $0.01$, $Coh^{1}$ is around $0.2$, and $\Delta^{1|2}$ takes random values between $\pm 10\%$ of the signal period, with a zero mean value. 
For these very noisy signals, we measure the same $\Delta^{1|2}$ phase delays 
during the de-synchronized $T_2$ period (Fig.~\ref{Fig3}a) and during the $T_1$ period (Fig.~\ref{Fig3}d); the phase delay estimate is so inaccurate that we make no difference with the synchronized $T_1$ period. 
For $SNR$ around $0.3\,-\,0.4$, the average coherence $Coh^{1}$ is $0.4$, which is the threshold defined by \cite{sensschoenfelder2008} to reject $\delta$ observations prior to their inversion. At this threshold, our synthetic experiment shows that computed phase delays $\Delta^{1|2}$ during $T_2$ period (Fig.~\ref{Fig3}a) are underestimated by $\simeq 40\%$ ($9\, \%$ of $T_0$ instead of the expected $15\, \%$ of $T_0$, i.e. $0.6\,\mathrm{s}$ instead of $1\,\mathrm{s}$).

To understand why a low $SNR$ value leads to $\Delta^{1|2}$ underestimation, we start from the definition of $\Delta$ (Eq.~\eqref{Deltak}); $\Delta^{1|2}$ is the difference between the average of the $n-1$ inter-station phase differences measured at station 1 ($\delta^{1,j}$) and the average of the $n-1$ inter-station phase differences measured at master station 2 ($\delta^{2,j}$). These inter-station phase difference measurements are on a bounded interval $[-\frac{T_0}{2} , \frac{T_0}{2}]$ (Eq.~\eqref{def_deltat}). Figure \ref{FigS1} shows the distributions of inter-station phase differences for a $15\, \% \, T_0$ shift for de-synchronized station 1 ($\delta^{1,j}$, blue) and no shift for master station 2 ($\delta^{2,j}$, red), as a function of the $SNR$ value. Further, if we restrict Fig.~\ref{FigS1} to a single time step, Eq.~\eqref{Deltak} shows that the geometric interpretation of $\Delta^{1|2}$ is the difference in area under the two distributions.

\begin{itemize}
\item When the $SNR$ is high, the $\delta^{1,j}$ and $\delta^{2,j}$ values are Gaussian-distributed around $15\, \%$ and $0$ values, respectively, as expected. The dispersion (standard deviation) of these two Gaussian distributions depends on the quality of the signal (of the $SNR$). With a favorable $SNR$, these Gaussian distributions are narrow, and $\Delta^{1|2}$ is close to $15\, \%$ of $T_0$, as expected. 
\item When the $SNR$ decreases, the dispersion of the two Gaussian distributions increases until it overlaps the $[-\frac{T_0}{2} , \frac{T_0}{2}]$ bounds of the support of these distributions. The average value of the $\delta^{1,j}$ gradually shifts from $15\, \%$ of $T_0$ to $0$. For small values of $SNR$, the distributions of $\delta^{1,j}$ and $\delta^{2,j}$ are no longer Gaussian but become uniform in the interval $[-\frac{T_0}{2} , \frac{T_0}{2}]$.
\end{itemize}

As the support of the distribution of $\delta$ measurements is a bounded interval centered in $0$, the averaged $\delta^{1,j}$ observations related to the de-synchronized station 1 tends to $0$ when $SNR$ decreases, resulting in a phase delay underestimation. Thus, the coherence level is important for the accuracy of the phase-delay estimates. This imperative for high coherence level justifies our choice to focus on phase-shift measurements of the surface wave main arrival, following \cite{stehly2007,sensschoenfelder2008,hable2018,weemstra2021}.

\begin{figure*}
\centering
   \includegraphics[width=\textwidth]{./figures/Fig4.png}
   \caption{Distributions of inter-station phase differences, for the de-synchronized station 1 ($\delta^{1,j}$, blue) and for the master station 2 ($\delta^{2,j}$, red), as a function of the $SNR$ value.}
   \label{FigS1}
\end{figure*}

Figure \ref{Fig3}b shows the same indicators as Fig.~\ref{Fig3}a, but taking into account the amplitude range decay in the NCCF computation for station pairs more than 20 km apart. In this case, the coherence is lower for the same $SNR$ value than in Fig.~\ref{Fig3}a, and the phase delay underestimation occurs for higher $SNR$ values than in Fig.~\ref{Fig3}a.

Now, we are interested in the coherence-weighted phase delay $\widetilde{\Delta}^{1|2}$ (Eq.~\eqref{def_Deltaw}) for the time-shifted station $1$, still considering station $2$ as the master station (Fig.~\ref{Fig3}c). Applying this coherence weighting on the phase-delay computation allows us to considerably reduce the bias in the phase-delay estimation for low $SNR$ values. 
Indeed, for $SNR$ values around $0.3\,-\,0.4$, where the average coherence $Coh^{1}$ is around $0.4$, the $\widetilde{\Delta}^{1|2}$ estimate remains around $13\, \%$ of $T_0$ (while $\Delta^{1|2}$ drops to $9\, \%$ of $T_0$ in Fig.~\ref{Fig3}a). Thus, the coherence-weighted phase-delay underestimation is limited to $\simeq 10\, \%$ inaccuracy, while the unweighted phase delay is underestimated by $\simeq 40\, \%$.

Obviously for the lowest quality signals ($SNR=0.01$ and $Coh^1=0.2$), the averaged value of coherence-weighted phase delay also drops to 0, with a larger standard deviation than the unweighted estimate. 
Therefore, the coherence-weighted estimates $\widetilde{\Delta}$ are less vulnerable to phase delay underestimation than non-weighted estimates $\Delta$ when the $SNR$ (and thus the coherence) decreases. 
In that sense, $\widetilde{\Delta}$ may be more reliable than $\Delta$ for intermediate quality signals.

