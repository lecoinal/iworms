#!/bin/bash
#OAR --project iworms
##OAR --project iste-equ-ondes
#OAR -l /nodes=1/cpu=1/core=1,walltime=02:00:00
#OAR -n logging
#OAR -p network_address='luke4'
##OAR -t besteffort

#  ---------------------------------------------------------------------------------------
#  $Id: submit_IWORMS_preprocess.sh 250 2018-03-28 14:25:56Z lecointre $
#  ---------------------------------------------------------------------------------------

set -e

# CIMENT GRID Clusters
source /applis/ciment/v2/env.bash

#ISTERRE Clusters
#source /soft/env.bash

cat $OAR_NODE_FILE

#####################################################

RAPATRIE_OPTION=1 # 1 # 3 # $2 # 3
   # O : directly read from NFS /media/resif    (MaxResidentSetSize = 1 756 312 kB)
   # 1 : obspy.fdsn.client
   # 2 : wget --post-file : non fontionnel
   # 3 : wget : 1 query for each daytrace --> THE BEST OPTION   (MaxResidentSetSize = 789 952 kB)

JJJ=$1 # "2017011" # $1 # "2017011"

listnet="FR,RD,RA,ZH,Z3,G"
liststa="*"
listloc="*"
listchan="HHZ"
#listnet="FR"
#liststa="ARBF"
#listloc="00"
#listchan="HH*"

minlat=43
maxlat=48.20 # 46
minlon=-0.66 # 3
maxlon=8

##########################################################

PEXE=iWORMS_preprocess.py
#PEXE=iWORMS_preprocess_onlyimport.py
#PEXE=preprocess_refact.py

case "$CLUSTER_NAME" in
  luke)
    PYTHONMODULE="python/2.7.12_gcc-4.6.2"
    MEDIARESIF="/resif/"
    ;;
  ISTERRE)
    PYTHONMODULE="python/python2.7"
    MEDIARESIF="/media/resif/"
    ;;
esac

##########################################################

increment_julday(){
  # usage increment_julday year jday
  # increment the given date of 1d
  year=$(( 10#$1 ));  julday=$(( 10#$2 ));  # en bash les variables ne sont pas typees : ex 08 doit etre entier 8 et pas octal
  if (( year % 4 )) ; then BISSEXTILE=0 ; else  BISSEXTILE=1 ; fi
  if (( $((year%100)) == 0 )) ; then BISSEXTILE=0 ; fi
  if (( $((year%400)) == 0 )) ; then BISSEXTILE=1 ; fi
  if (( $BISSEXTILE )) ; then nbdays=366 ; else nbdays=365 ; fi
  if (( $julday == $nbdays )) ; then
    julday=1
    year=$((year+1))
  else
    julday=$((julday+1))
  fi
  echo $(printf "%04d%03d" $year $julday)
                  }

decrement_julday(){
  # usage decrement_julday year jday
  # decrement the given date of 1d
  year=$(( 10#$1 ));  julday=$(( 10#$2 ));  # en bash les variables ne sont pas typees : ex 08 doit etre entier 8 et pas octal
  previousyear=$((year-1))
  if (( previousyear % 4 )) ; then BISSEXTILE=0 ; else  BISSEXTILE=1 ; fi
  if (( $((previousyear%100)) == 0 )) ; then BISSEXTILE=0 ; fi
  if (( $((previousyear%400)) == 0 )) ; then BISSEXTILE=1 ; fi
  if (( $BISSEXTILE )) ; then nbdays=366 ; else nbdays=365 ; fi
  if (( $julday == 1 )) ; then
    julday=$nbdays
    year=$previousyear
  else
    julday=$((julday-1))
  fi
  echo $(printf "%04d%03d" $year $julday)
                  }

tag2jul(){
    # usage tag2jul year month day : tag2jul YYYY MM DD
    year=$(( 10#$1 ));  month=$(( 10#$2 ));  day=$(( 10#$3 ));  # en bash les variables ne sont pas typees : ex 08 doit etre entier 8 et pas octal
    if (( year % 4 )) ; then BISSEXTILE=0 ; else  BISSEXTILE=1 ; fi
    if (( $((year%100)) == 0 )) ; then BISSEXTILE=0 ; fi
    if (( $((year%400)) == 0 )) ; then BISSEXTILE=1 ; fi
    declare -a calarray=(31 28 31 30 31 30 31 31 30 31 30 31)
    if (( $BISSEXTILE )) ; then calarray[1]=29 ; fi
    JUL=0
    for i in $(seq 0 $((month-2))) ; do a=${calarray[i]} ; JUL=$((JUL+a)) ; done
    JUL=$((JUL+day))
    echo $JUL
         }

jul2tag(){
    # usage jul2tag yearjday : jul2tag YYYYJJJ
    juldate=$(( 10#$1 ));
    year=${juldate:0:4}
    jday=${juldate:4:3}
    if (( year % 4 )) ; then BISSEXTILE=0 ; else  BISSEXTILE=1 ; fi
    if (( $((year%100)) == 0 )) ; then BISSEXTILE=0 ; fi
    if (( $((year%400)) == 0 )) ; then BISSEXTILE=1 ; fi
    declare -a calarray=(31 28 31 30 31 30 31 31 30 31 30 31)
    if (( $BISSEXTILE )) ; then calarray[1]=29 ; fi
    JUL=$(( 10#$jday ))
    i=0
    month=0
    while [ $month -eq 0 ] ; do
      a=${calarray[$i]}
      JUL=$((JUL-a))
      if [ $JUL -le 0 ] ; then month=$((i+1)) ; fi
      i=$((i+1))
    done
    day=$(( 10#$jday ))
    for i in $(seq 0 $((month-2))) ; do
      a=${calarray[$i]}
      day=$((day-a))
    done
    echo $(printf "%04d%02d%02d" $year $month $day)
         }

##########################################################

TAG=$(jul2tag $JJJ)
YYYY=${TAG:0:4}
MM=${TAG:4:2}
DD=${TAG:6:2}
	
echo "$JJJ"

CIGRI_CAMPAIGN_ID=${CIGRI_CAMPAIGN_ID:=00000}  # substitute if not initialized

mkdir -p /$SHARED_SCRATCH_DIR/$USER/$CIGRI_CAMPAIGN_ID

TMPDIR="/$SHARED_SCRATCH_DIR/$USER/IWORMS_preprocess_${CIGRI_CAMPAIGN_ID}_${OAR_JOB_ID}_${JJJ}"
echo $TMPDIR
mkdir -p $TMPDIR

cp $PEXE $TMPDIR
cp whitenWhisper.py $TMPDIR

cd $TMPDIR

##########################
# Get metadata
# The FDSN Station webservice returns station metadata. Results are available at multiple levels of granularity: network, station, channel and response. Metadata may be selected based on network, station, location or channel names in addition to time windows, geographic regions or metadata update times. Seismic metadata is available as FDSN StationXML and as tabular text format (RESIF Stations's webservice supports a special legacy option for dataless seed output). Data inventories are available through station webservice, from network to channel levels.

fmt="text"

# Print J+1 as yyyy-mm-dd
tmpTAG=$(jul2tag $(increment_julday ${JJJ:0:4} ${JJJ:4:3}))
TAGP1=$(echo "${tmpTAG:0:4}-${tmpTAG:4:2}-${tmpTAG:6:2}")

#set -x

url="http://ws.resif.fr/fdsnws/station/1/query?network=${listnet}&station=${liststa}&location=${listloc}&channel=${listchan}&format=${fmt}&level=channel&minlatitude=${minlat}&maxlatitude=${maxlat}&minlongitude=${minlon}&maxlongitude=${maxlon}&starttime=${YYYY}-${MM}-${DD}&endtime=${TAGP1}"
RC=$(wget -O stationsByCodes.${fmt} --server-response "$url" 2>&1 | awk '/^  HTTP/{print $2}')
# Check the return status
# The return status "204" (request was properly formatted and submitted but no data matches the selection) and "200" (successful request, results follow) are "normal"
# all other are fatal errors
echo "RC du wget: $RC"
case "$RC" in
  "200") grep -v "^#" stationsByCodes.${fmt} | awk -F'|' '{print $1 " " $2 " " $3 " " $4}' | sort -u > list_rsync.txt ;;
  "204") echo "$listnet : nodata"
         exit 0 ;; # As cigri should not abort
  *) exit $RC ;;
esac

##########################

nbtr=$(wc -l list_rsync.txt | awk '{print $1}')

# incompatibilite awk et python/2.7.12 donc creation array -> could be fixed with module load gawk (ticket id=2017051601)
declare -a arrnet
declare -a arrsta
declare -a arrloc
declare -a arrchan
# Loop over Network_Station_Channel
ii=0
while read tr ; do
  arrnet[$ii]=$(echo $tr | awk '{print $1}')
  arrsta[$ii]=$(echo $tr | awk '{print $2}')
  arrloc[$ii]=$(echo $tr | awk '{print $3}')
  arrchan[$ii]=$(echo $tr | awk '{print $4}')
  ii=$((ii+1))
done < list_rsync.txt

# Get USER and PASS before loading python module (incompatibilite awk) in case of wget option
if [[ $RAPATRIE_OPTION == '2' || $RAPATRIE_OPTION == '3' ]] ; then
  HTTPUSER=$(grep "user" $HOME/.iworms.conf | awk '{print $2}')
  HTTPPASS=$(grep "password" $HOME/.iworms.conf | awk '{print $2}')
fi

if [ $RAPATRIE_OPTION == 0 ] ; then

  JM1=$(decrement_julday ${JJJ:0:4} ${JJJ:4:3})  # $1 # "2017008" # "2015365"
  JP1=$(increment_julday ${JJJ:0:4} ${JJJ:4:3})  # $3 # "2017010" # "2016002"

  module load ${PYTHONMODULE}
  # Loop over Network_Station_Channel
  ii=0
  while read tr ; do
    START=$(date +%s.%N)
    net=${arrnet[$ii]}
    sta=${arrsta[$ii]}
    loc=${arrloc[$ii]}
    chan=${arrchan[$ii]}
    ii=$((ii+1))
    echo -n "$tr : $ii / $nbtr : "
    # Cat a 3days list of files but only if J exists
    # Loop over J, J-1, J+1
    YEAR=${JJJ:0:4}
    JDAY=${JJJ:4:3}
    file="/${MEDIARESIF}/validated_seismic_data/$YEAR/$net/$sta/$chan.D/$net.$sta.$loc.$chan.D.$YEAR.$JDAY"
    if [ -f $file ]; then
      listfile=""
      for J in $JM1 $JJJ $JP1 ; do
        YEAR=${J:0:4}
        JDAY=${J:4:3}
        #file="/nfs_scratch/lecointre/IWORMS/media/resif/validated_seismic_data/$YEAR/$net/$sta/$chan.D/$net.$sta.$loc.$chan.D.$YEAR.$JDAY"
        file="/${MEDIARESIF}/validated_seismic_data/$YEAR/$net/$sta/$chan.D/$net.$sta.$loc.$chan.D.$YEAR.$JDAY"
        if [ -f $file ]; then
          listfile="$listfile $file"
        fi
      done
      YEAR=${JJJ:0:4}
      JDAY=${JJJ:4:3}
#$SHARED_SCRATCH_DIR/$USER/bin/time -f "\t%M Maximum resident set size (KB)" python2.7 $PEXE $YEAR $JDAY
      cat $listfile | python2.7 $PEXE $YEAR $JDAY $RAPATRIE_OPTION
    else
      echo -n "no data "
    fi
    END=$(date +%s.%N)
    DIFF=$(echo "$END - $START" | bc)
    echo "$DIFF sec"
  done < list_rsync.txt

elif [ $RAPATRIE_OPTION == 1 ] ; then
	
  echo "obspy.clients.fdsn.Client"
  module load ${PYTHONMODULE}
  # Loop over Network_Station_Channel
  ii=0
  while read tr ; do
    net=${arrnet[$ii]}
    sta=${arrsta[$ii]}
    loc=${arrloc[$ii]}
    chan=${arrchan[$ii]}
    ii=$((ii+1))
    echo -n "$tr : $ii / $nbtr : "
    # Cat a 3days list of files but only if J exists
    # Loop over J, J-1, J+1
    YEAR=${JJJ:0:4}
    JDAY=${JJJ:4:3}
    START=$(date +%s.%N)
#echo -n "start $START "
#$SHARED_SCRATCH_DIR/$USER/bin/time -f "\t%M Maximum resident set size (KB)" python2.7 $PEXE $YEAR $JDAY $RAPATRIE_OPTION $net $sta $loc $chan
    python2.7 $PEXE $YEAR $JDAY $RAPATRIE_OPTION $net $sta $loc $chan
    END=$(date +%s.%N)
#echo -n "end $END "
    DIFF=$(echo "$END - $START" | bc)
    echo "$DIFF sec"
#echo "$YEAR $JDAY $net $sta $loc $chan"
  done < list_rsync.txt

elif [ $RAPATRIE_OPTION == 2 ] ; then
	
  echo "wget --post-file : non fonctionnel"
#  NTAG=$(echo "${YYYY}-${MM}-${DD}")
#  cat list_rsync.txt | awk -v datevar="$NTAG" '{print $1 " " $2 " " $3 " " $4 " " datevar "T00:00:00.000000 " datevar "T23:59:59.990000"}' > mydatarequest.txt	
#  wget --post-file=mydatarequest.txt -O - http://ws.resif.fr/fdsnws/dataselect/1/query > tmp.mseed	

else

  echo "wget : 1 query for each daytrace"

  YEAR=${JJJ:0:4}
  JDAY=${JJJ:4:3}
  JM1=$(decrement_julday ${JJJ:0:4} ${JJJ:4:3})  # $1 # "2017008" # "2015365"
  JP1=$(increment_julday ${JJJ:0:4} ${JJJ:4:3})  # $3 # "2017010" # "2016002"
  tmpTAG=$(jul2tag $JM1)
  YYYY=${tmpTAG:0:4}
  MM=${tmpTAG:4:2}
  DD=${tmpTAG:6:2}
  NTAGM1=$(echo "${YYYY}-${MM}-${DD}")
  tmpTAG=$(jul2tag $JP1)
  YYYY=${tmpTAG:0:4}
  MM=${tmpTAG:4:2}
  DD=${tmpTAG:6:2}
  NTAGP1=$(echo "${YYYY}-${MM}-${DD}")

  module load ${PYTHONMODULE}

  # Loop over Network_Station_Channel
  ii=0
  while read tr ; do
    START=$(date +%s.%N)
#echo -n "start $START "
    net=${arrnet[$ii]}
    sta=${arrsta[$ii]}
    loc=${arrloc[$ii]}
    chan=${arrchan[$ii]}
    ii=$((ii+1))
    echo -n "$tr : $ii / $nbtr : "

    wget --http-user="${HTTPUSER}" --http-password="${HTTPPASS}" "http://ws.resif.fr/fdsnws/dataselect/1/queryauth?network=${net}&station=${sta}&location=${loc}&channel=${chan}&starttime=${NTAGM1}T23:59:00.000000&endtime=${NTAGP1}T00:01:00.000000" -O - | python2.7 $PEXE $YEAR $JDAY $RAPATRIE_OPTION $net $sta $loc $chan
#    wget --http-user="${HTTPUSER}" --http-password="${HTTPPASS}" "http://ws.resif.fr/fdsnws/dataselect/1/queryauth?quality=M&network=${net}&station=${sta}&location=${loc}&channel=${chan}&starttime=${NTAGM1}T23:59:00.000000&endtime=${NTAGP1}T00:01:00.000000" -O - | python2.7 $PEXE $YEAR $JDAY $RAPATRIE_OPTION $net $sta $loc $chan
    #wget --http-user="${HTTPUSER}" --http-password="${HTTPPASS}" "http://ws.resif.fr/fdsnws/dataselect/1/queryauth?network=${net}&station=${sta}&location=${loc}&channel=${chan}&starttime=${NTAGM1}T23:59:00.000000&endtime=${NTAGP1}T00:01:00.000000" -O - > /dev/null

# debug :
    #wget --http-user="${HTTPUSER}" --http-password="${HTTPPASS}" "http://ws.resif.fr/fdsnws/dataselect/1/queryauth?network=${net}&station=${sta}&location=${loc}&channel=${chan}&starttime=${NTAGM1}T23:59:00.000000&endtime=${NTAGP1}T00:01:00.000000" -O - > tmp.mseed
#ls -l tmp.mseed
#python2.7 $PEXE $YEAR $JDAY -1 $net $sta $loc $chan
#/usr/bin/time -f "\t%M Maximum resident set size (KB)" python2.7 $PEXE $YEAR $JDAY -1 $net $sta $loc $chan

    END=$(date +%s.%N)
#echo -n "end $END "
    DIFF=$(echo "$END - $START" | bc)
    echo "$DIFF sec"
#echo "url: http://ws.resif.fr/fdsnws/dataselect/1/queryauth?network=${net}&station=${sta}&location=${loc}&channel=${chan}&starttime=${NTAGM1}T23:59:00.000000&endtime=${NTAGP1}T00:01:00.000000"
  done < list_rsync.txt
  
fi

mv $TMPDIR /$SHARED_SCRATCH_DIR/$USER/$CIGRI_CAMPAIGN_ID/. 
