#!/bin/bash
#OAR --project iworms
##OAR --project iste-equ-ondes
##OAR -l /nodes=1/cpu=1/core=1,walltime=02:00:00
##OAR -l /nodes=2/cpu=1/core=1,walltime=00:30:00
#OAR -l /nodes=1/cpu=1/core=2,walltime=00:05:00
#OAR -n logging
##OAR -p network_address='luke4'
##OAR -t besteffort
#OAR -t devel

set -e

#rm -rf LOG
#mkdir LOG

cat $OAR_NODE_FILE

nproc=$(cat $OAR_NODE_FILE | wc -l)

#path2virtualenv="$HOME/iworms/iworms_preprocess_src/version_nix/virtualenv"
path2virtualenv="$HOME/tmp/iworms/iworms_preprocess_src/version_nix/virtualenv"

#source /applis/site/nix.sh
source /applis/site/guix.sh
# Si on a switché de profile nix entretemps pour travailler sur autre chose, on 
# oublie pas de revenir sur le bon 
# nix-env --switch-profile $NIX_USER_PROFILE_DIR/iworms
source $path2virtualenv/obspy/bin/activate


## pour /usr/lib/x86_64-linux-gnu/libstdc++.so.6 ?
echo $LD_LIBRARY_PATH
export LD_LIBRARY_PATH=/usr/lib/x86_64-linux-gnu

#mpirun -n $nproc ./preprocess_refact.py

which mpirun

#mpirun -x LD_LIBRARY_PATH --machinefile $OAR_NODE_FILE --mca orte_rsh_agent "oarsh" --mca btl_openib_allow_ib "true" -n $nproc ./test.py
mpirun -x LD_LIBRARY_PATH --machinefile $OAR_NODE_FILE --mca orte_rsh_agent "oarsh" -n $nproc ./test.py

#cat LOG/log*.txt > LOG/log.txt

echo "Done"
