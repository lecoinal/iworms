# Faire tourner les prétraitements iWorms sur Luke
## Côté Nix
On source d’abord nix (si vous ne le faites pas le .bashrc): 
`source /applis/site/nix.sh`

Pour faire les choses bien, on créée un profile nix dédié:
`nix-env --switch-profile $NIX_USER_PROFILE_DIR/iworms`

Ensuite, on installe la version courante d’openmpi (sans ça, ça ne marche pas, je ne sais pas encore exactement pourquoi)
`nix-env -iA ciment-channel.openmpi`

Vient alors l’installation de l’environnement python qui va bien. On écrit un fichier python3.nix, contenant le bloc suivant : 

```
with import <nixpkgs> {};
pkgs.python36.withPackages (ps: with ps; [ numpy h5py-mpi lxml scipy matplotlib ipython virtualenv pip ])
```

Vous noterez qu’on est en python 3.  Le paquet h5py-mpi vient avec mpi4py. C’est pour ça que ce dernier n’est pas dans la liste.
Une fois ce fichier sauvegardé, on l’installe : 
`nix-env -if python3.nix`

On en a fini avec nix. Maintenant il faut installer obspy.

## Installation d’obspy avec virtualenv + pip
Obspy n’étant pas disponible dans Nix, il faut l’installer dans un virtual env avec pip. Pour ce faire :
```
mkdir virtualenv
cd virtualenv
virtualenv obspy
source obspy/bin/activate
pip install obspy
```

Et voilà ! Maintenant on a tout ce qu’il faut. Plus qu’à soumettre. 

## Soumission du job
Dans le script de soumission du job,  deux choses à ne pas oublier avant le mpirun : Sourcer nix et virtualenv.  Cela se fait en ajoutant ces deux lignes :
```
source /applis/site/nix.sh
# Si on a switché de profile nix entretemps pour travailler sur autre chose, on oublie pas de revenir sur le bon 
# nix-env --switch-profile $NIX_USER_PROFILE_DIR/iworms
source path2virtualenv/obspy/bin/activate
```

Si j’ai oublié quelque chose ou si vous avez un souci, contactez-moi.

#iworms