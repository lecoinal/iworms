Anciennes versions, conservées pour mémoire, qui fonctionnaient : 
 * soit avec le gestionnaire de paquets "module" sur froggy avec python 2.7.12
 * soit avec nix sur dahu avec python3 : fonctionnait en 2019 (voir [python3.nix](./python3.nix) et [nix_iworms.md](./nix_iworms.md)) mais plus en 2020, on ne sait pas pourquoi, on a switché sur l'utilisation de guix

