with import <nixpkgs> {};
pkgs.python36.withPackages (ps: with ps; [ numpy h5py-mpi lxml scipy matplotlib ipython virtualenv pip ])
