#  =======================================================================================
#                       ***  Python function whitenWhisper.py  ***
#   This is the Whiteing Python script from original whisper code (X. Briand)
#    
#  =======================================================================================
#   History :  1.0  : 2013  : X. Briand : Initial version
#               
#  ---------------------------------------------------------------------------------------
#
#  ---------------------------------------------------------------------------------------
#
#  http://forge.osug.fr/trac/ISTERRE-Correlation/wiki/Corr
#
#  ---------------------------------------------------------------------------------------
#  $Id: whitenWhisper.py 234 2017-07-27 08:21:39Z lecointre $
#  ---------------------------------------------------------------------------------------

import numpy as np
import scipy.fftpack
#import pyfftw


def WhiteningCos(Trace, freqMin, freqMax, FreqSampling, DivideFreq):
    """
    Return the trace whitened with an apodization in cosinus square.
    
    :Parameters:
        **Trace**: numpy array
            The trace to be whitened.
        **freqMin**, **freqMax**: float
            The frequency minimal and the frequency maximal which define the interval.
        **FreqSampling**: float
            The frequency of the sampling.
        **DivideFreq**: float
            The percentage of the length of the apodization of each border.
            
    """
    IndexWhiteMin, IndexWhiteMax, LengthApodisation, BorderLeft, BorderRight, NoramisationWhitening = \
        prepareWhiteningCos(len(Trace), freqMin, freqMax, FreqSampling, DivideFreq)
    return WhiteningCosPrepared(Trace, IndexWhiteMin, IndexWhiteMax, LengthApodisation, BorderLeft, BorderRight, NoramisationWhitening)

def WhiteningCosPrepared(Trace, IndexWhiteMin, IndexWhiteMax, LengthApodisation, BorderLeft, BorderRight, NormalisationWhitening):
    TraceWitened =  np.zeros(len(Trace), dtype='complex')
    TraceWitened[IndexWhiteMin:IndexWhiteMax] = scipy.fftpack.fft(Trace)[IndexWhiteMin:IndexWhiteMax]
    TraceWitened[IndexWhiteMin:IndexWhiteMax] /= np.absolute(TraceWitened[IndexWhiteMin:IndexWhiteMax])*NormalisationWhitening
    TraceWitened[IndexWhiteMin:IndexWhiteMin+int(LengthApodisation)] *= BorderLeft
    TraceWitened[IndexWhiteMax-int(LengthApodisation):IndexWhiteMax] *= BorderRight
    return ((scipy.fftpack.ifft(TraceWitened, overwrite_x=True)).real)

def prepareWhiteningCos(LengthTrace, freqMin, freqMax, FreqSampling, DivideFreq):
    dt = (freqMax - freqMin)/DivideFreq
    IndexWhiteMin = int(round(freqMin*LengthTrace/FreqSampling+1))-1
    IndexWhiteMax = int(round(freqMax*LengthTrace/FreqSampling+1))
    LengthApodisation = round(dt/FreqSampling*LengthTrace)
    BorderLeft = np.square(np.sin(np.arange(1,int(LengthApodisation)+1)/LengthApodisation/2*np.pi))
    BorderRight = np.square(np.sin(np.arange(int(LengthApodisation)+1,2*LengthApodisation+1)/LengthApodisation/2*np.pi))
    NoramisationWhitening = np.sqrt(float(IndexWhiteMax-IndexWhiteMin-5*LengthApodisation/4)/LengthTrace/2)
    return IndexWhiteMin, IndexWhiteMax, LengthApodisation, BorderLeft, BorderRight, NoramisationWhitening
