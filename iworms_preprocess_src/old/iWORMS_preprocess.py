#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

#  =======================================================================================
#                       ***  Python script iWORMS_preprocess.py  ***
#   This is the main Python script to preprocess RESIF MSEED data
#
#  =======================================================================================
#   History :  1.0  : 05/2017  : A. Lecointre : Initial Python version
#
#  ---------------------------------------------------------------------------------------
#
#  ---------------------------------------------------------------------------------------
#   methodology   : cleaning J-1, J, J+1
#                 : interpolate 100Hz (if necessary)
#                 : interpolate onto a fixed time vector (starttime.microsecond should be 
#                   XX0000)
#                 : cut between 00:00:00.000000 and 23:59:59.990000
#                 : highpass filter 0.05Hz
#                 : gapfilling : fill remaning gap with zero [TODO: and keep info about 
#                   gap position]
#                 : bandpass filter : 0.2 - 0.3333Hz
#                 : decimation factor 10 (no spline interp)
#                 : whitening : 0.2 - 0.3333Hz
#                 : check for NaN or Inf
#                 : 1-Bit
#                 : Int8 encoding
#                 : write output as a dataset into HDF5 daily file with gzip compression
#
#  https://ciment.ujf-grenoble.fr/wiki/index.php/Projects/pr-iworms#Preprocessing_des_donn.C3.A9es
#
#  ---------------------------------------------------------------------------------------
#  $Id: iWORMS_preprocess.py 251 2018-03-28 14:27:55Z lecointre $
#  ---------------------------------------------------------------------------------------

from obspy.core import read,UTCDateTime,Trace
import numpy as np
import h5py
import sys
# whitening code Whisper (X. Briand)
import whitenWhisper

cyr = sys.argv[1]  # as a string : "%04d"
cjd = sys.argv[2]  # as a string : "%03d"
yr = int(cyr)
jd = int(cjd)
opt = int(sys.argv[3])

# Create output HDF5 file in append mode
h5file = h5py.File('./'+'{:>04}'.format(yr)+'.'+'{:>03}'.format(jd)+'.trace.int8.h5','a')

if (opt == 0) or (opt == 3): 
	# Read stream from input files from NFS /media/resif (opt=0) or from input stream from wget -O - (opt=3)
	#s = read('tmp.mseed')
	from StringIO import StringIO
	try:
		s = read(StringIO(sys.stdin.read()))
	except Exception as e:
		print e
		sys.exit(0)  # cigri should not abort

elif opt == -1:
	# Used only for debug
	s = read("tmp.mseed")

elif opt == 1:
        # Download input stream thanks to obspy.clients.fdsn
	inet=sys.argv[4]
	ista=sys.argv[5]
	iloc=sys.argv[6]
	ichan=sys.argv[7]
	from obspy.clients.fdsn import Client
	import ConfigParser
	config=ConfigParser.ConfigParser()
	import getpass
	USERNAME=getpass.getuser()
	CONFIG_FILE="/home/"+USERNAME+"/.iworms.conf"
	import os
	os.path.isfile(CONFIG_FILE)
	config.read(CONFIG_FILE)
	USER=config.get('global','user')
	PASS=config.get('global','password')
	client = Client("RESIF",user=USER,password=PASS)
	t = UTCDateTime(cyr+cjd+"T00:00:00.000")
	print("client.get_waveforms",inet, ista, iloc, ichan, t - 60, t + 86400 + 60)
	try:
		#s = client.get_waveforms(inet, ista, iloc, ichan, t - 60, t + 86400 + 60, quality='M') # get from J-1min to J+1min to prevent bad time vector...
		s = client.get_waveforms(inet, ista, iloc, ichan, t - 60, t + 86400 + 60) # get from J-1min to J+1min to prevent bad time vector...
	except Exception as e:
		print e
		sys.exit(0)  # cigri should not abort

totnpts=0
for itr in range(s.count()):
	totnpts=totnpts+s[itr].stats.npts
print("total_npts",totnpts)

################################################################
# Nettoyage J-1,J,J+1:
# - 100Hz interpolaton if necessary
# - interpolation: time vector microsecond = XX0000 if necessary 
# - cut between 00:00:00.000000 and 23:59:59.990000
################################################################

# remove trace if not at least 2 samples
for itr in s.select(npts=1):
  s.remove(itr)

# for each traces in the stream
ff = 100. # on veut interpoler a 100Hz si ce n'est pas le cas
for itr in range(s.count()):
  ts = s[itr].stats.starttime
  # check if 1e6*(1./ff) divides microsec or not
  if (ts.microsecond % (1e6*(1./ff))) == 0:
    nts = ts
  else:  # on va a la freq 100Hz suivante
    microsec_to_add = (1e6*(1./ff)) - (ts.microsecond % (1e6*(1./ff)))
    nts = ts + 1e-6*microsec_to_add
  # interpolate to 100Hz and force to starttime.microsecond=XX0000
  #s[itr].interpolate(sampling_rate=100.,starttime=nts,method='weighted_average_slopes') # -> parfois last sample becomes nan ???
  #s[itr].interpolate(sampling_rate=ff,starttime=nts,method='linear')
  s[itr].interpolate(sampling_rate=ff,starttime=nts,method='lanczos',a=10)

# cut the trace(s) between 00:00:00.000000 and 23:59:59.990000
s.trim(starttime=UTCDateTime(year=yr, julday=jd),endtime=UTCDateTime(year=yr, julday=jd)+86400-s[0].stats.delta)

# stop if there is no stream left
if ( s.count() == 0 ):
  print('no data left after trim')
  sys.exit(0) # cigri should not abort

################################################################
# Highpass Filter 0.05Hz
################################################################

s.filter("highpass", freq=0.05)

################################################################
# Gap filling : fill with zero
################################################################

net = s[0].stats.network
sta = s[0].stats.station
loc = s[0].stats.location
channel = s[0].stats.channel

datatype = s[0].data.dtype

meta = {'station': sta, 'location': loc, 'network': net, 'channel': channel}

stend = s[-1].stats.endtime

trb = Trace(data=np.zeros(1,dtype=datatype),header=meta)
trb.stats.delta = s[0].stats.delta
trb.stats.starttime = UTCDateTime(str(yr)+"{:03d}".format(jd))
if s[0].stats.starttime != trb.stats.starttime:
        s.append(trb)

tre = Trace(data=np.zeros(1,dtype=datatype),header=meta)
tre.stats.delta = s[0].stats.delta
tre.stats.starttime = UTCDateTime(str(yr)+"{:03d}".format(jd))+86400-s[0].stats.delta
if stend != tre.stats.endtime:
        s.append(tre)

s.merge(method=0, fill_value=0)

################################################################
# Bandpass filter : 0.2 - 0.3333 Hz
################################################################

fb1 = 0.1 # 0.05 # 0.2 # <<FB1>> # 0.5
fb2 = 0.2 # 0.1 # 0.3333 # <<FB2>> # 25.0
s.filter("bandpass", freqmin=fb1, freqmax=fb2, corners=4, zerophase=True)

################################################################
# Decimation factor 10 using spline interpolation, update the channel name
################################################################

#tck = interpolate.splrep(np.arange(0, s[0].stats.npts, 1), s[0].data, s=0)
#s[0].data = interpolate.splev(np.arange(0, s[0].stats.npts , 10), tck, der=0)
#s[0].stats.sampling_rate=s[0].stats.sampling_rate/10
##channel = 'EPZ'   # extremely short period = 100Hz and corner period of the geophone = 0.1sec
##s[0].stats.channel = channel

################################################################
# Decimation factor 10: keep one sample/10
################################################################

s.decimate(factor=10, strict_length=False, no_filter=True)

################################################################
# Whitening
################################################################

freq = s[0].stats.sampling_rate # 100.0
DivideFreq = 20.0
s[0].data = whitenWhisper.WhiteningCos(s[0].data, fb1, fb2, freq, DivideFreq)

################################################################
# 1-Bit
################################################################

if np.size(np.where(np.isnan(s[0].data)))!=0:
	print('Presence de NaN dans data avant 1bit')
	print(np.where(np.isnan(s[0].data)))
	sys.exit(1)
if np.size(np.where(np.isinf(s[0].data)))!=0:
	print('Presence de +/-Inf dans data avant 1bit')
	print(np.where(np.isnan(s[0].data)))
	sys.exit(1)

s[0].data = np.sign(s[0].data)

################################################################
# Writing dataset into HDF5 output file
################################################################

arrname = net+"."+sta+"."+loc+"."+channel  # +".D."+cyr+"."+cjd
dset = h5file.create_dataset(arrname, (864000,), dtype='int8', compression="gzip", compression_opts=6, fletcher32='True')
dset[:] = s[0].data.astype('int8')
h5file.close()

