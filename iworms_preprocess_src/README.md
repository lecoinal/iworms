# Utilisation de GUIX pour préparer l'environnement logiciel pour jouer les benchs de preprocess RESIF

On dahu|luke frontend:

```
> source /applis/site/guix-start.sh
```

Create a new GUIX profile:
```
> refresh_guix prepobspy
```

Install python3 obspy mpi4py and h5py in this profile. 

For this, use the GUIX manifest [./manifest_prepobspy.scm](./manifest_prepobspy.scm):

```
> guix package -m ./manifest_prepobspy.scm -p $GUIX_USER_PROFILE_DIR/prepobspy
[...]
The following packages will be installed:
   openmpi       4.1.1
   python        3.9.9
   python-h5py-mpi 2.10.0
   python-mpi4py 3.0.3
   python-obspy  1.2.2
[...]

```
Then, at each new session:
```
> source /applis/site/guix-start.sh
> refresh_guix prepobspy
Activate profile  /var/guix/profiles/per-user/lecoinal/prepobspy
The following packages are currently installed in /var/guix/profiles/per-user/lecoinal/prepobspy:
openmpi      	4.1.1 	out	/gnu/store/6gqpzdghvm6bmnhqxjnlyamx6fa5a2a6-openmpi-4.1.1
python-mpi4py	3.0.3 	out	/gnu/store/jc6q1hywlwrmdpp4scvvdr0zz8s26zxy-python-mpi4py-3.0.3
python-h5py-mpi	2.10.0	out	/gnu/store/1z823q6bl61r3jmkblld0pzh4v9x92hh-python-h5py-mpi-2.10.0
python-obspy 	1.2.2 	out	/gnu/store/5nvw0869xvg7lsirp76pgkdwx2s4jmpx-python-obspy-1.2.2
python       	3.9.9 	out	/gnu/store/sz7lkmic6qrhfblrhaqaw0fgc4s9n5s3-python-3.9.9

```

The [./submit_guix.sh](./submit_guix.sh) GUIX submission script is an example OAR script to execute preprocess_refact.py code with using multinode parallelization.

---

# Discussion

For a Discussion about Big Data Downloading and Processing Challenges :

see [../paper/old/computing_data_challenges.tex](../paper/old/computing_data_challenges.tex)

[and wipipage about tests with RESIF datacenter and GriCAD clusters](https://ciment-grid.univ-grenoble-alpes.fr/legacy-wiki/ciment.ujf-grenoble.fr/wiki/index.php/Projects/pr-iworms) (look for items with *preprocess* / *logging* / *webservices* / ... in the summary)
 
<!---

Sur la frontale dahu:

```
> source /applis/site/guix-start.sh

> guix package -I

> echo $PATH
/home/lecointre/.guix-profile//bin:/home/lecointre/.guix-profile//sbin:/gnu/store/ajaqjzsdlcqmbrrxnmwazifw4ajdkvn2-profile/bin:/nix/var/nix/profiles/default/bin:/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games

> which guix
/gnu/store/ajaqjzsdlcqmbrrxnmwazifw4ajdkvn2-profile/bin/guix
```

On commence par créer le channels.scm du readme du channel gricad (https://gricad-gitlab.univ-grenoble-alpes.fr/bouttiep/gricad_guix_packages)

```
> cat ~/.config/guix/channels.scm
;; Add gricad packages to those Guix provides.
(cons (channel
	(name 'gricad-guix-packages)
	(url "https://gricad-gitlab.univ-grenoble-alpes.fr/bouttiep/gricad_guix_packages.git"))
      %default-channels)
```

On met à jour notre distribution locale

```
>  guix pull
Updating channel 'gricad-guix-packages' from Git repository at 'https://gricad-gitlab.univ-grenoble-alpes.fr/bouttiep/gricad_guix_packages.git'...
Updating channel 'guix' from Git repository at 'https://git.savannah.gnu.org/git/guix.git'...
Authenticating channel 'guix', commits 9edb3f6 to cc5f934 (29 new commits)...
[...]
hint: Consider setting the necessary environment variables by running:

     GUIX_PROFILE="/home/lecointre/.config/guix/current"
     . "$GUIX_PROFILE/etc/profile"

Alternately, see `guix package --search-paths -p "/home/lecointre/.config/guix/current"'.

New in this revision:
  1 package upgraded: cuirass@0.0.1-46.d22ffdf

hint: After setting `PATH', run `hash guix' to make sure your shell refers to `/home/lecointre/.config/guix/current/bin/guix'.
```

Pour l'instant j'utilise le guix du systeme :
```
> echo $GUIX_PROFILE 
/home/lecointre/.guix-profile/
> echo $PATH
/home/lecointre/.guix-profile//bin:/home/lecointre/.guix-profile//sbin:/gnu/store/ajaqjzsdlcqmbrrxnmwazifw4ajdkvn2-profile/bin:/nix/var/nix/profiles/default/bin:/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games
> which guix
/gnu/store/ajaqjzsdlcqmbrrxnmwazifw4ajdkvn2-profile/bin/guix
```

On veut utiliser le guix de mon env personnel (hint 1), et pas celle du système, qui est out-of-date, au bon vouloir des admins, ... 

L'autre hint concerne les paquets de mes profils

```
> GUIX_PROFILE="/home/lecointre/.config/guix/current"
> . "$GUIX_PROFILE/etc/profile"
> echo $PATH
/home/lecointre/.config/guix/current/bin:/home/lecointre/.guix-profile//bin:/home/lecointre/.guix-profile//sbin:/gnu/store/ajaqjzsdlcqmbrrxnmwazifw4ajdkvn2-profile/bin:/nix/var/nix/profiles/default/bin:/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games
> which guix
/home/lecointre/.config/guix/current/bin/guix
```

Par contre, je ne suis pas le 2eme hint (mais je ne sais pas encore bien pourquoi ?)

__TODO__: ici, il faut reprendre et créer un profil dédié pour ces benchs de preprocess...


__TODO__: j'ai l'impression qu'il y a un bug dans le code python du preprocess : sampling rate hard-coded : 10Hz vs 100Hz ...


Pour l'instant on n'a rien installé :
```
> guix package -I
>
```

On y va pour tout ca :
```
> guix install python-h5py-mpi python-obspy python-numpy python-mpi4py python python-matplotlib python-scipy
The following packages will be installed:
   python            3.8.2
   python-h5py-mpi   2.10.0
   python-matplotlib 3.1.2
   python-mpi4py     3.0.3
   python-numpy      1.17.3
   python-obspy      1.2.2
   python-scipy      1.3.2

The following derivation will be built:
   /gnu/store/qmrv81w7d6kn1rwpa7smrfija8q9h8f2-profile.drv

building profile with 7 packages...
hint: Consider setting the necessary environment variables by running:

     GUIX_PROFILE="/home/lecointre/.guix-profile"
     . "$GUIX_PROFILE/etc/profile"

Alternately, see `guix package --search-paths -p "/home/lecointre/.guix-profile"'.
```

Cette fois ci je ne suis pas le hint...
```
> guix package -I
python-h5py-mpi	2.10.0	out	/gnu/store/77dr5hz60al5ff3jw51faihfwl81ki5y-python-h5py-mpi-2.10.0
python-obspy	1.2.2	out	/gnu/store/wayw9zp9s2krg2kj9hxia9glni511z33-python-obspy-1.2.2
python-numpy	1.17.3	out	/gnu/store/aawl4k64qq5mwczi2ndaap3pgbqlb1pd-python-numpy-1.17.3
python-mpi4py	3.0.3	out	/gnu/store/5sa7617f82xy12yhzg0rihmcyyvn34zh-python-mpi4py-3.0.3
python	3.8.2	out	/gnu/store/kmfg2lq2aqgmdrr16hk7nc878aafpqhn-python-3.8.2
python-matplotlib	3.1.2	out	/gnu/store/gsjhzr1xy96hz8wvaa8mblvv2niqnkwy-python-matplotlib-3.1.2
python-scipy	1.3.2	out	/gnu/store/hyjjyl5kk2jwfiasnqzw4f9fhq3igy8a-python-scipy-1.3.2

> which python3
/home/lecointre/.guix-profile//bin/python3
> python3
Python 3.8.2 (default, Jan  1 1970, 00:00:01) 
[GCC 7.5.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> from obspy.core import read
>>> 
```

Pour l'instant je suis tjs sur la frontale dahu, maintenant testons sur un noeud (puis plusieurs) de calcul

on teste ce code :
```
> cat test.py
#!/usr/bin/env python
# -*- coding: utf-8 -*-
from obspy.core import read,UTCDateTime,Trace
from mpi4py import MPI
import h5py
import numpy as np
if __name__ == '__main__':
    n_process = MPI.COMM_WORLD.size
    rank = MPI.COMM_WORLD.rank

```
Avec ce script de soumission
```
> cat submit_test.sh 
#!/bin/bash
#OAR --project iworms
#OAR -l /nodes=1/cpu=1/core=2,walltime=00:05:00
#OAR -n logging
#OAR -t devel
set -e
cat $OAR_NODE_FILE
nproc=$(cat $OAR_NODE_FILE | wc -l)
source /applis/site/guix-start.sh
which mpirun
echo $LD_LIBRARY_PATH
mpirun -x LD_LIBRARY_PATH --machinefile $OAR_NODE_FILE --mca orte_rsh_agent "oarsh" -n $nproc ./test.py
echo "Done"
```

__TODO__: détailler les erreurs de ce job : 

11735726

```
> cat OAR.logging.11735726.stdout
dahu35
dahu35
Default profile on
The following packages are currently installed in /home/lecointre/.guix-profile/:
python-h5py-mpi	2.10.0	out	/gnu/store/77dr5hz60al5ff3jw51faihfwl81ki5y-python-h5py-mpi-2.10.0
python-obspy	1.2.2	out	/gnu/store/wayw9zp9s2krg2kj9hxia9glni511z33-python-obspy-1.2.2
python-numpy	1.17.3	out	/gnu/store/aawl4k64qq5mwczi2ndaap3pgbqlb1pd-python-numpy-1.17.3
python-mpi4py	3.0.3	out	/gnu/store/5sa7617f82xy12yhzg0rihmcyyvn34zh-python-mpi4py-3.0.3
python	3.8.2	out	/gnu/store/kmfg2lq2aqgmdrr16hk7nc878aafpqhn-python-3.8.2
python-matplotlib	3.1.2	out	/gnu/store/gsjhzr1xy96hz8wvaa8mblvv2niqnkwy-python-matplotlib-3.1.2
python-scipy	1.3.2	out	/gnu/store/hyjjyl5kk2jwfiasnqzw4f9fhq3igy8a-python-scipy-1.3.2
/usr/bin/mpirun

-------------------------------------------------------
Primary job  terminated normally, but 1 process returned
a non-zero exit code.. Per user-direction, the job has been aborted.
-------------------------------------------------------
--------------------------------------------------------------------------
mpirun detected that one or more processes exited with non-zero status, thus causing
the job to be terminated. The first process to do so was:

  Process name: [[65163,1],0]
  Exit code:    1
--------------------------------------------------------------------------

> cat OAR.logging.11735726.stderr
Traceback (most recent call last):
  File "./test.py", line 4, in <module>
Traceback (most recent call last):
    from obspy.core import read,UTCDateTime,Trace
  File "/home/lecointre/.guix-profile/lib/python3.8/site-packages/obspy/__init__.py", line 39, in <module>
  File "./test.py", line 4, in <module>
    from obspy.core import read,UTCDateTime,Trace
  File "/home/lecointre/.guix-profile/lib/python3.8/site-packages/obspy/__init__.py", line 39, in <module>
    from obspy.core.utcdatetime import UTCDateTime  # NOQA
  File "/home/lecointre/.guix-profile/lib/python3.8/site-packages/obspy/core/__init__.py", line 124, in <module>
    from obspy.core.utcdatetime import UTCDateTime  # NOQA
  File "/home/lecointre/.guix-profile/lib/python3.8/site-packages/obspy/core/__init__.py", line 124, in <module>
    from obspy.core.utcdatetime import UTCDateTime  # NOQA
  File "/home/lecointre/.guix-profile/lib/python3.8/site-packages/obspy/core/utcdatetime.py", line 25, in <module>
    from obspy.core.utcdatetime import UTCDateTime  # NOQA
  File "/home/lecointre/.guix-profile/lib/python3.8/site-packages/obspy/core/utcdatetime.py", line 25, in <module>
    import numpy as np
  File "/home/lecointre/.guix-profile/lib/python3.8/site-packages/numpy/__init__.py", line 142, in <module>
    import numpy as np
  File "/home/lecointre/.guix-profile/lib/python3.8/site-packages/numpy/__init__.py", line 142, in <module>
    from . import core
  File "/home/lecointre/.guix-profile/lib/python3.8/site-packages/numpy/core/__init__.py", line 47, in <module>
    from . import core
  File "/home/lecointre/.guix-profile/lib/python3.8/site-packages/numpy/core/__init__.py", line 47, in <module>
    raise ImportError(msg)
ImportError: 

IMPORTANT: PLEASE READ THIS FOR ADVICE ON HOW TO SOLVE THIS ISSUE!

Importing the numpy c-extensions failed.
- Try uninstalling and reinstalling numpy.
- If you have already done that, then:
  1. Check that you expected to use Python2.7 from "/usr/bin/python",
     and that you have no directories in your PATH or PYTHONPATH that can
     interfere with the Python and numpy version "1.17.3" you're trying to use.
  2. If (1) looks fine, you can open a new issue at
     https://github.com/numpy/numpy/issues.  Please include details on:
     - how you installed Python
     - how you installed numpy
     - your operating system
     - whether or not you have multiple versions of Python installed
     - if you built from source, your compiler versions and ideally a build log

- If you're working with a numpy git repository, try `git clean -xdf`
  (removes all files not under version control) and rebuild numpy.

Note: this error has many possible causes, so please don't comment on
an existing issue about this - open a new one instead.

Original error was: No module named _multiarray_umath

    raise ImportError(msg)
ImportError: 

IMPORTANT: PLEASE READ THIS FOR ADVICE ON HOW TO SOLVE THIS ISSUE!

Importing the numpy c-extensions failed.
- Try uninstalling and reinstalling numpy.
- If you have already done that, then:
  1. Check that you expected to use Python2.7 from "/usr/bin/python",
     and that you have no directories in your PATH or PYTHONPATH that can
     interfere with the Python and numpy version "1.17.3" you're trying to use.
  2. If (1) looks fine, you can open a new issue at
     https://github.com/numpy/numpy/issues.  Please include details on:
     - how you installed Python
     - how you installed numpy
     - your operating system
     - whether or not you have multiple versions of Python installed
     - if you built from source, your compiler versions and ideally a build log

- If you're working with a numpy git repository, try `git clean -xdf`
  (removes all files not under version control) and rebuild numpy.

Note: this error has many possible causes, so please don't comment on
an existing issue about this - open a new one instead.

Original error was: No module named _multiarray_umath
```




```
> guix install openmpi

> guix package -I
python-h5py-mpi	2.10.0	out	/gnu/store/77dr5hz60al5ff3jw51faihfwl81ki5y-python-h5py-mpi-2.10.0
python-obspy	1.2.2	out	/gnu/store/wayw9zp9s2krg2kj9hxia9glni511z33-python-obspy-1.2.2
python-numpy	1.17.3	out	/gnu/store/aawl4k64qq5mwczi2ndaap3pgbqlb1pd-python-numpy-1.17.3
python-mpi4py	3.0.3	out	/gnu/store/5sa7617f82xy12yhzg0rihmcyyvn34zh-python-mpi4py-3.0.3
python	3.8.2	out	/gnu/store/kmfg2lq2aqgmdrr16hk7nc878aafpqhn-python-3.8.2
python-matplotlib	3.1.2	out	/gnu/store/gsjhzr1xy96hz8wvaa8mblvv2niqnkwy-python-matplotlib-3.1.2
python-scipy	1.3.2	out	/gnu/store/hyjjyl5kk2jwfiasnqzw4f9fhq3igy8a-python-scipy-1.3.2
openmpi	4.0.3	out	/gnu/store/gp29sz8pr3fw8f00mparz29wl4l5nh9g-openmpi-4.0.3

> which mpirun
/home/lecointre/.guix-profile//bin/mpirun
```

remove openmpi puis reinstall...

```
> guix install openmpi
guix install: warning: Consider running 'guix pull' followed by
'guix package -u' to get up-to-date packages and security updates.

The following package will be installed:
   openmpi 4.0.3

hint: Consider setting the necessary environment variables by running:

     GUIX_PROFILE="/home/lecointre/.guix-profile"
     . "$GUIX_PROFILE/etc/profile"

Alternately, see `guix package --search-paths -p "/home/lecointre/.guix-profile"'.

> GUIX_PROFILE="/home/lecointre/.config/guix/current"
> . "$GUIX_PROFILE/etc/profile"
> which mpirun
/home/lecointre/.guix-profile//bin/mpirun
> which guix
/home/lecointre/.config/guix/current/bin/guix
> echo $PATH
/home/lecointre/.config/guix/current/bin:/home/lecointre/.guix-profile//bin:/home/lecointre/.guix-profile//sbin:/gnu/store/2m5zblmszcxk331vpjm2zqw4psxdl6fx-profile/bin:/nix/var/nix/profiles/default/bin:/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games
> echo $PYTHONPATH
/home/lecointre/.guix-profile//lib/python3.8/site-packages
> guix package -I
python-h5py-mpi	2.10.0	out	/gnu/store/77dr5hz60al5ff3jw51faihfwl81ki5y-python-h5py-mpi-2.10.0
python-obspy	1.2.2	out	/gnu/store/wayw9zp9s2krg2kj9hxia9glni511z33-python-obspy-1.2.2
python-numpy	1.17.3	out	/gnu/store/aawl4k64qq5mwczi2ndaap3pgbqlb1pd-python-numpy-1.17.3
python-mpi4py	3.0.3	out	/gnu/store/5sa7617f82xy12yhzg0rihmcyyvn34zh-python-mpi4py-3.0.3
python	3.8.2	out	/gnu/store/kmfg2lq2aqgmdrr16hk7nc878aafpqhn-python-3.8.2
python-matplotlib	3.1.2	out	/gnu/store/gsjhzr1xy96hz8wvaa8mblvv2niqnkwy-python-matplotlib-3.1.2
python-scipy	1.3.2	out	/gnu/store/hyjjyl5kk2jwfiasnqzw4f9fhq3igy8a-python-scipy-1.3.2
openmpi	4.0.3	out	/gnu/store/gp29sz8pr3fw8f00mparz29wl4l5nh9g-openmpi-4.0.3
```

script python a tester sur 2 processus MPI sur la frontale : a noter le chgmt python -> python3 dans l'entete
```
>  cat test.py 
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from obspy.core import read,UTCDateTime,Trace
from mpi4py import MPI
import h5py
import numpy as np
if __name__ == '__main__':
    n_process = MPI.COMM_WORLD.size
        rank = MPI.COMM_WORLD.rank

> mpirun -n 2 ./test.py 
>
```

OK je n'ai pas d'erreur sur la frontale.

J'essaie ce même script sur 2 core de 2 nodes différents : avec ce script de soumission

[...]

en mode multinode, on rencontre plusierus difficultes :

1) `WARNING: There was an error initializing an OpenFabrics device` : solution ajouter ceci au mpirun : `--mca btl '^openib'` ou plutot `-mca btl_openib_allow_ib 1` 

2) `orted: Error: unknown option "--tree-spawn"` : solution ajouter ceci au mpirun : `--prefix $GUIX_PROFILE` (et pourquoi pas `--prefix $GUIX_USER_PROFILE_DIR/monprofil` ?)

3) `ModuleNotFoundError: No module named 'obspy'` (puis pareil avec MPI4Py si on commente le `import obspy`, puis ...) : solution sourcer guix-start.sh dans bashrc (le sourcer dans le script de soumission ne suffit pas).


A noter que si on applique la solution 3), le `--prefix $GUIX_PROFILE` n'est plus requis (l'erreur 2 disparait).

Au final je continue avec le source guix-start.sh en dur dans mon bashrc, et avec ce script de soumission :

```
#!/bin/bash
#OAR --project iworms
#OAR -l /nodes=1/cpu=1/core=2,walltime=00:01:00
#OAR -n logging
#OAR -t devel
set -e
cat $OAR_NODE_FILE
nproc=$(cat $OAR_NODE_FILE | wc -l)
source /applis/site/guix-start.sh
mpirun --machinefile $OAR_NODE_FILE --mca btl '^openib' --mca orte_rsh_agent "oarsh" -n $nproc ./test.py
```

... et j'essaie d'étoffer le test.py

---------------------

ca marche

j'ai juste retiré OGS1 HHN qui refusait de s'interpol 200Hz -> 100Hz

reste une liste de 235 traces
```
235 list_rsync.txt
```

```
2 proc MPI sur deux noeuds distincts : 12191005
 * startTime = 2020-09-25 10:45:33
 * stopTime = 2020-09-25 10:54:05


2 proc MPI sur 2 nodes, pareil mais ecriture gzip force 6 :
avec dset.collective et conversion int8 a l'ecriture
12192457
freeze tout a la fermeture du fichier...


2 proc MPI sur deux noeuds distincts, pas compressé, mais force i8 a l'ecriture :
12196152: 521.458285765 sec

32 procs sur 2 noeuds : 2*16 (1cpu)
12197265: 74.902662742 sec

64 procs sur 2 noeuds :  
12197456: 
obspy.clients.fdsn.header.FDSNRedirectException: Requests with credentials (username, password) are not being redirected by default to improve security. To force redirects and if you trust the data center, set `force_redirect` to True when initializing the Client.

change force_redirect=True a l'init du Client
client = Client("RESIF",user=USER,password=PASS, force_redirect=True)

64 procs sur 2 noeuds :
12199413
obspy.clients.fdsn.header.FDSNException: No FDSN services could be discovered at 'http://ws.resif.fr'. This could be due to a temporary service outage or an invalid FDSN service address.

service mapping:
client = Client(service_mappings={'dataselect': 'http://ws.resif.fr/fdsnws/dataselect/1', 'station':  'http://ws.resif.fr/fdsnws/station/1'},user=USER,password=PASS)
12200641
ElapsedTime: 27.997937139 sec

zut, pb de reproductibilite...

rejoue 12200641 a l'id :
12203194 : output id

rejoue 32 procs sur 2 noeuds avec service_mappings
12203454 : id 12203194,12200641

rejoue 32 procs sur 2 noeuds avec force_redirect
12203651 :  diff, en fait les service_mappings ne telechargent rien, ca fait que des traces nulles

j'avais omis le base_url, je reviens à ceci ;
client = Client(base_URL='RESIF', service_mappings={'dataselect': 'http://ws.resif.fr/fdsnws/dataselect/1', 'station':  'http://ws.resif.fr/fdsnws/station/1'},user=USER,password=PASS)
```



####################################################################

on verif la reproductibilite avec 1 seul proc dans les 3 cas

```
1 seul proc, service_mappings : 12205407
grep "This trace won' be processed" OAR.logging.12205407.stdout|wc -l
20
1 seul proc, force_redirect : 12205935 (output id a 12205407) 
grep "This trace won' be processed" OAR.logging.12205935.stdout | wc -l
20
1 seul proc, rien : 12209088 (output id a 12205407)
grep "This trace won' be processed" OAR.logging.12209088.stdout |wc -l20
20





96 procs sur 3 noeuds :
to be continued...
```

-->
