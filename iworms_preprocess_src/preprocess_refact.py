#!/usr/bin/env python3
# -*- coding: utf-8 -*-

###############################################################################
#    This file is part of iworms-preprocess : a python code for downloading   #
#    seismic traces from RESIF datacent, process them, and write them as      #
#    HDF5 datasets.                                                           #
#    The code uses MPI parallelization over the seimic traces dimension.      #
#    The goal was to benchmark the RESIF datacenter capability to distribute  #
#    large amout of data                                                      #
#                                                                             #
#    Copyright (C) 2019 Albanne Lecointre, Philippe Roux, Christophe Picard,  #
#                       Pierre-Antoine Bouttier, Violaine Louvet              #
#    albanne.lecointre@univ-grenoble-alpes.fr                                 #
#                                                                             #
#    This program is free software: you can redistribute it and/or modify     #
#    it under the terms of the GNU General Public License as published by     #
#    the Free Software Foundation, either version 3 of the License, or        #
#    (at your option) any later version.                                      #
#                                                                             #
#    This program is distributed in the hope that it will be useful,          #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of           #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
#    GNU General Public License for more details.                             #
#                                                                             #
#    You should have received a copy of the GNU General Public License        #
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.   #
###############################################################################

#  =======================================================================================
#                       ***  Python script iWORMS_preprocess.py  ***
#   This is the main Python script to preprocess RESIF MSEED data
#
#  =======================================================================================
#   History :  1.0  : 05/2017  : A. Lecointre : Initial Python version
#
#  ---------------------------------------------------------------------------------------
#
#  ---------------------------------------------------------------------------------------
#   methodology   : cleaning J-1, J, J+1
#                 : interpolate 100Hz (if necessary)
#                 : interpolate onto a fixed time vector (starttime.microsecond should be
#                   XX0000)
#                 : cut between 00:00:00.000000 and 23:59:59.990000
#                 : highpass filter 0.05Hz
#                 : gapfilling : fill remaning gap with zero [TODO: and keep info about
#                   gap position]
#                 : bandpass filter : 0.2 - 0.3333Hz
#                 : decimation factor 10 (no spline interp)
#                 : whitening : 0.2 - 0.3333Hz
#                 : check for NaN or Inf
#                 : 1-Bit
#                 : Int8 encoding
#                 : write output as a dataset into HDF5 daily file with gzip compression
#
#  https://ciment.ujf-grenoble.fr/wiki/index.php/Projects/pr-iworms#Preprocessing_des_donn.C3.A9es
#
#  ---------------------------------------------------------------------------------------

from obspy.core import read,UTCDateTime,Trace
from mpi4py import MPI
import h5py
import numpy as np
import sys
import whitenWhisper

import time
import socket

#import StringIO
#from memory_profiler import profile

# fp = open('/home/bouttier/iworms_refact/memory_profile.log','w+')

## @profile(stream=fp)
def get_input(syr, sjd, inet, ista, iloc, ichan, n):
    empty_trace = False
    # Read stream from input files from input stream from wget -O -
    #s = read('tmp.mseed')
    # try:
        # s = read(StringIO.StringIO(sys.stdin.read()))
	# s = read("tmp.mseed")
        # return s
    # except Exception as e:
        # print e
        # sys.exit(0)  # cigri should not abort
    from obspy.clients.fdsn import Client
    import configparser
    config=configparser.ConfigParser()
    # import getpass
    # USERNAME=getpass.getuser()
    # CONFIG_FILE="/home/"+USERNAME+"/.iworms.conf"
    # print(CONFIG_FILE)
    import os
    CONFIG_FILE = os.environ['HOME'] + '/.iworms.conf'
    os.path.isfile(CONFIG_FILE)
    config.read(CONFIG_FILE)
    USER=config.get('global','user')
    PASS=config.get('global','password')
    client = Client("RESIF",user=USER,password=PASS)
    #client = Client("RESIF",user=USER,password=PASS, force_redirect=True)
    #client = Client(base_url='RESIF', service_mappings={'dataselect': 'http://ws.resif.fr/fdsnws/dataselect/1', 'station':  'http://ws.resif.fr/fdsnws/station/1'},user=USER,password=PASS)
    t = UTCDateTime(syr+sjd+"T00:00:00.000")
    print("client.get_waveforms",inet, ista, iloc, ichan, t - 60, t + 86400 + 60)
    flog.write(" "+USER+" "+inet+" "+ista+" "+iloc+" "+ichan+" "+(t-60).strftime("%Y-%m-%d+%H:%M:%SZ ")+(t+86400+60).strftime("%Y-%m-%d+%H:%M:%SZ "))
    try:
                 #s = client.get_waveforms(inet, ista, iloc, ichan, t - 60, t + 86400 + 60, quality='M') # get from J-1min to J+1min to prevent bad time        vector...
        s = client.get_waveforms(inet, ista, iloc, ichan, t - 60, t + 86400 + 60) # get from J-1min to J+1min to prevent bad time vector...
    except Exception as e:
        print(e)
        print('This trace won\' be processed')
        empty_trace = True  # cigri should not abort
        s = np.zeros(n)
        pass
    return s, empty_trace

## @profile(stream=fp)
def interpol(s, yr, jd, ff =100.):
    #print("s.count()",s.count())
    #print(" ")
    for itr in range(s.count()): # range(s.count()-1,s.count()): #range(s.count()):
      #print(" ")
      #print("itr",itr)
      #print("s[itr].stats",s[itr].stats)
      ts = s[itr].stats.starttime
      # check if 1e6*(1./ff) divides microsec or not
      if (ts.microsecond % (1e6*(1./ff))) == 0:
        nts = ts
      else:  # on va a la freq 100Hz suivante
        microsec_to_add = (1e6*(1./ff)) - (ts.microsecond % (1e6*(1./ff)))
        nts = ts + 1e-6*microsec_to_add
      #print(itr)
      #print("s",s)
      #print("ts",ts)
      #print("nts",nts)
      #print(s[itr].data)
      #print(np.max(s[itr].data),np.min(s[itr].data),np.mean(s[itr].data))
      #s[itr].interpolate(sampling_rate=ff,starttime=nts,method='nearest')
      s[itr].interpolate(sampling_rate=ff,starttime=nts,method='lanczos',a=5)
      #s[itr].interpolate(sampling_rate=ff,starttime=nts,method='linear')
      #print("s[itr].stats after interpol",s[itr].stats)
      #s[itr].interpolate(sampling_rate=ff,method='lanczos',a=5)
    #print(s)
    #print(s[itr].data)
    #  # check if microsecond = XX0000 or not (last four digit should be zero)
    #  if 10000*int(ts.microsecond/10000.) == ts.microsecond:  # pas de decalage
    #    nts = ts
    #  else:
    #    nts = UTCDateTime(year=ts.year, julday=ts.julday, hour=ts.hour, minute=ts.minute, second=ts.second, microsecond=10000*(int(ts.microsecond/10000.)+1))
    #  print ts,nts
    # interpolate to 100Hz and force to starttime.microsecond=XX0000
    # s[itr].interpolate(sampling_rate=100.,starttime=nts,method='weighted_average_slopes') # -> parfois last sample becomes nan ???

    # cut the trace(s) between 00:00:00.000000 and 23:59:59.990000
    s.trim(starttime=UTCDateTime(year=yr, julday=jd),endtime=UTCDateTime(year=yr, julday=jd)+86400-s[0].stats.delta)

    # stop if there is no stream left
    if ( s.count() == 0 ):
        print('no data left after trim')
        sys.exit(0) # cigri should not abort

    ################################################################
    # Highpass Filter 0.05Hz
    ################################################################
    # s.filter("highpass", freq=0.05)
    return s

## @profile(stream=fp)
def fill_with_zero(s, yr, jd):
    ################################################################
    # Gap filling : fill with zero
    ################################################################

    net = s[0].stats.network
    sta = s[0].stats.station
    loc = s[0].stats.location
    channel = s[0].stats.channel

    datatype = s[0].data.dtype

    meta = {'station': sta, 'location': loc, 'network': net, 'channel': channel}

    stend = s[-1].stats.endtime

    trb = Trace(data=np.zeros(1,dtype=datatype),header=meta)
    trb.stats.delta = s[0].stats.delta
    trb.stats.starttime = UTCDateTime(str(yr)+"{:03d}".format(jd))
    if s[0].stats.starttime != trb.stats.starttime:
            s.append(trb)

    tre = Trace(data=np.zeros(1,dtype=datatype),header=meta)
    tre.stats.delta = s[0].stats.delta
    tre.stats.starttime = UTCDateTime(str(yr)+"{:03d}".format(jd))+86400-s[0].stats.delta
    if stend != tre.stats.endtime:
            s.append(tre)

    s.merge(method=0, fill_value=0)
    return s

## @profile(stream=fp)
def final_treatment(s, fb1 =0.1, fb2 =0.2):
    ################################################################
    # Bandpass filter : 0.2 - 0.3333 Hz
    ################################################################
    s.filter("bandpass", freqmin=fb1, freqmax=fb2, corners=4, zerophase=True)

    ################################################################
    # Decimation factor 10 using spline interpolation, update the channel name
    ################################################################

    #tck = interpolate.splrep(np.arange(0, s[0].stats.npts, 1), s[0].data, s=0)
    #s[0].data = interpolate.splev(np.arange(0, s[0].stats.npts , 10), tck, der=0)
    #s[0].stats.sampling_rate=s[0].stats.sampling_rate/10
    ##channel = 'EPZ'   # extremely short period = 100Hz and corner period of the geophone = 0.1sec
    ##s[0].stats.channel = channel

    ################################################################
    # Decimation factor 10: keep one sample/10
    ################################################################

    s.decimate(factor=10, strict_length=False, no_filter=True)

    ################################################################
    # Whitening
    ################################################################

    freq = s[0].stats.sampling_rate # 100.0
    DivideFreq = 20.0
    s[0].data = whitenWhisper.WhiteningCos(s[0].data, fb1, fb2, freq, DivideFreq)

    ################################################################
    # 1-Bit
    ################################################################

    if np.size(np.where(np.isnan(s[0].data)))!=0:
        print('Presence de NaN dans data avant 1bit')
        print(np.where(np.isnan(s[0].data)))
        sys.exit(1)
    if np.size(np.where(np.isinf(s[0].data)))!=0:
        print('Presence de +/-Inf dans data avant 1bit')
        print(np.where(np.isnan(s[0].data)))
        sys.exit(1)

    s[0].data = np.sign(s[0].data)
    return s

## @profile(stream=fp)
def write_output(s, yr, jd):
    ################################################################
    # Writing dataset into HDF5 output file
    ################################################################
    h5file = h5py.File('./'+'{:>04}'.format(yr)+'.'+'{:>03}'.format(jd)+'.trace.int8.h5','a')
    net = s[0].stats.network
    sta = s[0].stats.station
    loc = s[0].stats.location
    channel = s[0].stats.channel
    ## replace channel name HH[ENZ] by BH[ENZ] with respect to channel naming convention
    #channel = "B" + channel[1:]
    arrname = net+"."+sta+"."+loc+"."+channel  # +".D."+cyr+"."+cjd
    dset = h5file.create_dataset(arrname, (864000,), dtype='int8', compression="gzip", compression_opts=6, fletcher32='True')
    dset[:] = s[0].data.astype('int8')
    h5file.close()

## @profile(stream=fp)
def main(syr, sjd, inet, ista, iloc, ichan):
    yr = int(syr)
    jd = int(sjd)
    s, empty_trace = get_input(syr, sjd, inet, ista, iloc, ichan)
    s = interpol(s, yr, jd)
    s = fill_with_zero(s, yr, jd)
    s = final_treatment(s)
    return s

def read_list_traces(path):
    f = open(path, 'r')
    lines = f.readlines()
    f.close()
    net = []
    sta = []
    loc = []
    channel = []
    for l in lines:
        splitline = l.split(' ')
        sta.append(splitline[1])
        net.append(splitline[0])
        loc.append(splitline[2])
        channel.append(splitline[3].replace('\n',' '))
    return net, sta, loc, channel


if __name__ == '__main__':
    # MPI info
    n_process = MPI.COMM_WORLD.size
    rank = MPI.COMM_WORLD.rank
    # n is the size of 1d-array datas for one trace
    net, sta, loc, channel = read_list_traces('list_rsync.txt')
    n = 864000
    n_traces = len(net)
    # TODO: take in input the path of the output
    f = h5py.File('./parallel_traces.hdf5', 'w', driver='mpio', comm=MPI.COMM_WORLD)
    dset = f.create_dataset('trace', (n_traces, n), dtype='i')
    #dset = f.create_dataset('trace', (n_traces, n), dtype='i', compression="gzip", compression_opts=6)
### fletcher32='True', shuffle ? inutile il faudrait bitshuffle...)
    # TODO : take in input parameter for select wanted traces
    syr = "2017"
    sjd = "179"

    flog = open('LOG/log'+'{:04d}'.format(rank)+'.txt', 'w')
##logline example
## date_requete node RESIFuserID network station location_code channel_code starttime endtime nb_samples elapsed_time
##2018-12-07+09:27:34Z luke4 lecointrea FR ARBF 00 HHE 2017-01-10+23:59:00Z 2017-01-12+00:01Z 10815412 14.322721206
#
#
    for i in range(n_traces):
        if i % n_process == rank:
            start_time = time.time()
            logline = time.strftime("%Y-%m-%d+%H:%M:%SZ ")
            logline = logline + socket.gethostname()
            flog.write(logline)
            s, empty_trace = get_input(syr, sjd, net[i], sta[i], loc[i], channel[i], n)
            totnpts=0
            if not(empty_trace):
                for itr in range(s.count()):
                    totnpts=totnpts+s[itr].stats.npts
                s = interpol(s, int(syr), int(sjd))
                s = fill_with_zero(s, int(syr), int(sjd))
                s = final_treatment(s)
                #with dset.collective:
                dset[i] = s[0].data.astype(dtype='i')
            else:
                #with dset.collective:
                dset[i] = s.astype(dtype='i')
            flog.write(str(totnpts)+" "+'{:f}'.format(time.time() - start_time)+"\n")

    flog.close()

    f.close()
