#!/bin/bash
#OAR --project iworms
##OAR -l /nodes=2/cpu=1/core=1,walltime=00:02:00
##OAR -l /nodes=1/cpu=1/core=1,walltime=00:30:00
#OAR -l /nodes=2,walltime=00:30:00
#OAR -n logging
#OAR -t devel

set -e

cat $OAR_NODE_FILE
nproc=$(cat $OAR_NODE_FILE | wc -l)

source /applis/site/guix-start.sh
MYGUIXPROFILE=prepobspy
refresh_guix $MYGUIXPROFILE
export MPIPREFIX=$GUIX_USER_PROFILE_DIR/$MYGUIXPROFILE   # useful in multinodes

# Here we suppose that prepobspy is a GUIX profile which contains:
# openmpi      	4.1.1 	out	/gnu/store/6gqpzdghvm6bmnhqxjnlyamx6fa5a2a6-openmpi-4.1.1
# python-mpi4py	3.0.3 	out	/gnu/store/jc6q1hywlwrmdpp4scvvdr0zz8s26zxy-python-mpi4py-3.0.3
# python-h5py-mpi	2.10.0	out	/gnu/store/1z823q6bl61r3jmkblld0pzh4v9x92hh-python-h5py-mpi-2.10.0
# python-obspy 	1.2.2 	out	/gnu/store/5nvw0869xvg7lsirp76pgkdwx2s4jmpx-python-obspy-1.2.2
# python       	3.9.9 	out	/gnu/store/sz7lkmic6qrhfblrhaqaw0fgc4s9n5s3-python-3.9.9

guix package -I
which guix
which mpirun
which python3
echo $LD_LIBRARY_PATH
echo $PATH
echo $PYTHONPATH
echo $GUIX_PROFILE




PEXE="preprocess_refact.py"

TMPDIR="/$SHARED_SCRATCH_DIR/$USER/oar.$OAR_JOB_ID"
mkdir -p $TMPDIR

cp $PEXE $TMPDIR/.
cp whitenWhisper.py $TMPDIR/.
cp list_rsync.txt $TMPDIR/.

cd $TMPDIR

mkdir LOG


#mpirun --machinefile $OAR_NODE_FILE --mca orte_rsh_agent "oarsh" --mca btl_openib_allow_ib "true" -n $nproc ./test.py
#mpirun -x LD_LIBRARY_PATH --machinefile $OAR_NODE_FILE --mca orte_rsh_agent "oarsh" --mca btl '^openib' -n $nproc ./test.py
#mpirun --machinefile $OAR_NODE_FILE --mca orte_base_help_aggregate 0 --mca orte_rsh_agent "oarsh" --prefix $GUIX_PROFILE -n $nproc ./test.py
#mpirun --machinefile $OAR_NODE_FILE --mca btl '^openib'  --mca orte_rsh_agent "oarsh" --prefix $GUIX_PROFILE -n $nproc "source /applis/site/guix-start.sh ; ./test.py"

START=$(date +%s.%N)

# In multinodes : 2 solutions : 
# 1)
# Add "source /applis/site/guix-start.sh" and "refresh_guix prepobspy" in your ~/.bashrc
# mpirun --machinefile $OAR_NODE_FILE --mca btl_openib_allow_ib "true" --mca plm_rsh_agent "oarsh" --prefix $MPIPREFIX -n $nproc ./$PEXE
# OR 2) pas de modif dans bashrc
NEWPYTHONPATH=`guix package -p $GUIX_USER_PROFILE_DIR/$MYGUIXPROFILE --search-paths | grep PYTHONPATH |awk -F= '{print $2}' |sed -e "s/\"//g"`
NEWLIBRARY_PATH=`guix package -p $GUIX_USER_PROFILE_DIR/$MYGUIXPROFILE --search-paths | grep LIBRARY_PATH |awk -F= '{print $2}' |sed -e "s/\"//g"` 
mpirun --machinefile $OAR_NODE_FILE --mca btl_openib_allow_ib "true" --mca plm_rsh_agent "oarsh" --prefix $MPIPREFIX -x PYTHONPATH="$NEWPYTHONPATH" -x LIBRARY_PATH="$NEWLIBRARY_PATH"  --mca mpi_warn_on_fork 0   -n $nproc ./$PEXE
#
#


# --mca mpi_warn_on_fork 0 : warning à l'import d'h5py-mpi avec openmpi-4.1.1

date
END=$(date +%s.%N)
DIFF=$(echo "$END - $START" | bc)
echo "ElapsedTime: $DIFF sec"

sync

echo "Done"
