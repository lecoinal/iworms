#!/bin/bash
#OAR -l /nodes=1/cpu=1/core=1,walltime=48:00:00
#OAR --project iste-equ-ondes
#OAR -n iworms

#################

set -e

ii=$1

for sta in $(cat /data/projects/jacinto/GRID/name.txt | head -$((ii*100)) | tail -100) ; do 
  echo "sta : $sta : $(date)"
  /soft/miniseed/bin/msi -tg /data/projects/jacinto/db/2014/1??/${sta}*
done

