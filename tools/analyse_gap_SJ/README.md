Un petit script pour analyser les gaps presents dans les 1108 stations San Jacinto:

Un job pour analyser 100 nodes : 

```
$ for ii in $(seq 1 11) ; do oarsub -S ./analyse_gap_SJ.sh $ii" ; done
$ oarsub -S ./analyse_gap_SJ_fin.sh
```

Les résultats sont dans les stdout joints.

On observe que 1066 nodes / 1108 connaissent au moins un gap

Il y a la période 139-142, au cours de laquelle 1053 stations rencontrent un gap, d'une durée moyenne de 0.9h, médiane de 0.6h, min de 1.2sec max de 23.8h.

Il reste le cas des 1108 - 1053 = 55 stations qui n'ont pas eu cette interrution pour remplacement de batteries, j'ai l'impression que ce sont soit des stations qui n'ont pas redémarré après la coupure (donc je ne le vois pas comme un gap), soit des stations qui ont été démarrées en fin d'expérience, elles n'existaient pas encore sur la période 139-142.

Et il y a l'ensemble de la période 130-156, au cours de laquelle 5.8 nodes / jour en moyenne connaissent un gap d'un jour, de minuit à minuit (autrement dit un fichier mseed journalier manquant). Ca concerne au maximum 12 stations par jour. Ces gaps journaliers n'ont pas été bouchés.




