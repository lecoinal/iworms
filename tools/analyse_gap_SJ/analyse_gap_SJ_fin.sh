#!/bin/bash
#OAR -l /nodes=1/cpu=1/core=1,walltime=48:00:00
#OAR --project iste-equ-ondes
#OAR -n iworms

#################

set -e

for sta in $(cat /data/projects/jacinto/GRID/name.txt | tail -8) ; do 
  echo "sta : $sta : $(date)"
  /soft/miniseed/bin/msi -tg /data/projects/jacinto/db/2014/1??/${sta}*
done

