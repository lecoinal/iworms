#!/bin/bash
#OAR --project iworms
##OAR --project iste-equ-ondes
##OAR -l /nodes=1/cpu=1/core=4,walltime=1:00:00
#OAR -l /nodes=1,walltime=5:00:00
#OAR -n measure_dt

set -e

# CIMENT GRID Clusters
#source /applis/site/nix.sh
source /applis/site/guix-start.sh
refresh_guix measuredt

# refresh_guix iworms_mpi
# guix package -i openmpi python@3 python-numpy python-mpi4py python-h5py hdf5

AVGWIN=$1   # 18 (3h), 12 (2h), 6 (1h), 3 (30min), 1 (10min)
EXP=$2  # "EXP020"
FC=$3   # "6.0"

#ISTERRE Clusters
#source /soft/env.bash

date

START=$(date +%s.%N)

cat $OAR_NODE_FILE

nbprocs=$(cat $OAR_NODE_FILE | wc -l)

TMPDIR="$SHARED_SCRATCH_DIR/$USER/oar.$OAR_JOB_ID/"

PEXE=measure_dt_compute_Dt.py

mkdir -p $TMPDIR
cp $PEXE $TMPDIR/. 
cd $TMPDIR

echo $TMPDIR

####ln -s /bettik/lecoinal/IWORMS/out_N100.h5 in.h5
####ln -s /bettik/lecoinal/IWORMS/out_N5.h5 in.h5
###ln -s /bettik/lecoinal/IWORMS/out.h5 in.h5
####h5ls in.h5 | awk '{print $1 " " NR-1}' > list_pair.txt
###awk '{print $1 " " NR-1}' ../oar.39149898/list_pair.txt > list_pair.txt

# For IWORMS SanJacinto dataset
#ln -s /bettik/lecoinal/IWORMS/out.h5 in.h5
#awk '{print $1 "_EPZ_EPZ_corr " NR-1}' /bettik/lecoinal/IWORMS/SJ_132_150_923/list_pair.txt > list_pair.txt
#nbpair=$(wc -l list_pair.txt | awk '{print $1}')
#STAREF="R3010"
#STAREF="R0102"

# For resolve argentiere dataset
ln -s /bettik/lecoinal/RESOLVE/CORRELATIONS/${EXP}/10MINCAT/ZO.2018115.2018147.h5 in.h5
h5ls in.h5 > zzzzz
awk '{print $1 " " NR-1}' zzzzz > list_pair.txt
nbpair=$(wc -l list_pair.txt | awk '{print $1}')
STAREF="AR057"



# get the 0based index of this staref
istaref=$(grep -n -m 1 $STAREF list_pair.txt|awk -F: '{print $1}')
if [ "$istaref" == "1" ] ; then
  # perhaps it should be 0 
  if [ "$(head -1 list_pair.txt | awk -F_ '{print $1}')" == "$STAREF" ] ; then
    istaref=0
  fi
fi

echo $STAREF $istaref

case "$CLUSTER_NAME" in
  luke)
    ### suppose that we have already install python3 with h5py and matplotlib: nix-env -if python3-env.nix
    # we suppose that we have already prepared measuredt guix environment with manifest scm file
    ;;
  ISTERRE)
    PYTHONMODULE="python/python3.5"
    IDIR="/media/luke4_scratch_md1200/lecointre/"
    module load ${PYTHONMODULE}
    ;;
esac

# /usr/bin/time -v   python3 -u $PEXE -n $nbpair -istaref $istaref
#/usr/bin/time -v mpiexec -n ${nbprocs} python3 -u $PEXE -n $nbpair -istaref $istaref -avgwin $AVGWIN

# For ZO Argentiere dataset : 2001 samplaes in corr as -2:2sec with Fc=500Hz
# 144*33days = 4752 timesteps
# 4-8Hz -> Fc 6Hz
/usr/bin/time -v mpiexec -n ${nbprocs} python3 -u $PEXE -n $nbpair -istaref $istaref -avgwin $AVGWIN -itb 500 -ite 1501 -t 501 -s 4752 -fs 500 -f $FC

#mv out.h5 out_$JD.h5
#
#mv $TMPDIR /$SHARED_SCRATCH_DIR/$USER/$CIGRI_CAMPAIGN_ID/.
#
date
END=$(date +%s.%N)
DIFF=$(echo "$END - $START" | bc)
echo "ElapsedTime: $DIFF sec"
