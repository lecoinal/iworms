#!/usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
#    This file is part of iworms-measuredt: a python code for  measuring      #
#    clock differences dt between a GF(timestep) and a GF(ref) at each        #
#    timestep and for each pair of sensors (each edge of the graph)           #
#    and then computing clock errors Dt relatively to a master node           #
#    at each timestep and each sensor (each node of the graph)                #
#                                                                             #
#    Copyright (C) 2021 Albanne Lecointre, Philippe Roux, Christophe Picard,  #
#                       Pierre-Antoine Bouttier, Violaine Louvet              #
#    albanne.lecointre@univ-grenoble-alpes.fr                                 #
#                                                                             #
#    This program is free software: you can redistribute it and/or modify     #
#    it under the terms of the GNU General Public License as published by     #
#    the Free Software Foundation, either version 3 of the License, or        #
#    (at your option) any later version.                                      #
#                                                                             #
#    This program is distributed in the hope that it will be useful,          #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of           #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
#    GNU General Public License for more details.                             #
#                                                                             #
#    You should have received a copy of the GNU General Public License        #
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.   #
###############################################################################

import numpy as np
from mpi4py import MPI
import argparse
import h5py
import math
import time
import linecache
import sys
from scipy.signal import butter, lfilter

""" 
    module for measuring clock differences dt between a GF(timestep) and a GF(ref)
    at each timestep and for each pair of sensors (each edge of the graph)
    and then computing clock errors Dt relatively to a master node 
    at each timestep and each sensor (each node of the graph)

"""

def read_HFcorr(h5f, dsetname, nstep, btz, etz, revopt):
    """H5fileid, dsetname : input (strings)
       btz, etz :input (ints) : begin and end indices for lagtime (t_zoom from t_corr)
       rev option : input (boolean flag) : option for applying reverse sum of causal and acausal parts
       read a 2D corr dataset (only t_zoom lag) """
    tmp = h5f[dsetname][0:nstep,btz:etz]
##    tmp[np.isnan(tmp)] = 0.0
#    # Check for NaN values, and if any, replace by small value with random noise
#    if np.isnan(tmp).any():
#        print("Warning: NaN values in correlations for",dsetname,btz,etz)
#        dim0 = np.shape(tmp)[0]  # timestep dimension
#        for it in np.arange(dim0):
#            if np.isnan(tmp[it,:]).any():
#                print("Warning: NaN values in istep in correlations for",dsetname,it)
#                with open("nan_"+str(rank)+".txt", "a") as o:
#                    o.write(dsetname+" "+str(it)+" "+str(ipair)+"\n")
#                # Normally, tmp[it,:] should be full of NaN values
#                if np.isfinite(tmp[it,:]).any():
#                    print("Error: only some NaN values in correlations for",dsetname,it)
#                    sys.exit("Error: erratic NaN values in correlations...")
#                else:
#                    tmp[it,:] = 1e-3*(np.random.rand(etz-btz)-0.5)
    if revopt:
        dim0 = np.shape(tmp)[0]  # timestep dimension
        dim1 = int((etz-btz+1)/(revopt+1))  # corr timelag dimension
        dset = np.empty((dim0, dim1), np.float32)
        for it in np.arange(dim0):
            tmp_rev = tmp[it,:]+tmp[it,::-1]
            dset[it,:] = tmp_rev[int(np.floor(len(tmp_rev)/2)):]
    else:
        dset = tmp
    return dset

def butter_bandpass(lowcut, highcut, fs, order=5):
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    b, a = butter(order, [low, high], btype='band')
    return b, a

def butter_bandpass_filter(data, lowcut, highcut, fs, order=5):
    b, a = butter_bandpass(lowcut, highcut, fs, order=order)
    y = lfilter(b, a, data)
    return y

def compute_coherence(refGF_norm, stepGF, dt, sf):
    """ mesure de la coherence entre refGF_norm et stepGF,
        associee au decalage dt     """
    idec = int(round(-sf*dt))
    stepGF_rolled = np.roll(stepGF, idec)
    if ( max(abs(stepGF_rolled)) == 0 ) or ( max(abs(refGF_norm)) == 0 ):
        print("one or both of GF = 0 thus we apply coherence = 0")
        coherence = 0.0
    else:
        stepGF_rolled_norm = stepGF_rolled / max(abs(stepGF_rolled))
        coherence = np.corrcoef(refGF_norm, stepGF_rolled_norm)[1, 0]
    return coherence

def compute_Dt(deltatt, nbsta, iref):
    """ Calcul du Dt:
     Dt(S=j,ref=k) = (1/nbsta) * ( sum( deltat[j,:]) ) - sum ( deltat[k,:] ) )
    """
    invN = 1.0/float(nbsta)
    dt_2D = np.zeros((nbsta, nbsta), dtype=np.float32)
    sumdt = np.empty((nbsta,), dtype=np.float32)
    Dt = np.zeros((nbsta,), dtype=np.float32)
    # reorganize flattened allpairs vector dt into an antisymmetric matrix
    # diag(dt_2D) should be 0-init
    cpt = 0
    for ji in np.arange(nbsta-1):
        for jj in np.arange(ji+1, nbsta):
            dt_2D[ji, jj] = deltatt[cpt]
            dt_2D[jj, ji] = -deltatt[cpt]
            cpt += 1
    for ji in np.arange(nbsta):
        # On calcule la somme des (N-1) elements non nuls de chaque ligne
        # (dt(ji,1)+dt(ji,2)+...+dt(ji,N))
        sumdt[ji] = np.sum(dt_2D[ji, :])
    # On choisit la station k comme référence, on suppose donc Dt(k)=0
    # On itère pour toutes les k références
    for ji in np.arange(nbsta):
        # On calcule les differences de sommes 2 a 2
        Dt[ji] = invN*(sumdt[ji]-sumdt[iref])
    return Dt

def compute_Dtw(deltatt, coht, nbsta, iref):
    """ Calcul du Dtw:
     Dtw(S=j,ref=k) = sum( deltat[j,:]*coh[j,:] ) / sum( coh[j,:] without coh[j,j] )
                    - sum( deltat[k,:]*coh[k,:] ) / sum( coh[k,:] without coh[k,k] )
    """
    dt_2D = np.zeros((nbsta, nbsta), dtype=np.float32)
    co_2D = np.zeros((nbsta, nbsta), dtype=np.float32) # on met zero sur la diag des coh !
    sumdtcoh = np.empty((nbsta,), dtype=np.float32)
    sumcoh = np.empty((nbsta,), dtype=np.float32)
    invNm1 = 1.0/float(nbsta-1)
    Dt = np.zeros((nbsta,), dtype=np.float32)
    Coh = np.zeros((nbsta,), dtype=np.float32)
    # reorganize flattened allpairs vector dt into an antisymmetric matrix
    # diag(dt_2D) should be 0-init
    cpt = 0
    for ji in np.arange(nbsta-1):
        for jj in np.arange(ji+1, nbsta):
            dt_2D[ji, jj] = deltatt[cpt]
            dt_2D[jj, ji] = -deltatt[cpt]
            co_2D[ji, jj] = coht[cpt]
            co_2D[jj, ji] = coht[cpt]
            cpt += 1
    for ji in np.arange(nbsta):
            # On calcule la somme des (N-1) elements non nuls de chaque ligne
            # (dt(ji,1)+dt(ji,2)+...+dt(ji,N))
        sumdtcoh[ji] = np.dot(dt_2D[ji, :],co_2D[ji,:])
        sumcoh[ji] = np.sum(co_2D[ji,:])
        # On choisit la station k comme référence, on suppose donc Dt(k)=0
        # On itère pour toutes les k références
        #for iref in np.arange(nbref):
    for ji in np.arange(nbsta):
            # On calcule les differences de sommes 2 a 2
        Dt[ji] = (sumdtcoh[ji]/sumcoh[ji])-(sumdtcoh[iref]/sumcoh[iref])
        Coh[ji] = invNm1 * sumcoh[ji] 
    return Dt, Coh

def write_f32gz(fid, var, varname):
    """ Create and write a dataset named varname, containing
    variable var, into HDF5 file identifier fid,
    with F32 encoding, and apply gz compression filter
    """
    dset = fid.create_dataset(varname,
                              data=var,
                              dtype="float32",
                              compression="gzip")
    return dset

def create_local_topo(nit, nproc, rang):
    """ build a mapping to split nit iterations 
    into nproc MPI processes
    nit_loc: number of iteration in local processus (int)
    itstart: the starting point for local processus in global array (int)
    """
    nit_loc = int(nit / nproc)
    if rang < (nit % nproc):
        nit_loc=nit_loc+1
    itstart = (int(nit / nproc) * rang ) + min((nit % nproc), rang)
    return nit_loc, itstart

#################

if __name__ == "__main__":

    n_process = MPI.COMM_WORLD.size
    rank = MPI.COMM_WORLD.rank

    parser = argparse.ArgumentParser(description="measure dt")
    parser.add_argument("-i", default="in.h5", type=str, help="input Green Function at each timestep (will be a hdf5 file, default is in.h5)")
    parser.add_argument("-n", default="0", type=int, help="number of pairs of station in timestep input in.h5 file")
    parser.add_argument("-t", default="101", type=int, help="number of time lags for cc")
    parser.add_argument("-tb", default="0.0", type=float, help="begin of tcorr (sec)")
    parser.add_argument("-te", default="1.0", type=float, help="end of tcorr (sec)")
    parser.add_argument("-itb", default="100", type=int, help="begin of tcorr for zoomed lag window (sample)")
    parser.add_argument("-ite", default="301", type=int, help="end of tcorr for zoomed lag window (sample)")
    parser.add_argument("-rev", default="1", type=int, help="make the sum of causal and reversed acausal parts (default 1 : yes/true)")
    parser.add_argument("-avgwin", default="12", type=int, help="number of timesteps in moving averged window (default 12 : which corresponds to 2h if 10min timestep)")
    parser.add_argument("-istaref", default="1", type=int, help="index (0-based) of reference station for computing Dt")
    parser.add_argument("-s", default="2736", type=int, help="number of time steps in input in.h5 file")
    parser.add_argument("-f", default="4.35", type=float, help="central frequency to determine dphi, default is (2.9+5.8)/2=4.35")
    parser.add_argument("-fs", default="100", type=int, help="input frequency for correlation, default is 100Hz")
    parser.add_argument("-o", default="out.h5", type=str, help="output measured dt (will be a hdf5 file, default is out.h5)")
#
    args = parser.parse_args()
    ntime = args.t # 401 # np.size(GFref)   # 201 centre en zero
    t0 = args.tb
    t1 = args.te
    it0 = args.itb
    it1 = args.ite
    istaref = args.istaref
    nstep = args.s # 2736  # doit etre corrigé selon avgwin 144 # np.shape(GF)[0]   # ou 133 pour la derniere journée
    nbpair = args.n
    centralFreq = args.f # EXP003 : ( 2.9 + 5.8 ) / 2.0 = 4.35Hz

    omega = 2.0 * math.pi * centralFreq
    inv_omega = 1.0 / omega

    revopt = args.rev
    avgwin = args.avgwin

    nstep = nstep - avgwin + 1

#    normalize = 1
    timevec = np.linspace(t0, t1, num=ntime)

    Fs = args.fs

    phase = np.exp( 1j * omega * timevec[:] )

    deltat = np.empty((nstep,),dtype=np.float32)
    coh = np.empty((nstep,),dtype=np.float32)

# In case of NaN, replace GFwin with random noise following uniforme law U[-1e-3; 1e-3], and then filtered
# caracteristique du filter une seule fois pour toute
    lowcut = centralFreq - 2.0  # Hz
    highcut = centralFreq + 2.0
    bb, aa = butter_bandpass(lowcut, highcut, Fs, order=3)

    nbpair_loc, ipair_start = create_local_topo(nbpair, n_process, rank)

#    print("[",rank,"]", nbpair_loc,ipair_start)
    deltat_local = np.empty((nstep,nbpair_loc),dtype=np.float32)
    coh_local = np.empty((nstep,nbpair_loc),dtype=np.float32)
    map_ipair = np.empty((nbpair_loc),dtype=np.int32)
    deltat_local[:,:] = np.nan
    coh_local[:,:] = np.nan
    map_ipair[:] = -1

# Open all corr input file
    h5f = h5py.File(args.i, "r")

    nbsta = int(math.ceil(math.sqrt(2.0*nbpair))) 

    if rank == 0:
        tstart0 = time.time()
        h5fo = h5py.File(args.o,'w') # , driver='mpio', comm=MPI.COMM_WORLD)
        dsetdt = h5fo.create_dataset('dt', (nstep, nbpair), dtype='float32', shuffle=True, compression='gzip')
        dsetcoh = h5fo.create_dataset('coh', (nstep, nbpair), dtype='float32', shuffle=True, compression='gzip')
        dsetDt = h5fo.create_dataset('Dt', (nstep, nbsta), dtype='float32', shuffle=True, compression='gzip')
        dsetDtw = h5fo.create_dataset('Dtw', (nstep, nbsta), dtype='float32', shuffle=True, compression='gzip')
        dsetCoh = h5fo.create_dataset('Coh', (nstep, nbsta), dtype='float32', shuffle=True, compression='gzip')
        h5fo.close()

    # Loop over the stations pairs for measuring dt

    for ipair_loc in np.arange(nbpair_loc):
        line = linecache.getline('list_pair.txt',ipair_start+ipair_loc+1)
        node = line.split(" ")[0]
        ipair = int(line.split(" ")[1])

        tstart=time.time()

        # Read correlations from this pair at every nstep timesteps and along t_corr zoom
        GF = read_HFcorr(h5f, node, nstep, it0, it1, revopt)

        # Compute a reference GF
        GFref = np.nanmean(GF, axis=0)

        # and its normalized value
        if (max(abs(GFref)) != 0):
            GFref_n = GFref/max(abs(GFref))
        else:
            print("Warning: GFref is 0 everywhere")
            GFref_n = GFref

        DFTbase = np.dot(GFref,phase) # DFT signal ref

        if ((DFTbase != 0) and (np.isfinite(DFTbase))):

            # Loop over timesteps
            for istep in np.arange(nstep):
                GFwin = np.nanmean(GF[istep:istep+avgwin,:],axis=0)  # decimation

                # Check for NaN values, and if any, replace by small value with random noise
                if np.isnan(GFwin).any():
                    print("Warning: NaN values in decimated correlations GFwin for",istep,ipair,node)
                    #with open("nan_"+str(rank)+".txt", "a") as o:
                    #    o.write(node+" "+str(ipair)+" "+str(istep)+"\n")
                    # Normally, in that case GFwin should be full of NaN values
                    if np.isfinite(GFwin).any():
                        print("Error: only some NaN values in correlations for",dsetname,it)
                        sys.exit("Error: erratic NaN values in correlations...")
                    else:
                        GFwin = 1e-3*(np.random.uniform(-1, 1, len(GFwin)))
                        GFwin = lfilter(bb, aa, GFwin)

                DFTfluc = np.dot( GFwin , phase ) #  DFT signal perturbe
                #if ((DFTfluc != 0) and (np.isfinite(DFTfluc))):
                if np.isfinite(DFTfluc):
                    #deltat[istep,ipair] = np.angle(DFTfluc/DFTbase) * inv_omega # calcul du dt
                    #coh[istep,ipair] = compute_coherence(GFref_n, GFwin, deltat[istep,ipair], Fs)
                    deltat[istep] = np.angle(DFTfluc/DFTbase) * inv_omega # calcul du dt
                    coh[istep] = compute_coherence(GFref_n, GFwin, deltat[istep], Fs)
                    if (DFTfluc == 0):
                        print("0 in DFTfluc",istep,ipair,node,deltat[istep],coh[istep])
                else:
                    print("Error: NaNs in DFTfluc",istep,ipair,node,DFTfluc)
                    #with open("error_"+str(rank)+".txt", "a") as o:
                    #    o.write("DFTfluc "+str(istep)+" "+str(ipair)+" "+node+" "+str(DFTfluc)+"\n")
                    deltat[istep] = np.nan
                    coh[istep] = np.nan
        else:
            print("Error: NaNs or ZEROS in DFTbase",ipair,node,DFTbase)
            #with open("error_"+str(rank)+".txt", "a") as o:
            #    o.write("DFTbase ",str(istep)+" "+str(ipair)+" "+node+" "+str(DFTbase)+"\n")
            deltat[:] = np.nan
            coh[:] = np.nan

        deltat_local[:,ipair_loc] = deltat[:]   # on realise la transposition des maintenant...
        coh_local[:,ipair_loc] = coh[:]

        map_ipair[ipair_loc]=ipair


        elps=time.time()-tstart
        print(rank, ipair_loc, ipair, node, elps)

    h5f.close()

    MPI.COMM_WORLD.Barrier()
    if rank == 0:
        elps0 = time.time()-tstart0
        print("[",rank,"], Total elapsed time for measuring dt",elps0)
        tstart0 = time.time()

    for rang_ecrit in range(n_process):
        if rank == rang_ecrit:
            tstart=time.time()
            h5fo = h5py.File(args.o,'a') # , driver='mpio', comm=MPI.COMM_WORLD)
            h5fo['/dt'][:,map_ipair[:]] = deltat_local[:,:]
            h5fo['/coh'][:,map_ipair[:]] = coh_local[:,:]
            h5fo.close()
            elps = time.time()-tstart
            print("[",rank,"] seq write dt and coh",elps)
        MPI.COMM_WORLD.Barrier()

    if rank == 0:
        elps0 = time.time()-tstart0
        print("[",rank,"], Total elapsed time for writing dt",elps0)
        tstart0 = time.time()

# maintenant on relit en separant les process selon nstep

    nstep_loc, istep_start = create_local_topo(nstep, n_process, rank)
    Dt_loc = np.zeros((nstep_loc, nbsta), dtype=np.float32)
    Dtw_loc = np.zeros((nstep_loc, nbsta), dtype=np.float32)
    Coh_loc = np.zeros((nstep_loc, nbsta), dtype=np.float32)
    map_istep = np.empty((nstep_loc),dtype=np.int32)
    map_istep[:] = -1

    deltatt = np.zeros((nbpair,), dtype=np.float32)
    coht = np.zeros((nbpair,), dtype=np.float32)

    h5fo = h5py.File(args.o,'r')

    # loop over local istep
    for istep_loc in np.arange(nstep_loc):
        tstart=time.time()
        istep = istep_start+istep_loc # get istep global
        deltatt[:]=h5fo['/dt'][istep,:]
        coht[:]=h5fo['/coh'][istep,:]
        Dt_loc[istep_loc,:] = compute_Dt(deltatt, nbsta, istaref)
        Dtw_loc[istep_loc,:], Coh_loc[istep_loc,:] = compute_Dtw(deltatt, coht, nbsta, istaref)
        map_istep[istep_loc]=istep
        elps = time.time()-tstart
        print(rank, istep_loc,istep,elps)

    h5fo.close()

    MPI.COMM_WORLD.Barrier()

    if rank == 0:
        elps0 = time.time()-tstart0
        print("[",rank,"], Total elapsed time for computing Dt",elps0)
        tstart0 = time.time()

    for rang_ecrit in range(n_process):
        if rank == rang_ecrit:
            tstart = time.time()
            h5fo = h5py.File(args.o,'a') # , driver='mpio', comm=MPI.COMM_WORLD)
            h5fo['/Dt'][map_istep[:],:] = Dt_loc[:,:]
            h5fo['/Dtw'][map_istep[:],:] = Dtw_loc[:,:]
            h5fo['/Coh'][map_istep[:],:] = Coh_loc[:,:]
            h5fo.close()
            elps = time.time()-tstart
            print("[",rank,"] seq write Dt, Dtw and Coh",elps)
        MPI.COMM_WORLD.Barrier()

    if rank == 0:
        elps0 = time.time()-tstart0
        print("[",rank,"], Total elapsed time for writing Dt",elps0)
