#!/bin/bash
#OAR --project iworms
##OAR --project iste-equ-ondes
#OAR -l /nodes=1/cpu=1/core=1,walltime=40:00:00
#OAR -n measure_dt
#OAR -p network_address='luke4'
##OAR -t besteffort

set -e

# CIMENT GRID Clusters
#source /applis/ciment/v2/env.bash
source /applis/site/nix.sh

#ISTERRE Clusters
#source /soft/env.bash

date

START=$(date +%s.%N)

cat $OAR_NODE_FILE

JD=$1
JDp1=$((JD+1))
EXP="EXP003"

STAREF="R3010"

case "$CLUSTER_NAME" in
  luke)
    #PYTHONMODULE="python/2.7.12_gcc-4.6.2"
    #module load ${PYTHONMODULE}
    # suppose that we have already install python3 with h5py and matplotlib: nix-env -if python3-env.nix
    IDIR="/scratch_md1200/lecointre/"
    ;;
  ISTERRE)
    PYTHONMODULE="python/python3.5"
    IDIR="/media/luke4_scratch_md1200/lecointre/"
    module load ${PYTHONMODULE}
    ;;
esac

#####################################################

INPUT_STEP_DIR="/$IDIR/SAN_JACINTO/CORRELATION/$EXP/"
INPUT_REF_DIR="/$IDIR/SAN_JACINTO/CORRELATION/$EXP/GLOBALMEAN/"
INPUT_STEP_FILE="SG.2014$JD.000000.h5"
#INPUT_STEP_FILE="parallel_out.h5"
#INPUT_REF_FILE="SG.GLOBALMEAN.2014.128.158.000000.h5"
INPUT_REF_FILE="SG.GLOBALMEAN.2014.132.150.000000.h5"

###########################################################

PEXE=measure_dt_J.py


###########################################################

CIGRI_CAMPAIGN_ID=${CIGRI_CAMPAIGN_ID:=00000}  # substitute if not initialized

mkdir -p /$SHARED_SCRATCH_DIR/$USER/$CIGRI_CAMPAIGN_ID
#mkdir -p /scratch_md1200/$USER/$CIGRI_CAMPAIGN_ID

TMPDIR="/$SHARED_SCRATCH_DIR/$USER/IWORMS_measure_dt_${CIGRI_CAMPAIGN_ID}_${OAR_JOB_ID}_${JD}"
#TMPDIR="/scratch_md1200/$USER/IWORMS_measure_dt_${CIGRI_CAMPAIGN_ID}_${OAR_JOB_ID}_${JD}"
echo $TMPDIR
mkdir -p $TMPDIR

cp $PEXE $TMPDIR

cd $TMPDIR

###########################
## Get data

ln -sf $INPUT_STEP_DIR/$INPUT_STEP_FILE in.h5
ln -sf $INPUT_REF_DIR/$INPUT_REF_FILE ref.h5
if [[ "$JD" < "150" ]] ; then
  ln -sf $INPUT_STEP_DIR/SG.2014${JDp1}.000000.h5 inp1.h5
fi

# Get the list of stations always available during the whole period 132 150:
cat /$IDIR/SAN_JACINTO/SAN_JACINTO_h5ls/h5ls_13[2-9].txt /$IDIR/SAN_JACINTO/SAN_JACINTO_h5ls/h5ls_14?.txt /$IDIR/SAN_JACINTO/SAN_JACINTO_h5ls/h5ls_150.txt | awk -F_ '{print $1}' | sort | uniq -c | grep " 19 " | awk '{print $2}' > liststa_132_150.txt

# Deduce a list of wanted pairs (without autocorr)
rm -f sta0.txt list_pair.txt
Nsta=$(wc -l liststa_132_150.txt | awk '{print $1}')
sta0=$(head -1 liststa_132_150.txt)
for i1 in $(seq 1 $((Nsta-1))) ; do
  echo $sta0 >> sta0.txt
done
for i1 in $(seq 1 $((Nsta-1))) ; do
  sta1=$(head -$i1 liststa_132_150.txt | tail -1)
  head -$((Nsta-i1)) sta0.txt | sed -e "s/$sta0/$sta1/" > sta1.txt

  tail -$((Nsta-i1)) liststa_132_150.txt > sta2.txt

  paste --delimiter="_" sta1.txt sta2.txt >> list_pair.txt
  
done
rm sta0.txt sta1.txt sta2.txt

nbpair=$(wc -l list_pair.txt | awk '{print $1}')

# Get the index of reference station
indstaref=$(cat liststa_132_150.txt | grep -n "$STAREF" | awk -F: '{print $1}')

echo "$JD $STAREF $indstaref"

if [[ "$JD" < "150" ]] ; then

## option [ -2 : 2 ] sec : 401 points pour mesure dt
#/usr/bin/time -v   python2.7 -u measure_dt.py -n $nbpair -t 401 -tb -2 -te 2
#
## option [ -1 : 1 ] sec : 201 points pour mesure dt
#/usr/bin/time -v   python2.7 -u measure_dt.py -n $nbpair -t 201 -tb -1 -te 1 -itb 100 -ite 301 

## option [ 0 : 1 ] + [ 0 : -1 ] : 101 points pour mesure dt
#/usr/bin/time -v python2.7 -u measure_dt.py -n $nbpair -t 101 -tb 0 -te 1 -itb 100 -ite 301 -istaref $((indstaref-1))

# option [ 0 : 1 ] + [ 0 : -1 ] : 101 points pour mesure dt
/usr/bin/time -v python3 -u $PEXE -n $nbpair -t 101 -tb 0 -te 1 -itb 100 -ite 301 -istaref $((indstaref-1)) --ip inp1.h5

else

# option [ 0 : 1 ] + [ 0 : -1 ] : 101 points pour mesure dt
/usr/bin/time -v python3 -u $PEXE -n $nbpair -t 101 -tb 0 -te 1 -itb 100 -ite 301 -istaref $((indstaref-1)) -s 133

fi

mv out.h5 out_$JD.h5

mv $TMPDIR /$SHARED_SCRATCH_DIR/$USER/$CIGRI_CAMPAIGN_ID/.

date
END=$(date +%s.%N)
#module purge # bc doesn't work if loading python module on v2 cigri env
DIFF=$(echo "$END - $START" | bc)
echo "ElapsedTime: $DIFF sec"
