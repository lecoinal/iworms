#!/applis/ciment/v2/stow/x86_64/gcc_4.6.2/python_2.7.12/bin/python2.7

import numpy as np
import argparse
import h5py
import math
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import time

""" module for measuring clock difference dt between a GF(timestep) and a GF(ref)"""

def read_ref(h5filename, dsetname):
    """h5filename, dsetname : input (strings)
       read 1D-dataset dsetname from h5filename HDF5file """
    h5f = h5py.File(h5filename, "r")
    dset = h5f[dsetname][:]
    print(dset.shape)
    h5f.close()
    return dset

def read_step(h5filename, dsetname):
    """h5filename, dsetname : input (strings)
       read 2D-dataset dsetname from h5filename HDF5file """
    h5f = h5py.File(h5filename, "r")
    dset = h5f[dsetname][:,:]
    print(dset.shape)
#    plt.clf()
#    plt.plot(dset,'.b')
#    plt.grid()
#    plt.title(dsetname)
#    fig = plt.gcf()
#    fig.set_size_inches(16.6,8.0)
#    plt.savefig(dsetname+".png",bbox_inches="tight")
    h5f.close()
    return dset

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="measure dt")
    parser.add_argument("-i", default="in.h5", type=str, help="input Green Function at each timestep (will be a hdf5 file, default is in.h5)")
    parser.add_argument("-n", default="0", type=int, help="number of pairs of station in timestep input in.h5 file")
    parser.add_argument("-t", default="401", type=int, help="number of time lags for cc")
    parser.add_argument("-tb", default="-2.0", type=float, help="begin of tcorr (sec)")
    parser.add_argument("-te", default="2.0", type=float, help="end of tcorr (sec)")
    parser.add_argument("-itb", default="0", type=int, help="begin of tcorr (sample)")
    parser.add_argument("-ite", default="401", type=int, help="end of tcorr (sample)")
    parser.add_argument("-s", default="144", type=int, help="number of time steps in input in.h5 file")
    parser.add_argument("-r", default="ref.h5", type=str, help="input reference Green Function (will be a hdf5 file, default is ref.h5)")
    parser.add_argument("-f", default="4.35", type=float, help="central frequency to determine dphi, default is (2.9+5.8)/2=4.35")
    parser.add_argument("-o", default="out.h5", type=str, help="output measured dt (will be a hdf5 file, default is out.h5)")

    args = parser.parse_args()

    ntime = args.t # 401 # np.size(GFref)   # 201 centre en zero
    t0 = args.tb
    t1 = args.te
    it0 = args.itb
    it1 = args.ite
    nstep = args.s # 144 # np.shape(GF)[0]
    nbpair = args.n
    X = args.f # ( 2.9 + 5.8 ) / 2.0

    ln_decimate = 12

    normalize = 1
    timevec = np.linspace(t0, t1, num=ntime)
    Fs = 100

    phase = np.exp( 2.0 * 1j * math.pi * X * timevec[:] )
    ln_mkplt = 0

    GF = np.zeros((nstep,len(timevec)),dtype=np.float32)

    if ln_decimate != 0:
        nstep_dec = nstep / ln_decimate
        GF_decimated = np.zeros((nstep_dec,len(timevec)),dtype=np.float32)
        deltat = np.empty((nstep_dec,nbpair),dtype=np.float32)
        coh = np.empty((nstep_dec,nbpair),dtype=np.float32)
    else:
        deltat = np.empty((nstep,nbpair),dtype=np.float32)
        coh = np.empty((nstep,nbpair),dtype=np.float32)

    deltat[:,:] = -9999.0 # np.nan
    coh[:,:] = -9999.0 # np.nan

    # Open ref input file
    h5fr = h5py.File(args.r, "r")

    # Open step input file
    h5f = h5py.File(args.i, "r")

    # Loop over the stations pairs for measuring dt
    ipair=0
    for node in h5f:   # il faut iterer sur step et pas sur ref (une paire peut etre presente dans ref mais pas dans step)
	tstart=time.time()

        if node.split('_')[0] != node.split('_')[1]:  # on n'itere pas sur les autocorr

            if t0 != 0:  # utilise les 2 parties causales et acausales de -lag a + lag
                GF = h5f[node+'/EPZ_EPZ/corr'][0:nstep,it0:it1]
                GFref = h5fr[node+'/EPZ_EPZ/corr'][it0:it1]
            else:        # somme les deux parties causales et acausale sur zoom 0 a +lag
                for istep in np.arange(nstep):
                    tmp = h5f[node+'/EPZ_EPZ/corr'][istep,it0:it1]
                    tmp_rev = tmp+tmp[::-1]   # sum( tmp , reverse(tmp) )
                    GF[istep,:] = tmp_rev[len(tmp_rev)/2:] 
                tmp = h5fr[node+'/EPZ_EPZ/corr'][it0:it1]
                tmp_rev = tmp+tmp[::-1]   # sum( tmp , reverse(tmp) )
                GFref = tmp_rev[len(tmp_rev)/2:] 

            # option pour decimer GF d'un facteur 12, sans overlap.
            if ln_decimate != 0:
                jj = 0
                for ii in np.arange(0,nstep,ln_decimate):
                    GF_decimated[jj,:] = np.mean(GF[ii:ii+ln_decimate,:], axis=0)
                    jj += 1

            if np.std(GFref) != 0:
                DFTbase = np.dot(GFref,phase) # DFT signal ref
                if ln_decimate != 0:
                    for istep in np.arange(nstep_dec):
                         DFTfluc = np.dot( GF_decimated[istep,:] , phase ) #  DFT signal perturbe
                         if DFTfluc != 0:
                             deltat[istep,ipair] = np.angle(DFTfluc/DFTbase) / ( 2.0 * math.pi * X ) # calcul du dt
                             # shift GF_istep (to compute coh)
                             GF_stepn = np.roll(GF_decimated[istep,:],int(round(-Fs*deltat[istep,ipair])))
                             coh[istep,ipair] = np.corrcoef(GFref/max(abs(GFref)),GF_stepn/max(abs(GF_stepn)))[1,0]

                else:
                    for istep in np.arange(nstep):
                         DFTfluc = np.dot( GF[istep,:] , phase ) #  DFT signal perturbe
                         if DFTfluc != 0:
                             deltat[istep,ipair] = np.angle(DFTfluc/DFTbase) / ( 2.0 * math.pi * X ) # calcul du dt
                             # shift GF_istep (to compute coh)
                             GF_stepn = np.roll(GF[istep,:],int(round(-Fs*deltat[istep,ipair])))
                             coh[istep,ipair] = np.corrcoef(GFref/max(abs(GFref)),GF_stepn/max(abs(GF_stepn)))[1,0]
                             if ln_mkplt:
                                 plt.clf()
                                 plt.plot(timevec,GFref,'g')
                                 plt.plot(timevec,GF[istep,:],'b')
                                 plt.plot(timevec,GF_stepn,'.r')
                                 plt.legend(('ref','timestep before corr','timestep after corr'))
                                 plt.title('dt:'+str(deltat[istep,ipair])+'    coh:'+str(coh[istep,ipair]))
                                 plt.savefig('./'+str(istep)+'.'+str(ipair)+'.png', bbox_inches=0)
                elps=time.time()-tstart
                print(ipair,node,elps)
            else:
                elps=time.time()-tstart
                print(ipair,node,elps,"np.std(GFref)=0")
            ipair+=1
        
    h5f.close()
    h5fr.close()
    
    h5fo = h5py.File(args.o,'w')
    dset = h5fo.create_dataset("dt", data=deltat, dtype='float32', compression="gzip")
    dset = h5fo.create_dataset("coh", data=coh, dtype='float32', compression="gzip")
    h5fo.close()  
 
    plt.clf()
    #plt.imshow(deltat, extent=[0,nstep,0,nbpair], aspect='auto') 
    plt.imshow(deltat, aspect='auto') 
    plt.xlabel('timestep')
    plt.ylabel('pair')
    plt.title('dt');
    plt.colorbar()
    plt.savefig('./dt.png', bbox_inches=0)
    plt.clf()
    #plt.imshow(coh, extent=[0,nstep,0,nbpair], aspect='auto') 
    plt.imshow(coh, aspect='auto') 
    plt.xlabel('timestep')
    plt.ylabel('pair')
    plt.title('coh');
    plt.colorbar()
    plt.savefig('./coh.png', bbox_inches=0)
    
    nbsta = int(math.ceil(math.sqrt(2.0*nbpair))) 
    invN = 1.0/float(nbsta)

    if ln_decimate != 0:
        nstep = nstep_dec

    dt_2D = np.zeros((nbsta,nbsta), dtype=np.float32)
    #nbref = nbsta
    nbref = 1
    Dt = -9999.0 * np.ones((nbref,nstep,nbsta), dtype=np.float32)  # not really necessary to assign missing value ?
    Dt_mean_ref = -9999.0 * np.ones((nstep,nbsta), dtype=np.float32)
    sumdt = np.empty((nbsta,), dtype=np.float32)
  
    for istep in np.arange(nstep):
        # reorganize flattened allpairs vector dt into an antisymmetric matrix
        cpt=0
        for ji in np.arange(nbsta-1):
            for jj in np.arange(ji+1,nbsta):
                dt_2D[ji,jj] = deltat[istep,cpt]
                dt_2D[jj,ji] = -deltat[istep,cpt]
                cpt += 1
        for ji in np.arange(nbsta):
            # On calcule la somme des (N-1) elements non nuls de chaque ligne
            # (dt(i,1)+dt(i,2)+...+dt(i,N))
            sumdt[ji]=np.sum(dt_2D[ji,:])
        # On choisit la station k comme reference, on suppose donc Dt(k)=0
        # On itere pour toutes les k eferences
        for iref in np.arange(nbref):
            print('istep,iref: ',str(istep),str(iref))
            for ji in np.arange(nbsta):
                # On calcule les differences de sommes 2 a 2
                # sdtN=sum(dt_2D(N,:));
                Dt[iref,istep,ji]=invN*(sumdt[ji]-sumdt[iref])

    h5fo = h5py.File('Dt.h5','w')
    dset = h5fo.create_dataset("Dt_ref", data=Dt, dtype='float32', compression="gzip")
    # On moyenne les solutions obtenues avec toutes les references
    # N.B. : cela revient au meme que de directement moyenner tous les dts arrivant (ou partant, selon convention choisie) a un noeud
#    Dt_mean_ref=np.mean(Dt,axis=0)
#    dset = h5fo.create_dataset("Dt", data=Dt_mean_ref, dtype='float32', compression="gzip")
    h5fo.close()
