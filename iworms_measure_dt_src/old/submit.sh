#!/bin/bash
#OAR --project iworms
##OAR --project iste-equ-ondes
#OAR -l /nodes=1/cpu=1/core=4,walltime=240:00:00
#OAR -n measure_dt

set -e

# CIMENT GRID Clusters
source /applis/site/nix.sh

#ISTERRE Clusters
#source /soft/env.bash

date

START=$(date +%s.%N)

cat $OAR_NODE_FILE

TMPDIR="$SHARED_SCRATCH_DIR/$USER/oar.$OAR_JOB_ID/"

PEXE=measure_dt_compute_Dt.py

mkdir -p $TMPDIR
cp $PEXE $TMPDIR/. 
cd $TMPDIR

ln -s /bettik/lecointre/IWORMS/out.h5 in.h5
h5ls in.h5 | awk '{print $1}' > list_pair.txt
nbpair=$(wc -l list_pair.txt | awk '{print $1}')


STAREF="R3010"

# get the 0based index of this staref
istaref=$(grep -n -m 1 $STAREF list_pair.txt|awk -F: '{print $1}')
if [ "$istaref" == "1" ] ; then
  # perhaps it should be 0 
  if [ "$(head -1 list_pair.txt | awk -F_ '{print $1}')" == "$STAREF" ] ; then
  istaref=0
  fi
fi

echo $STAREF $istaref

case "$CLUSTER_NAME" in
  luke)
    # suppose that we have already install python3 with h5py and matplotlib: nix-env -if python3-env.nix
    ;;
  ISTERRE)
    PYTHONMODULE="python/python3.5"
    IDIR="/media/luke4_scratch_md1200/lecointre/"
    module load ${PYTHONMODULE}
    ;;
esac

/usr/bin/time -v   python3 -u $PEXE -n $nbpair -istaref $istaref

#mv out.h5 out_$JD.h5
#
#mv $TMPDIR /$SHARED_SCRATCH_DIR/$USER/$CIGRI_CAMPAIGN_ID/.
#
date
END=$(date +%s.%N)
DIFF=$(echo "$END - $START" | bc)
echo "ElapsedTime: $DIFF sec"
