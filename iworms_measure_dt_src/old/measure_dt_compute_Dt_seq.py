#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import argparse
import h5py
import math
#import matplotlib
#matplotlib.use('agg')
#import matplotlib.pyplot as plt
import time

""" 
    module for measuring clock differences dt between a GF(timestep) and a GF(ref)
    at each timestep and for each pair of sensors (each edge of the graph)
    and then computing clock errors Dt relatively to a master node 
    at each timestep and each sensor (each node of the graph)

"""

def read_HFcorr(h5f, dsetname, nstep, btz, etz, revopt):
    """H5fileid, dsetname : input (strings)
       btz, etz :input (ints) : begin and end indices for lagtime (t_zoom from t_corr)
       rev option : input (boolean flag) : option for applying reverse sum of causal and acausal parts
       read a 2D corr dataset (only t_zoom lag) """
    tmp = h5f[dsetname][0:nstep,btz:etz]
    if revopt:
        dim0 = np.shape(tmp)[0]  # timestep dimension
        dim1 = int((etz-btz+1)/(revopt+1))  # corr timelag dimension
        dset = np.empty((dim0, dim1), np.float32)
        for it in np.arange(dim0):
            tmp_rev = tmp[it,:]+tmp[it,::-1]
            dset[it,:] = tmp_rev[int(np.floor(len(tmp_rev)/2)):]
    else:
        dset = tmp
    return dset


def compute_coherence(refGF_norm, stepGF, dt, sf):
    """ mesure de la coherence entre refGF_norm et stepGF,
        associee au decalage dt     """
    idec = int(round(-sf*dt))
    stepGF_rolled = np.roll(stepGF, idec)
    stepGF_rolled_norm = stepGF_rolled / max(abs(stepGF_rolled))
    coherence = np.corrcoef(refGF_norm, stepGF_rolled_norm)[1, 0]
    return coherence

def compute_Dt(deltat, nstep, nbsta, iref):
    """ Calcul du Dt:
     Dt(S=j,ref=k) = (1/nbsta) * ( sum( deltat[j,:]) ) - sum ( deltat[k,:] ) )
    """
    invN = 1.0/float(nbsta)
    dt_2D = np.zeros((nbsta, nbsta), dtype=np.float32)
    sumdt = np.empty((nbsta,), dtype=np.float32)
    for istep in np.arange(nstep):
        # reorganize flattened allpairs vector dt into an antisymmetric matrix
        # diag(dt_2D) should be 0-init
        cpt = 0
        for ji in np.arange(nbsta-1):
            for jj in np.arange(ji+1, nbsta):
                dt_2D[ji, jj] = deltat[istep, cpt]
                dt_2D[jj, ji] = -deltat[istep, cpt]
                cpt += 1
        for ji in np.arange(nbsta):
            # On calcule la somme des (N-1) elements non nuls de chaque ligne
            # (dt(ji,1)+dt(ji,2)+...+dt(ji,N))
            sumdt[ji] = np.sum(dt_2D[ji, :])
        # On choisit la station k comme référence, on suppose donc Dt(k)=0
        # On itère pour toutes les k références
#        for iref in np.arange(nbref):
        for ji in np.arange(nbsta):
            # On calcule les differences de sommes 2 a 2
            Dt[istep, ji] = invN*(sumdt[ji]-sumdt[iref])
    return Dt

def compute_Dtw(deltat, coh, nstep, nbsta, iref):
    """ Calcul du Dtw:
     Dtw(S=j,ref=k) = sum( deltat[j,:]*coh[j,:] ) / sum( coh[j,:] without coh[j,j] )
                    - sum( deltat[k,:]*coh[k,:] ) / sum( coh[k,:] without coh[k,k] )
    """
    dt_2D = np.zeros((nbsta, nbsta), dtype=np.float32)
    co_2D = np.zeros((nbsta, nbsta), dtype=np.float32) # on met zero sur la diag des coh !
    sumdtcoh = np.empty((nbsta,), dtype=np.float32)
    sumcoh = np.empty((nbsta,), dtype=np.float32)
    invNm1 = 1.0/float(nbsta-1)
    for istep in np.arange(nstep):
        # reorganize flattened allpairs vector dt into an antisymmetric matrix
        # diag(dt_2D) should be 0-init
        cpt = 0
        for ji in np.arange(nbsta-1):
            for jj in np.arange(ji+1, nbsta):
                dt_2D[ji, jj] = deltat[istep, cpt]
                dt_2D[jj, ji] = -deltat[istep, cpt]
                co_2D[ji, jj] = coh[istep, cpt]
                co_2D[jj, ji] = coh[istep, cpt]
                cpt += 1
        for ji in np.arange(nbsta):
            # On calcule la somme des (N-1) elements non nuls de chaque ligne
            # (dt(ji,1)+dt(ji,2)+...+dt(ji,N))
            sumdtcoh[ji] = np.dot(dt_2D[ji, :],co_2D[ji,:])
            sumcoh[ji] = np.sum(co_2D[ji,:])
        # On choisit la station k comme référence, on suppose donc Dt(k)=0
        # On itère pour toutes les k références
        #for iref in np.arange(nbref):
        for ji in np.arange(nbsta):
            # On calcule les differences de sommes 2 a 2
            Dt[istep, ji] = (sumdtcoh[ji]/sumcoh[ji])-(sumdtcoh[iref]/sumcoh[iref])
            Coh[istep, ji] = invNm1 * sumcoh[ji] 
    return Dt, Coh

def write_f32gz(fid, var, varname):
    """ Create and write a dataset named varname, containing
    variable var, into HDF5 file identifier fid,
    with F32 encoding, and apply gz compression filter
    """
    dset = fid.create_dataset(varname,
                              data=var,
                              dtype="float32",
                              compression="gzip")
    return dset

#################

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="measure dt")
    parser.add_argument("-i", default="in.h5", type=str, help="input Green Function at each timestep (will be a hdf5 file, default is in.h5)")
    parser.add_argument("-n", default="0", type=int, help="number of pairs of station in timestep input in.h5 file")
    parser.add_argument("-t", default="101", type=int, help="number of time lags for cc")
    parser.add_argument("-tb", default="0.0", type=float, help="begin of tcorr (sec)")
    parser.add_argument("-te", default="1.0", type=float, help="end of tcorr (sec)")
    parser.add_argument("-itb", default="100", type=int, help="begin of tcorr for zoomed lag window (sample)")
    parser.add_argument("-ite", default="301", type=int, help="end of tcorr for zoomed lag window (sample)")
    parser.add_argument("-rev", default=True, type=bool, help="make the sum of causal and reversed acausal parts")
    parser.add_argument("-avgwin", default="12", type=int, help="number of timesteps in moving averged window (default 12 : which corresponds to 2h if 10min timestep)")
    parser.add_argument("-istaref", default="1", type=int, help="index (0-based) of reference station for computing Dt")
    parser.add_argument("-s", default="2736", type=int, help="number of time steps in input in.h5 file")
    parser.add_argument("-f", default="4.35", type=float, help="central frequency to determine dphi, default is (2.9+5.8)/2=4.35")
    parser.add_argument("-fs", default="100", type=int, help="input frequency for correlation, default is 100Hz")
    parser.add_argument("-o", default="out.h5", type=str, help="output measured dt (will be a hdf5 file, default is out.h5)")
#
    args = parser.parse_args()
    ntime = args.t # 401 # np.size(GFref)   # 201 centre en zero
    t0 = args.tb
    t1 = args.te
    it0 = args.itb
    it1 = args.ite
    istaref = args.istaref
    nstep = args.s # 2736  # doit etre corrigé selon avgwin 144 # np.shape(GF)[0]   # ou 133 pour la derniere journée
    nbpair = args.n
    centralFreq = args.f # EXP003 : ( 2.9 + 5.8 ) / 2.0 = 4.35Hz

    omega = 2.0 * math.pi * centralFreq
    inv_omega = 1.0 / omega

    revopt = args.rev
    avgwin = args.avgwin

    nstep = nstep - avgwin + 1

#    normalize = 1
    timevec = np.linspace(t0, t1, num=ntime)

#    # Fs est imposé par les fichiers d'input (de corr)
    Fs = args.fs

    phase = np.exp( 1j * omega * timevec[:] )

    deltat = np.empty((nstep,nbpair),dtype=np.float32)
    coh = np.empty((nstep,nbpair),dtype=np.float32)

    deltat[:,:] = np.nan
    coh[:,:] = np.nan

# Open all corr input file
    h5f = h5py.File(args.i, "r")

    # Loop over the stations pairs for measuring dt
    ipair=0

    with open("list_pair.txt") as f:
        for node in f:

            tstart=time.time()
            node = node.rstrip("\n")

            # Read correlations from this pair at every nstep timesteps and along t_corr zoom
            GF = read_HFcorr(h5f, node, nstep, it0, it1, revopt)

            # Compute a reference GF
            GFref = np.mean(GF, axis=0)

            # and its normalized value
            GFref_n = GFref/max(abs(GFref))

            if np.std(GFref) != 0:
                DFTbase = np.dot(GFref,phase) # DFT signal ref

                # Loop over timesteps
                for istep in np.arange(nstep):
                    GFwin = np.mean(GF[istep:istep+avgwin,:],axis=0)  # decimation
                    DFTfluc = np.dot( GFwin , phase ) #  DFT signal perturbe
                    if DFTfluc != 0:
                        deltat[istep,ipair] = np.angle(DFTfluc/DFTbase) * inv_omega # calcul du dt
                        coh[istep,ipair] = compute_coherence(GFref_n, GFwin, deltat[istep,ipair], Fs)

            elps=time.time()-tstart
            print(ipair,node,elps)
            ipair+=1

    f.close()
   
    h5f.close()
 
    h5fo = h5py.File(args.o,'w')
    dset = write_f32gz(h5fo, deltat, "dt")
    dset = write_f32gz(h5fo, coh, "coh")

    tstart = time.time()

    nbsta = int(math.ceil(math.sqrt(2.0*nbpair))) 

    Dt = np.zeros((nstep, nbsta), dtype=np.float32)
    Dt = compute_Dt(deltat, nstep, nbsta, istaref)
    dset = write_f32gz(h5fo, Dt, "Dt")
            
    elps = time.time()-tstart
    print("Compute Dt",elps)

    tstart = time.time()

    Dt = np.zeros((nstep, nbsta), dtype=np.float32)
    Coh = np.zeros((nstep, nbsta), dtype=np.float32)
    Dt, Coh = compute_Dtw(deltat, coh, nstep, nbsta, istaref)
    dset = write_f32gz(h5fo, Dt, "Dtw")
    dset = write_f32gz(h5fo, Coh, "Coh")

    elps = time.time()-tstart
    print("Compute Dtw and Coh",elps)

    h5fo.close()
