#!/bin/bash
##OAR --project iworms
#OAR --project iste-equ-ondes
#OAR -l /nodes=1/cpu=1/core=1,walltime=60:00:00
#OAR -n measure_dt
##OAR -p network_address='luke4'
##OAR -t besteffort

set -e

# CIMENT GRID Clusters
#source /applis/ciment/v2/env.bash

#ISTERRE Clusters
source /soft/env.bash

date

START=$(date +%s.%N)

cat $OAR_NODE_FILE

JD=$1
EXP="EXP003"

case "$CLUSTER_NAME" in
  luke)
    PYTHONMODULE="python/2.7.12_gcc-4.6.2"
    #IDIR="/scratch_md1200/lecointre/"
    #IDIR="/bettik/lecointre/"
    IDIR="/scratch_md1200/lecointre/"
    ;;
  ISTERRE)
    PYTHONMODULE="python/python2.7"
    IDIR="/media/luke4_scratch_md1200/lecointre/"
    ;;
esac

#####################################################

INPUT_STEP_DIR="/$IDIR/SAN_JACINTO/CORRELATION/$EXP/"
INPUT_REF_DIR="/$IDIR/SAN_JACINTO/CORRELATION/$EXP/GLOBALMEAN/"
INPUT_STEP_FILE="SG.2014$JD.000000.h5"
#INPUT_REF_FILE="SG.GLOBALMEAN.2014.128.158.000000.h5"
INPUT_REF_FILE="SG.GLOBALMEAN.2014.132.150.000000.h5"

###########################################################

PEXE=measure_dt.py

module load ${PYTHONMODULE}

###########################################################

CIGRI_CAMPAIGN_ID=${CIGRI_CAMPAIGN_ID:=00000}  # substitute if not initialized

mkdir -p /$SHARED_SCRATCH_DIR/$USER/$CIGRI_CAMPAIGN_ID
#mkdir -p /scratch_md1200/$USER/$CIGRI_CAMPAIGN_ID

TMPDIR="/$SHARED_SCRATCH_DIR/$USER/IWORMS_measure_dt_${CIGRI_CAMPAIGN_ID}_${OAR_JOB_ID}_${JD}"
#TMPDIR="/scratch_md1200/$USER/IWORMS_measure_dt_${CIGRI_CAMPAIGN_ID}_${OAR_JOB_ID}_${JD}"
echo $TMPDIR
mkdir -p $TMPDIR

cp $PEXE $TMPDIR

cd $TMPDIR

###########################
## Get data

ln -sf $INPUT_STEP_DIR/$INPUT_STEP_FILE in.h5
ln -sf $INPUT_REF_DIR/$INPUT_REF_FILE ref.h5

h5ls in.h5 | awk '{print $1}' > listpair.txt

nbpair=$(wc -l listpair.txt | awk '{print $1}')  # avec autocorr
echo "nbpair with autocorr $nbpair"
nbsta=$(echo "scale=0; sqrt ( 2*$nbpair )" | bc -l)
echo "nbsta $nbsta"
nbpair=$(echo "scale=0; $nbsta*($nbsta-1)/2" | bc -l)
echo "nbpair without autocorr $nbpair"

## option [ -2 : 2 ] sec : 401 points pour mesure dt
#/usr/bin/time -v   python2.7 -u measure_dt.py -n $nbpair -t 401 -tb -2 -te 2
#
## option [ -1 : 1 ] sec : 201 points pour mesure dt
#/usr/bin/time -v   python2.7 -u measure_dt.py -n $nbpair -t 201 -tb -1 -te 1 -itb 100 -ite 301 

# option [ 0 : 1 ] + [ 0 : -1 ] : 101 points pour mesure dt
/usr/bin/time -v   python2.7 -u measure_dt.py -n $nbpair -t 101 -tb 0 -te 1 -itb 100 -ite 301 

mv out.h5 out_$JD.h5
mv Dt.h5 Dt_$JD.h5

mv $TMPDIR /$SHARED_SCRATCH_DIR/$USER/$CIGRI_CAMPAIGN_ID/.

date
END=$(date +%s.%N)
#module purge # bc doesn't work if loading python module on v2 cigri env
DIFF=$(echo "$END - $START" | bc)
echo "ElapsedTime: $DIFF sec"
