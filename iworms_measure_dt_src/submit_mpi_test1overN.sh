#!/bin/bash
#OAR --project f-image
##OAR --project iste-equ-ondes
#OAR -l /nodes=1/cpu=1/core=4,walltime=1:00:00
#OAR -n measure_dt

set -e

# CIMENT GRID Clusters
#source /applis/site/nix.sh
source /applis/site/guix-start.sh

# refresh_guix iworms_mpi

# guix package -i openmpi python@3 python-numpy python-mpi4py python-h5py hdf5

AVGWIN=$1   # 12 (2h), 6 (1h), 3 (30min), 1 (10min)

n=$2        # 10 ou 100

#ISTERRE Clusters
#source /soft/env.bash

date

START=$(date +%s.%N)

cat $OAR_NODE_FILE

nbprocs=$(cat $OAR_NODE_FILE | wc -l)

TMPDIR="$SHARED_SCRATCH_DIR/$USER/oar.$OAR_JOB_ID/"

PEXE=measure_dt_compute_Dt.py

mkdir -p $TMPDIR
cp $PEXE $TMPDIR/. 
cd $TMPDIR

echo $TMPDIR

##ln -s /bettik/lecoinal/IWORMS/out_N100.h5 in.h5
##ln -s /bettik/lecoinal/IWORMS/out_N5.h5 in.h5
#ln -s /bettik/lecoinal/IWORMS/out.h5 in.h5
##h5ls in.h5 | awk '{print $1 " " NR-1}' > list_pair.txt
#awk '{print $1 " " NR-1}' ../oar.39149898/list_pair.txt > list_pair.txt

#ln -s /bettik/lecoinal/IWORMS/out.h5 in.h5
#awk '{print $1 "_EPZ_EPZ_corr " NR-1}' /bettik/lecoinal/IWORMS/SJ_132_150_923/list_pair.txt > list_pair.txt
#nbpair=$(wc -l list_pair.txt | awk '{print $1}')

STAREF="R3010"


ln -s /bettik/lecoinal/IWORMS/out.h5 in.h5
cp /bettik/lecoinal/IWORMS/SJ_132_150_923/liststa_132_150.txt .
grep $STAREF liststa_132_150.txt > liststa
for ii in $(seq 1 $n) ; do
  rnd=$(( $RANDOM % 923 ))
  head -$((rnd+1)) liststa_132_150.txt | tail -1 >> liststa
  echo $ii $rnd
done

sort -u liststa > sorted_liststa

while [ $(wc -l sorted_liststa|awk '{print $1}') -lt $((n+1)) ]; do 
  rnd=$(( $RANDOM % 923 ))
  head -$((rnd+1)) liststa_132_150.txt | tail -1 >> liststa
  echo $rnd
  sort -u liststa > sorted_liststa
done

for ii in $(seq 1 $n) ; do
  s1=$(head -$ii sorted_liststa | tail -1)
  for ij in $(seq $((ii+1)) $((n+1))) ; do
    s2=$(head -$ij sorted_liststa | tail -1)
    echo "${s1}_${s2}" >> zzzzz
  done
done

#awk '{print $1 "_EPZ_EPZ_corr " NR-1}' /bettik/lecoinal/IWORMS/SJ_132_150_923/list_pair.txt > list_pair.txt
awk '{print $1 "_EPZ_EPZ_corr " NR-1}' zzzzz > list_pair.txt
nbpair=$(wc -l list_pair.txt | awk '{print $1}')


# get the 0based index of this staref
istaref=$(grep -n -m 1 $STAREF list_pair.txt|awk -F: '{print $1}')
if [ "$istaref" == "1" ] ; then
  # perhaps it should be 0 
  if [ "$(head -1 list_pair.txt | awk -F_ '{print $1}')" == "$STAREF" ] ; then
    istaref=0
  fi
fi

echo $STAREF $istaref $nbpair

case "$CLUSTER_NAME" in
  luke)
    # suppose that we have already install python3 with h5py and matplotlib: nix-env -if python3-env.nix
    ;;
  ISTERRE)
    PYTHONMODULE="python/python3.5"
    IDIR="/media/luke4_scratch_md1200/lecointre/"
    module load ${PYTHONMODULE}
    ;;
esac

# /usr/bin/time -v   python3 -u $PEXE -n $nbpair -istaref $istaref
/usr/bin/time -v mpiexec -n ${nbprocs} python3 -u $PEXE -n $nbpair -istaref $istaref -avgwin $AVGWIN

#mv out.h5 out_$JD.h5
#
#mv $TMPDIR /$SHARED_SCRATCH_DIR/$USER/$CIGRI_CAMPAIGN_ID/.
#
date
END=$(date +%s.%N)
DIFF=$(echo "$END - $START" | bc)
echo "ElapsedTime: $DIFF sec"
