Module for measuring clock differences dt between a GF(timestep) and a GF(ref) at each timestep and for each pair of sensors (each edge of the graph) and then computing clock errors Dt relatively to a master node at each timestep and each sensor (each node of the graph).

Dependancies:

```
> source /applis/site/guix-start.sh
> refresh_guix measuredt
> guix package -m manifest_measuredt.scm -p $GUIX_USER_PROFILE_DIR/measuredt
> refresh_guix measuredt
Activate profile  /var/guix/profiles/per-user/lecoinal/measuredt
The following packages are currently installed in /var/guix/profiles/per-user/lecoinal/measuredt:
hdf5            1.12.1  out     /gnu/store/1rjs998na0xd57ach5w0432z615p4j5b-hdf5-1.12.1
python-scipy 	1.8.0 	out	/gnu/store/ca6jqzx72j3xs6ljcrwf7c849125cr7b-python-scipy-1.8.0
python-h5py     3.6.0   out     /gnu/store/68nggkk7rxkmxn8k6iadh3fiqyb9cqvs-python-h5py-3.6.0
python-mpi4py	3.0.3 	out	/gnu/store/mp714a9xjcdgrc4i4fq0dx7l8qmlw7mx-python-mpi4py-3.0.3
python          3.9.9   out     /gnu/store/sz7lkmic6qrhfblrhaqaw0fgc4s9n5s3-python-3.9.9
>
```

Note: In the present code the writings are serialized, but now python-h5py-mpi is working with guix (and in multi-nodes), thus we could change h5py for h5py-mpi and remove the synchronization barriers around writing...

The input file should contain HDF5 datasets with the correlations between each pair of sensors:
```
# example with 10min-correlations along 4752 timesteps (33days), with lags between -2 and +2sec at 500Hz (2001 samples):
> h5ls ZO.2018115.2018147.h5 |head -20
AR001_AR002_DPZ_DPZ_corr Dataset {4752, 2001}
AR001_AR004_DPZ_DPZ_corr Dataset {4752, 2001}
AR001_AR005_DPZ_DPZ_corr Dataset {4752, 2001}
AR001_AR006_DPZ_DPZ_corr Dataset {4752, 2001}
AR001_AR007_DPZ_DPZ_corr Dataset {4752, 2001}
[...]
AR097_AR099_DPZ_DPZ_corr Dataset {4752, 2001}
AR097_AR100_DPZ_DPZ_corr Dataset {4752, 2001}
AR098_AR099_DPZ_DPZ_corr Dataset {4752, 2001}
AR098_AR100_DPZ_DPZ_corr Dataset {4752, 2001}
AR099_AR100_DPZ_DPZ_corr Dataset {4752, 2001}
```

The output file contains the time differences dt and associated coherence coh for all pairs of sensors (here 4753 : 98 sensors), and the computed clock error Dt, and associated Coherence Coh, and weighted clock error Dtw:
```
> h5ls Dt_115_147.h5 
Coh                      Dataset {4735, 98}
Dt                       Dataset {4735, 98}
Dtw                      Dataset {4735, 98}
coh                      Dataset {4735, 4753}
dt                       Dataset {4735, 4753}
```
In this example we stacked correlations along 3hours time width, with keeping 10-min time resolution, thus the time vector results in 4735 time steps (33*144-17 : no overlap on next day 148).

---

# Discussion

For a Discussion about Big Data challenges about Computing correlations, Measuring time differences and Computing clock errors :

see [../paper/annex_sjprocess.tex](../paper/annex_sjprocess.tex)



